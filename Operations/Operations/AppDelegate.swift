//
//  AppDelegate.swift
//  Operations
//
//  Created by Gabani King on 22/01/22.
//

import UIKit
import IQKeyboardManager

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var is_NewVersion = false
    var dicLoginDetails: OPLoginData?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
                
        dicLoginDetails = getCurrentUserData()
        
        IQKeyboardManager.shared().isEnabled = true
        
        APIClient().MakeAPICallWithAuthHeaderGetHeaderEmail(APP_VERSION, headers: ["Email": "vb@gmail.com"]) { (responce, error, statusCose) in
            if error == nil {
                print("Responce: \(String(describing: responce))")
                
                let version_number = responce?.value(forKey: "versionName") as? String ?? ""
                
                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                
                if version_number == appVersion
                {
                    self.is_NewVersion = true
                }
                else
                {
                    self.is_NewVersion = false
                }
                
            }
            
        }
     ///   setupVC()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func saveCurrentUserData(dic: OPLoginData)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "CURRENT_USER_DATA")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> OPLoginData
    {
        if let data = UserDefaults.standard.object(forKey: "CURRENT_USER_DATA"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! OPLoginData
        }
        return OPLoginData()
    }
    
    func setupVC(){
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let home: LoginOPViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginOPViewController") as! LoginOPViewController
        
        let homeNavigation = UINavigationController(rootViewController: home)
        
        homeNavigation.navigationBar.isHidden = true
        self.window?.rootViewController = homeNavigation
        self.window?.makeKeyAndVisible()
        
    }
    
}


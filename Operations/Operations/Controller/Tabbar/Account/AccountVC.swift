//
//  AccountVC.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit
import SwiftUI

class AccountVC: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblNAme: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var imgPto: UIImageView!
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblEmail.text = appDelegate.dicLoginDetails?.email ?? ""
        lblNAme.text = appDelegate.dicLoginDetails?.fullName ?? ""
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Functions
    
    //MARK: - TableView Delegate & DataSource
    
    //MARK: - CollectionView Delegate & DataSource
    
    //MARK: - IBActions
    
    @IBAction func TapOnSetting(_ sender: Any) {
        
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(settingVC, animated: true)
    }
    @IBAction func TapOnProfile(_ sender: Any) {
    }
    @IBAction func TapOnInternalQueies(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InternalQueryVC") as! InternalQueryVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func TapOnPlaceOrder(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderVC") as! PlaceOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func TapOnOrderHistory(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func clickedSalesOrder(_ sender: Any) {
        
        let SalesOrderVC = self.storyboard?.instantiateViewController(withIdentifier: "SalesOrderVC") as! SalesOrderVC
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(SalesOrderVC, animated: true)
        
    }
    @IBAction func clickedSalesOrderHistory(_ sender: Any) {
        
        let orderHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "SalesOrderHistoryVC") as! SalesOrderHistoryVC
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(orderHistoryVC, animated: true)
        
    }
    @IBAction func clickedReports(_ sender: Any) {
        
        let reportsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportsVC") as! ReportsVC
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(reportsVC, animated: true)
        
    }
    @IBAction func TapOnTrainings(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrainingsVC") as! TrainingsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func TapOnAboutCompany(_ sender: Any) {
    }
    @IBAction func TapOnOurBrands(_ sender: Any) {
    }
    @IBAction func TapOnAboutDirectord(_ sender: Any) {
    }
    @IBAction func TapOnWorkingStructure(_ sender: Any) {
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure want to Logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            
            UserDefaults.standard.setValue(false, forKey: "UserLogin")
            UserDefaults.standard.synchronize()
            
            let contentView = WelcomeView()
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(UIHostingController(rootView: contentView), animated: true)
        }))
        
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
       
    }
}

//
//  UpdateGroupViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class UpdateGroupViewController: UIViewController {

    @IBOutlet weak var allPermissionsTableVBiew: UITableView!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var headerLbl: UILabel!
    
    var groupID : Int?
    var groupName : String?
    var headerArray = ["UPDATE PERMISSION"]
    var groupPolicyData = OPGroupPolicy()
    
    let arrCreatePermissions = NSMutableArray()
    
    var str_isDefault = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if str_isDefault == true
        {
            checkBtn.isSelected = true
            checkBtn.setImage(UIImage(named: "check-1"), for: .normal)
        }
        else
        {
            checkBtn.isSelected = false
            checkBtn.setImage(UIImage(named: "unCheck"), for: .normal)
        }
 
        headerLbl.text = groupName
        callGroupPolicyAPI()
        
//        if #available(iOS 15.0, *) {
//            allPermissionsTableVBiew.sectionHeaderTopPadding = 0
//        }

    }
    
    func callGroupPolicyAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary("roles/\(groupID ?? 0)" + GET_GROUP_POLICY, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    self.groupPolicyData = OPGroupPolicy(fromDictionary: response!)
                }
                
                for obj in self.groupPolicyData.permissions!
                {
                    if obj.isSelected == true
                    {
                        let dicNEwData = NSMutableDictionary()
                        dicNEwData.setValue(obj.permissionId, forKey: "permissionId")
                        dicNEwData.setValue(obj.permissionName, forKey: "permissionName")
                        dicNEwData.setValue(obj.permissionCode, forKey: "permissionCode")

                        self.arrCreatePermissions.add(dicNEwData)
                    }
                }
                
                
                self.allPermissionsTableVBiew.delegate = self
                self.allPermissionsTableVBiew.dataSource = self
                self.allPermissionsTableVBiew.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callUpdateGroupAPI()
    {
        
        var param = [String : Any]()
        
        param = ["groupId": "\(groupID ?? 0)","groupName":self.headerLbl.text ?? "","isActive":true,"isDefault":self.str_isDefault,"isGroupSelected":true,"permissions":arrCreatePermissions]

        APIClient.sharedInstance.MakeAPICallWithAuthOutHeaderPut(CREATE_GROUP, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2001
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    
    @IBAction func clickedDone(_ sender: Any) {
        callUpdateGroupAPI()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func checkBtnClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            checkBtn.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        else
        {
            sender.isSelected = true
            checkBtn.setImage(UIImage(named: "check-1"), for: .normal)
            
        }
    }
    
    @objc func addPermissionSwitchClicked(_ sender: UISwitch){
        print("addPermissionSwitchClicked \(sender.tag)")
        
        if sender.isOn == true
        {
            let cell = sender.superview?.superview?.superview as! AddPermissionCell
            
            let indexPath = self.allPermissionsTableVBiew.indexPath(for: cell)

            let dicdata = groupPolicyData.permissions[indexPath!.row-1]
            
            let dicNEwData = NSMutableDictionary()
            dicNEwData.setValue(dicdata.permissionId, forKey: "permissionId")
            dicNEwData.setValue(dicdata.permissionName, forKey: "permissionName")
            dicNEwData.setValue(dicdata.permissionCode, forKey: "permissionCode")

            self.arrCreatePermissions.add(dicNEwData)
        }
        else
        {
            let cell = sender.superview?.superview?.superview as! AddPermissionCell

            let indexPath = self.allPermissionsTableVBiew.indexPath(for: cell)

            let dicdata = groupPolicyData.permissions[indexPath!.row-1]
            
            let dicNEwData = NSMutableDictionary()
            dicNEwData.setValue(dicdata.permissionId, forKey: "permissionId")
            dicNEwData.setValue(dicdata.permissionName, forKey: "permissionName")
            dicNEwData.setValue(dicdata.permissionCode, forKey: "permissionCode")

            self.arrCreatePermissions.remove(dicNEwData)
        }
    }

}

extension UpdateGroupViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return groupPolicyData.permissions.count+1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchViewCell") as! searchViewCell
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPermissionCell") as! AddPermissionCell
            
            let dictData = groupPolicyData.permissions[indexPath.row-1]
            cell.permissionLbl.text = dictData.permissionName
            cell.onOffwitch.addTarget(self, action: #selector(addPermissionSwitchClicked(_:)), for: .valueChanged)
            cell.onOffwitch.tag = indexPath.row-1
            
            cell.onOffwitch.isOn = dictData.isSelected
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
        headerView.lblTitle.text = headerArray[section]
        headerView.btnOpen.isHidden = true
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

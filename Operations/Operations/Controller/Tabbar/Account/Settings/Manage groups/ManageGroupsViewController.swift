//
//  ManageGroupsViewController.swift
//  Operations
//
//  Created by iMac on 10/03/22.
//

import UIKit

class ManageGroupsViewController: UIViewController {

    @IBOutlet weak var groupListTableView: UITableView!
    
    var arrGroups = [OPRoles]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        groupListTableView.delegate = self
        groupListTableView.dataSource = self
       
       
      
//        if #available(iOS 15.0, *) {
//            groupListTableView.sectionHeaderTopPadding = 0
//        }
//
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetRolesAPI()
    }

    func callGetRolesAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_GROUP_ROLES, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.groupListTableView.isHidden = false
                self.arrGroups.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPRoles(fromDictionary: obj as! NSDictionary)
                        self.arrGroups.append(dicData)
                    }
                    self.groupListTableView.dataSource = self
                    self.groupListTableView.delegate = self
                    self.groupListTableView.reloadData()
                    
                }
                else
                {
                    self.arrGroups.removeAll()
                    self.groupListTableView.reloadData()
                }
            }
            else
            {
                self.arrGroups.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createNewGroupClicked(_ sender: Any) {
        let createGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        self.navigationController?.pushViewController(createGroupVC, animated: true)
    }
    
    @objc func groupArrowClicked(_ sender: UIButton){
        print("groupArrowClicked \(sender.tag)")
        
        let updateGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateGroupViewController") as! UpdateGroupViewController
        
        if arrGroups[sender.tag].isDefault == true
        {
            updateGroupVC.str_isDefault = true
        }
        else
        {
            updateGroupVC.str_isDefault = false
        }
        
        updateGroupVC.groupID = arrGroups[sender.tag].groupId
        updateGroupVC.groupName = arrGroups[sender.tag].groupName
        self.navigationController?.pushViewController(updateGroupVC, animated: true)
    }

}

extension ManageGroupsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.groupListTableView.dequeueReusableCell(withIdentifier: "GroupListCell") as! GroupListCell
//
//        let dictData = arrGroups[indexPath.row]
//
//        cell.groupLbl.text = dictData.groupName
//        cell.arrowBtn.addTarget(self, action: #selector(groupArrowClicked(_:)), for: .touchUpInside)
//        cell.arrowBtn.tag = indexPath.row
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
//        headerView.lblTitle.text = "MANAGE CURRENT GROUPS"
//        headerView.btnOpen.isHidden = true
//        return headerView
//
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        return UIView()
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}


class GroupListCell : UITableViewCell{
    @IBOutlet weak var groupLbl: UILabel!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var groupImage: UIImageView!
}

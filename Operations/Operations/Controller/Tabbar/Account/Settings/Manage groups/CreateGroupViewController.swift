//
//  CreateGroupViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class CreateGroupViewController: UIViewController {

    @IBOutlet weak var allPermissionsTableVBiew: UITableView!
    @IBOutlet weak var checkBtn: UIButton!
    
    @IBOutlet weak var txtGroupName: UITextField!
    
    var headerArray = ["ADD PERMISSION"]
    var arrAllPermission = [OPPermissions]()
    
    let arrCreatePermissions = NSMutableArray()
    
    var str_isDefault = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        callGetPermissionAPI()
        
//        if #available(iOS 15.0, *) {
//            allPermissionsTableVBiew.sectionHeaderTopPadding = 0
//        }

    }
    
    func callGetPermissionAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_ALL_PERMISSIONS, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.allPermissionsTableVBiew.isHidden = false
                self.arrAllPermission.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPPermissions(fromDictionary: obj as! NSDictionary)
                        self.arrAllPermission.append(dicData)
                    }
                    self.allPermissionsTableVBiew.dataSource = self
                    self.allPermissionsTableVBiew.delegate = self
                    self.allPermissionsTableVBiew.reloadData()
                    
                }
                else
                {
                    self.arrAllPermission.removeAll()
                    self.allPermissionsTableVBiew.reloadData()
                }
            }
            else
            {
                self.arrAllPermission.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    func callCreateGroupAPI()
    {
        
        var param = [String : Any]()
        
        param = ["groupId": "0","groupName":self.txtGroupName.text ?? "","isActive":true,"isDefault":self.str_isDefault,"isGroupSelected":true,"permissions":arrCreatePermissions]

        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CREATE_GROUP, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2000
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    
    @IBAction func clickedDone(_ sender: Any) {
        
        if txtGroupName.text == ""
        {
            self.view.makeToast("Please enter group name")
        }
        else
        {
            self.callCreateGroupAPI()
        }
        
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func checkBtnClicked(_ sender: UIButton) {
        if sender.isSelected {
            self.str_isDefault = false
            sender.isSelected = false
            checkBtn.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        else
        {
            self.str_isDefault = true
            sender.isSelected = true
            checkBtn.setImage(UIImage(named: "check-1"), for: .normal)
            
        }
    }
    
    @objc func addPermissionSwitchClicked(_ sender: UISwitch){
        print("addPermissionSwitchClicked \(sender.tag)")
        
        if sender.isOn == true
        {
            let cell = sender.superview?.superview?.superview as! AddPermissionCell
            
            let indexPath = self.allPermissionsTableVBiew.indexPath(for: cell)

            let dicdata = arrAllPermission[indexPath!.row-1]
            
            let dicNEwData = NSMutableDictionary()
            dicNEwData.setValue(dicdata.permissionId, forKey: "permissionId")
            dicNEwData.setValue(dicdata.permissionName, forKey: "permissionName")
            dicNEwData.setValue(dicdata.permissionCode, forKey: "permissionCode")
            dicNEwData.setValue(true, forKey: "isActive")
            dicNEwData.setValue(true, forKey: "isSelected")

            self.arrCreatePermissions.add(dicNEwData)
        }
        else
        {
            let cell = sender.superview?.superview?.superview as! AddPermissionCell

            let indexPath = self.allPermissionsTableVBiew.indexPath(for: cell)

            let dicdata = arrAllPermission[indexPath!.row-1]
            
            let dicNEwData = NSMutableDictionary()
            dicNEwData.setValue(dicdata.permissionId, forKey: "permissionId")
            dicNEwData.setValue(dicdata.permissionName, forKey: "permissionName")
            dicNEwData.setValue(dicdata.permissionCode, forKey: "permissionCode")
            dicNEwData.setValue(true, forKey: "isActive")
            dicNEwData.setValue(true, forKey: "isSelected")

            self.arrCreatePermissions.remove(dicNEwData)
        }
    }

}

extension CreateGroupViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5 //arrAllPermission.count+1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchViewCell") as! searchViewCell
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPermissionCell") as! AddPermissionCell
            
//            let dictData = arrAllPermission[indexPath.row-1]
//            cell.permissionLbl.text = dictData.permissionName
//            cell.onOffwitch.addTarget(self, action: #selector(addPermissionSwitchClicked(_:)), for: .valueChanged)
//            cell.onOffwitch.tag = indexPath.row-1
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
        headerView.lblTitle.text = headerArray[section]
        headerView.btnOpen.isHidden = true
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

class AddPermissionCell : UITableViewCell{
    @IBOutlet weak var onOffwitch: UISwitch!
    @IBOutlet weak var permissionLbl: UILabel!
}

class searchViewCell: UITableViewCell{
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var mainView: UIView!
    
}

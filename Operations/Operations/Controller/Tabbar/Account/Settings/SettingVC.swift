//
//  SettingVC.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class SettingVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: - IBOutlets
    
    @IBOutlet var collView: UICollectionView!
    
    private enum Constants {
        static let underlineViewColor: UIColor = .white
        static let underlineViewHeight: CGFloat = 2
    }
    
    var passwordReqCount = 0
    var signupReqCount = 0
    let sectionInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: widthPerItem)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 2
        // edit properties here
        
        return _flowLayout
    }
    
    var arrName = ["Upload Documents","Employee Details", "SignUp Requests", "Password Request", "Manage Groups", "Manage Warehouse", "Inventory History", "All Queries", "All Order", "Manage Trainings", "All Sales Order"]
    var arrImage = ["ic_1", "ic_2", "profile-social-svgrepo-com (1)", "admin_password_request", "admin_manage_group", "admin_manage_warehouse", "ic_7", "ic_8", "admin_manage_warehouse", "admin_manage_warehouse", "admin_manage_warehouse"]
    
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appDelegate.dicLoginDetails?.isSuperAdmin == true
        {
            arrName = ["Upload Documents","Employee Details", "SignUp Requests", "Password Request", "Manage Groups", "Manage Warehouse", "Inventory History", "All Queries", "All Order", "Manage Trainings", "All Sales Order"]
            arrImage = ["ic_1", "ic_2", "profile-social-svgrepo-com (1)", "admin_password_request", "admin_manage_group", "admin_manage_warehouse", "ic_7", "ic_8", "admin_manage_warehouse", "admin_manage_warehouse", "admin_manage_warehouse"]
        }
        else
        {
            
        }
        
        callCountAPI()
        
        collView.delegate = self
        collView.dataSource = self
        collView.collectionViewLayout = flowLayout
        
        collView.register(UINib(nibName: "SettingCell", bundle: nil), forCellWithReuseIdentifier: "SettingCell")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK: - Functions
    
    //MARK: - TableView Delegate & DataSource
    
    //MARK: - CollectionView Delegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "SettingCell", for: indexPath) as! SettingCell
        cell.lblName.text = arrName[indexPath.row]
        cell.imgView.image = UIImage(named: arrImage[indexPath.row])
        
        if indexPath.row == 2 || indexPath.row == 3 {
            if indexPath.row == 2{
                cell.lblCount.text = "\(signupReqCount)"
            }else{
                cell.lblCount.text = "\(passwordReqCount)"
            }
            cell.viewCount.isHidden = false
            
            cell.lblName.textAlignment = .left
        } else {
            cell.viewCount.isHidden = true
            cell.lblName.textAlignment = .center
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let uploadDocumentVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadDocumentVC") as! UploadDocumentVC
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(uploadDocumentVC, animated: true)
            
        } else if indexPath.row == 1 {
            
            let searchEmployeeVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchEmployeeVC") as! SearchEmployeeVC
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(searchEmployeeVC, animated: true)
            
        }else if indexPath.row == 2 {
            
        }else if indexPath.row == 3 {
            
        }else if indexPath.row == 4 {
            
            let manageGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageGroupsViewController") as! ManageGroupsViewController
            self.navigationController?.navigationBar.isHidden = true
            self.navigationController?.pushViewController(manageGroupVC, animated: true)
            
        }else if indexPath.row == 5 {
            let createGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageWarehouseViewController") as! ManageWarehouseViewController
            self.navigationController?.pushViewController(createGroupVC, animated: true)
        }else if indexPath.row == 6 {
            
            let InventeoryVC = self.storyboard?.instantiateViewController(withIdentifier: "InventoryHistoryVC") as! InventoryHistoryVC
            self.navigationController?.pushViewController(InventeoryVC, animated: true)
            
        }else if indexPath.row == 7 {
            let allQueriesVC = self.storyboard?.instantiateViewController(withIdentifier: "AllQueriesViewController") as! AllQueriesViewController
            self.navigationController?.pushViewController(allQueriesVC, animated: true)
        }else if indexPath.row == 8 {
            
            let allOrdersVC = self.storyboard?.instantiateViewController(withIdentifier: "AllOrdersVC") as! AllOrdersVC
            self.navigationController?.pushViewController(allOrdersVC, animated: true)
            
        }else if indexPath.row == 9 {
            
            let TrainigVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageTrainingsVC") as! ManageTrainingsVC
            self.navigationController?.pushViewController(TrainigVC, animated: true)
            
        }else if indexPath.row == 10 {
            
            let OrderHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "SalesOrderHistoryVC") as! SalesOrderHistoryVC
            self.navigationController?.pushViewController(OrderHistoryVC, animated: true)
            
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func TapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - API Calling
    
    func callCountAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary(REQ_COUNT, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                
                self.passwordReqCount = response?.value(forKey: "passwordReqCount") as? Int ?? 0
                self.signupReqCount = response?.value(forKey: "signupReqCount") as? Int ?? 0
                self.collView.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    
}

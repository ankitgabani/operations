//
//  DetailWarehouseViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class DetailWarehouseViewController: UIViewController {

    @IBOutlet weak var employeeListTableVBiew: UITableView!
    @IBOutlet weak var headerLbl: UILabel!
 
    var headerArray = ["WAREHOUSE EMPLOYEE"]
    var arrWarehouseDetails = [OPWarehouseDetail]()
    var warehouseID : Int?
    var warehouseName : String?
    
    var arrWarehouseUsers = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.employeeListTableVBiew.dataSource = self
        self.employeeListTableVBiew.delegate = self

        
        headerLbl.text = warehouseName
        callGetWarehousDetailAPI()
        
//        if #available(iOS 15.0, *) {
//            employeeListTableVBiew.sectionHeaderTopPadding = 0
//        }
    }
    
    func callGetWarehousDetailAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray("warehouses/\(warehouseID ?? 0)" + GET_WAREHOUS_DETAILS, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.employeeListTableVBiew.isHidden = false
                self.arrWarehouseDetails.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPWarehouseDetail(fromDictionary: obj as! NSDictionary)
                        
                        if dicData.isAssignedToWarehouse == true
                        {
                            let dicData11 = NSMutableDictionary()
                            dicData11.setValue(dicData.email, forKey: "email")
                            dicData11.setValue(dicData.employeeId, forKey: "employeeId")
                            dicData11.setValue(dicData.firstName, forKey: "firstName")
                            dicData11.setValue(dicData.fullName, forKey: "fullName")
                            dicData11.setValue(dicData.isAssignedToWarehouse, forKey: "isAssignedToWarehouse")
                            dicData11.setValue(dicData.lastName, forKey: "lastName")
                            dicData11.setValue(dicData.mobileNumber, forKey: "mobileNumber")
                            dicData11.setValue(dicData.nameInitials, forKey: "nameInitials")
                            dicData11.setValue(dicData.userId, forKey: "userId")
                            dicData11.setValue(dicData.wareHouse, forKey: "wareHouse")
                            dicData11.setValue(dicData.wareHouseId, forKey: "wareHouseId")

                            self.arrWarehouseUsers.add(dicData11)
                        }
                        
                        self.arrWarehouseDetails.append(dicData)
                    }
                    
                    self.employeeListTableVBiew.reloadData()
                    
                }
                else
                {
                    self.arrWarehouseDetails.removeAll()
                    self.employeeListTableVBiew.reloadData()
                }
            }
            else
            {
                self.arrWarehouseDetails.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    @IBAction func clickedDone(_ sender: Any) {
        callWarehouseUsersAPI()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @objc func selectSwitchClicked(_ sender: UISwitch){
        print("addPermissionSwitchClicked \(sender.tag)")
    }
    
    func callWarehouseUsersAPI()
    {
        
        var param = [String : Any]()
        
        param = ["schema":self.arrWarehouseUsers,"items":self.arrWarehouseUsers.count,"required":true]

        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(UPDATE_ASSIGNED_USERS + "\(warehouseID ?? 0)" + "/users", parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2000
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
//                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }

}

extension DetailWarehouseViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrWarehouseDetails.count+1
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchViewCell") as! searchViewCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "employeeTableCell") as! employeeTableCell
            
            let dicttData = arrWarehouseDetails[indexPath.row-1]
            cell.employeeNameLbl.text = dicttData.fullName
            cell.emailIdLbl.text = dicttData.email
            cell.onOffwitch.addTarget(self, action: #selector(selectSwitchClicked(_:)), for: .valueChanged)
            cell.onOffwitch.tag = indexPath.row
            cell.onOffwitch.isOn = dicttData.isAssignedToWarehouse
            
            return cell
            
        }
    }
    
}

class employeeTableCell: UITableViewCell{
    @IBOutlet weak var onOffwitch: UISwitch!
    @IBOutlet weak var employeeNameLbl: UILabel!
    @IBOutlet weak var emailIdLbl: UILabel!
    
    
}

//
//  ManageWarehouseViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class ManageWarehouseViewController: UIViewController {
    
    @IBOutlet weak var warehousesListTableView: UITableView!
    var headerArray = ["CURRENT WAREHOUSES"]
    var arrCurrentWarehouses = [OPWarehouses]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        warehousesListTableView.delegate = self
        warehousesListTableView.dataSource =  self
               
//        if #available(iOS 15.0, *) {
//            warehousesListTableView.sectionHeaderTopPadding = 0
//        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetWarehousesAPI()
    }
    
    func callGetWarehousesAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_CURRENT_WAREHOUSES, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.warehousesListTableView.isHidden = false
                self.arrCurrentWarehouses.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPWarehouses(fromDictionary: obj as! NSDictionary)
                        self.arrCurrentWarehouses.append(dicData)
                    }
                    self.warehousesListTableView.dataSource = self
                    self.warehousesListTableView.delegate = self
                    self.warehousesListTableView.reloadData()
                    
                }
                else
                {
                    self.arrCurrentWarehouses.removeAll()
                    self.warehousesListTableView.reloadData()
                }
            }
            else
            {
                self.arrCurrentWarehouses.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createNewWarehouseClicked(_ sender: Any) {
        let createGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateWarehouseViewController") as! CreateWarehouseViewController
        self.navigationController?.pushViewController(createGroupVC, animated: true)
    }
    
    @objc func warehouseEditClicked(_ sender: UIButton){
        print("groupArrowClicked \(sender.tag)")
        
      //  let dictData = arrCurrentWarehouses[sender.tag]

        
        let updateWarehouseVC = self.storyboard?.instantiateViewController(withIdentifier: "UpdateWarehouseViewController") as! UpdateWarehouseViewController
//        updateWarehouseVC.str_W_Name = dictData.descriptionField
//        updateWarehouseVC.str_W_Code = dictData.code
//        updateWarehouseVC.str_MasterID = "\(dictData.masterId ?? 0)"
//        updateWarehouseVC.isCanRegister = dictData.canRegister
//        updateWarehouseVC.modalPresentationStyle = .overCurrentContext
        self.present(updateWarehouseVC, animated: true, completion: nil)
    }
    
    @objc func warehouseDetailClicked(_ sender: UIButton){
        print("groupArrowClicked \(sender.tag)")
        
        let warehouseDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailWarehouseViewController") as! DetailWarehouseViewController
//        warehouseDetailVC.warehouseID = arrCurrentWarehouses[sender.tag].masterId
//        warehouseDetailVC.warehouseName = arrCurrentWarehouses[sender.tag].descriptionField
        self.navigationController?.pushViewController(warehouseDetailVC, animated: true)
    }
    
}

extension ManageWarehouseViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WarehouseListCell") as! WarehouseListCell
        
      //  let dictData = arrCurrentWarehouses[indexPath.row]

      //  cell.warehouseLbl.text = dictData.descriptionField
        cell.editBtn.addTarget(self, action: #selector(warehouseEditClicked(_:)), for: .touchUpInside)
        cell.editBtn.tag = indexPath.row

        cell.detailBtn.addTarget(self, action: #selector(warehouseDetailClicked(_:)), for: .touchUpInside)
        cell.detailBtn.tag = indexPath.row
        
        return cell
    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
//
//        headerView.lblTitle.text = headerArray[section]
//
//        headerView.btnOpen.isHidden = true
//
//        return headerView
//
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    
}


class WarehouseListCell : UITableViewCell{
    @IBOutlet weak var warehouseLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var detailBtn: UIButton!
    
}


//
//  UpdateWarehouseViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class UpdateWarehouseViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var txtNAem: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var btnCheckein: UIButton!
    
    var str_W_Name = ""
    var str_W_Code = ""
    var str_MasterID = ""

    var isCanRegister = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtNAem.text = str_W_Name
        self.txtCode.text = str_W_Code
        
        if isCanRegister == true
        {
            isCanRegister = true
            btnCheckein.isSelected = true
            btnCheckein.setImage(UIImage(named: "check-1"), for: .normal)
        }
        else
        {
            isCanRegister = false
            btnCheckein.isSelected = false
            btnCheckein.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        
        mainView.clipsToBounds = true
        mainView.cornerRadius = 15
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        
        if txtNAem.text == ""
        {
            self.view.makeToast("Please enter warehouse name")
        }
        else if txtCode.text == ""
        {
            self.view.makeToast("Please enter warehouse code")
        }
        else
        {
            callUpdateWarehouseAPI()
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkBtnClicked(_ sender: UIButton) {
        if sender.isSelected {
            isCanRegister = false
            sender.isSelected = false
            sender.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        else
        {
            isCanRegister = true
            
            sender.isSelected = true
            sender.setImage(UIImage(named: "check-1"), for: .normal)
            
        }
    }
    
    func callUpdateWarehouseAPI()
    {
        
        var param = [String : Any]()
        
        param = ["masterId": str_MasterID,"description":self.txtNAem.text!,"code":self.txtCode.text!,"isActive":true,"version":0,"documentExtension":"","documentPath":"","canRegister":isCanRegister]

        APIClient.sharedInstance.MakeAPICallWithAuthOutHeaderPutWarehours(CREATE_WAREHOUESE,user_ID: appDelegate.dicLoginDetails?.userId ?? 0, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2001
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

//
//  CreateWarehouseViewController.swift
//  Operations
//
//  Created by iMac on 11/03/22.
//

import UIKit

class CreateWarehouseViewController: UIViewController {

    @IBOutlet weak var txtWarehouseName: UITextField!
    @IBOutlet weak var txtWarehouseCode: UITextField!
    @IBOutlet weak var checkboxBtn: UIButton!
    
    var iscanRegister = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
   
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func checkBoxClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            iscanRegister = false
            sender.isSelected = false
            checkboxBtn.setImage(UIImage(named: "unCheck"), for: .normal)
        }
        else
        {
            iscanRegister = true
            sender.isSelected = true
            checkboxBtn.setImage(UIImage(named: "check-1"), for: .normal)
            
        }
        
    }
    @IBAction func createWarehouseClicked(_ sender: Any) {
        
        if txtWarehouseName.text == ""
        {
            self.view.makeToast("Please enter warehouse name")
        }
        else if txtWarehouseCode.text == ""
        {
            self.view.makeToast("Please enter warehouse code")
        }
        else
        {
            callCreateWarehouseAPI()
        }
        
    }
    
    func callCreateWarehouseAPI()
    {
        
        var param = [String : Any]()
        
        param = ["masterId": "0","description":self.txtWarehouseName.text!,"code":self.txtWarehouseCode.text!,"isActive":true,"version":0,"documentExtension":"","documentPath":"","canRegister":iscanRegister]

        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(CREATE_WAREHOUESE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2000
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

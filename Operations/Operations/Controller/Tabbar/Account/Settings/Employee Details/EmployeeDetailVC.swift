//
//  EmployeeDetailVC.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit
import GSImageViewerController

class EmployeeDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    
    @IBOutlet var tblView: UITableView!
    var arrSection = ["PERSONAL DETAILS", "ADDRESS DETAILS", "FAMILY DETAILS", "QUALIFICATION DETAILS", "UPLOADED DOCUMENTS", "ADMIN ACTIONS"]
    var arrDoc = ["AADHAAR", "NEWW", "Phd", "12th"]
    var userId = Int()
    
    var dicUserDetails = OPUserDetails()
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callUserDetailsAPI()
        
        tblView.register(UINib(nibName: "PersonalCell", bundle: nil), forCellReuseIdentifier: "PersonalCell")
        tblView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        tblView.register(UINib(nibName: "FamilyCell", bundle: nil), forCellReuseIdentifier: "FamilyCell")
        tblView.register(UINib(nibName: "QualificationCell", bundle: nil), forCellReuseIdentifier: "QualificationCell")
        tblView.register(UINib(nibName: "DocumentCell", bundle: nil), forCellReuseIdentifier: "DocumentCell")
        tblView.register(UINib(nibName: "ButtonsCell", bundle: nil), forCellReuseIdentifier: "ButtonsCell")
        tblView.register(UINib(nibName: "DetailsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "DetailsHeaderView")
        
    }
    
    //MARK: - Functions
    
    //MARK: - TableView Delegate & DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return dicUserDetails.relationDetail.count
        } else if section == 3 {
            return dicUserDetails.qualification.count
        } else if section == 4 {
            return dicUserDetails.document.count
        }  else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "PersonalCell") as! PersonalCell
            
            cell.lblNAme.text = dicUserDetails.fullName.uppercased()
            cell.lblEmail.text = dicUserDetails.email ?? ""
            cell.lblUserId.text = dicUserDetails.employeeId
            cell.lblMobile.text = dicUserDetails.mobileNumber
            cell.lblAdharNo.text = dicUserDetails.aadharNumber
            cell.lblLocation.text = dicUserDetails.wareHouse
            
            if dicUserDetails.profileImageByteArray != ""
            {
                cell.imgPro.image = self.convertBase64ToImage(imageString: dicUserDetails.profileImageByteArray)

            }
            
            return cell
    
        } else if indexPath.section == 1 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "AddressCell")  as! AddressCell
            
            let dictAddress = dicUserDetails.address
            cell.lblPincode.text = dictAddress?.pinCode ?? ""
            cell.lblPermanent.text = dictAddress?.permanentAddress
            cell.lblResidential.text = dictAddress?.residentialAddress
            cell.lblHomeTown.text = dictAddress?.homeTown
            
            return cell
            
        } else if indexPath.section == 2 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "FamilyCell") as! FamilyCell
            
            let dicdata = dicUserDetails.relationDetail[indexPath.row]
            
            cell.lblAge.text = "\(dicdata.relationAge ?? 0)"
            cell.lblProfession.text = dicdata.profession ?? ""
            cell.lblRelationship.text = dicdata.relationDescription ?? ""
            cell.lblRelative.text = dicdata.relativeName ?? ""
            
            return cell
            
        } else if indexPath.section == 3 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "QualificationCell")  as! QualificationCell
            
            let dicdata = dicUserDetails.qualification[indexPath.row]
            
            cell.lblMark.text = "\(dicdata.percentage ?? 0.0)"
            cell.lblQulification.text = dicdata.qualificationName
            
            return cell
            
        } else if indexPath.section == 4 {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "DocumentCell")  as! DocumentCell
            
            let dicdata = dicUserDetails.document[indexPath.row]
            
            cell.lblTitle.text = dicdata.documentName
            
//            cell.lblTitle.text = arrDoc[indexPath.row]
            return cell
        } else {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "ButtonsCell")  as! ButtonsCell
            
            
            cell.btnEditDetails.addTarget(self, action: #selector(clickedGotoEditDetails(_ :)), for: .touchUpInside)
            
            cell.btnAssignGroup.addTarget(self, action: #selector(clickedAssignDetail(_:)), for: .touchUpInside)
            
            cell.btnDeactiveUser.addTarget(self, action: #selector(clickedDeactivateUser(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    @objc func clickedGotoEditDetails(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditDetailsViewController") as! EditDetailsViewController
        vc.dicUserDetails = self.dicUserDetails
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickedAssignDetail(_ sender: UIButton)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AssignGroupViewController") as! AssignGroupViewController
        vc.userID = dicUserDetails.userId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func clickedDeactivateUser(_ sender: UIButton)
    {
        //  Confirmation Alert
        let popupView = UIAlertController(title: "Deactivate User", message: "Are you sure you want to deactivate this user?", preferredStyle: .alert)
        
        // Create YES button with action handler
        let yes = UIAlertAction(title: "YES", style: .default, handler: { (action) -> Void in
            print("YES button tapped")
            
            self.callUserDeactivateAPI()
            
        })
        
        // Create YES button with action handler
        let no = UIAlertAction(title: "NO", style: .default, handler: { (action) -> Void in
            print("NO button tapped")
        })
        
        popupView.addAction(no)
        popupView.addAction(yes)
        // Present Alert to
        self.present(popupView, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 4
        {
            let dicdata = dicUserDetails.document[indexPath.row]
            
            
            callDownloadTempAPI(str_path: dicdata.documentPath)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tblView.dequeueReusableHeaderFooterView(withIdentifier: "DetailsHeaderView")  as! DetailsHeaderView
        view.lblTitle.text = arrSection[section]
        
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 255
        } else if indexPath.section == 1 {
            return 160
        } else if indexPath.section == 2 {
            return 160
        } else if indexPath.section == 3 {
            return 80
        } else if indexPath.section == 4 {
            return 40
        } else {
            return 100
        }
    }
    
    //MARK: - CollectionView Delegate & DataSource
    
    //MARK: - IBActions
    
    @IBAction func TapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - API Calling
    
    
    func callUserDetailsAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary(GET_USERS_DETAILS + "\(userId)", parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    self.dicUserDetails = OPUserDetails(fromDictionary: response!)
                }
                self.tblView.delegate = self
                self.tblView.dataSource = self
                self.tblView.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callDownloadTempAPI(str_path: String){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionaryDownloandHeader(DOWNLOAND_TEMP, path: str_path, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                let data = response?.value(forKey: "data") as? String
                if data != nil
                {
                    
                    let img_Downoad = self.convertBase64ToImage(imageString: data ?? "")
                    
//                    let imageView = UIImageView()
//                    imageView.image = img_Downoad
//                    imageView.setupImageViewer()
                    
                    let imageInfo   = GSImageInfo(image: img_Downoad, imageMode: .aspectFit)
                    let imageViewer = GSImageViewerController(imageInfo: imageInfo)
                                        
                    self.present(imageViewer, animated: true, completion: nil)
 
                    
                    //                    UIImageWriteToSavedPhotosAlbum(img_Downoad, nil, nil, nil)
                    //
                    //                    self.view.makeToast("Document successfully downloaded")
                }
                
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString,
                             options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    func callUserDeactivateAPI()
    {
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPutUSer(DEACTIVATE, user_ID: "\(userId)", deactivate_By: "\(userId)") { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    let code = response?.value(forKey: "code") as? Int
                    let message = response?.value(forKey: "message") as? String
                    
                    if code == 2001
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        vc.str_Name = message!
                        self.present(vc, animated: true, completion: nil)
                    }
                    else
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                        vc.str_Title = message!
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
}

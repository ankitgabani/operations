//
//  SearchEmployeeVC.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class SearchEmployeeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var collView: UICollectionView!
    @IBOutlet weak var viewSearchTop: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var btnCancle: UIButton!
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        _flowLayout.itemSize = CGSize(width: 130, height: 35)
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 2
        return _flowLayout
    }
    var arrWarehouses = [SearchEmployee]()
    var arrUsers = [UsersDataModel]()
    var arrFilteredUsers = [UsersDataModel]()
    var selectedIndex = -1
    var isSearching = false
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        txtSearch.delegate = self
        
        self.tblView.isHidden = true
        
        
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
            
            
        }
        else
        {
            imgCancel.isHidden = false
           

        }
        
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        

        collView.delegate = self
        collView.dataSource = self
        
        collView.register(UINib(nibName: "SearchEmployeeCell", bundle: nil), forCellWithReuseIdentifier: "SearchEmployeeCell")
        tblView.delegate = self
        tblView.dataSource = self
        
        callGetWarehousesAPI()
        
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField)
    {
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
            
        }
        else
        {
            imgCancel.isHidden = false
        }
    }
    
    
    //MARK: - Functions
    
    //MARK: - TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return arrFilteredUsers.count
        }else{
            return arrUsers.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(withIdentifier: "SearchEmployeCell") as! SearchEmployeCell
        
        if isSearching{
            cell.lblCode.text = arrFilteredUsers[indexPath.row].employeeId
            cell.lblName.text = arrFilteredUsers[indexPath.row].fullName.uppercased()
            cell.lblGmail.text = arrFilteredUsers[indexPath.row].email
            cell.lblInitialLetter.text = arrFilteredUsers[indexPath.row].nameInitials.uppercased()
            cell.imgView.backgroundColor = .random()
            
        }
        
        else{
            cell.lblCode.text = arrUsers[indexPath.row].employeeId
            cell.lblName.text = arrUsers[indexPath.row].fullName.uppercased()
            cell.lblGmail.text = arrUsers[indexPath.row].email
            cell.lblInitialLetter.text = arrUsers[indexPath.row].nameInitials.uppercased()
            cell.imgView.backgroundColor = .random()
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let employeeDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "EmployeeDetailVC") as! EmployeeDetailVC
        self.navigationController?.navigationBar.isHidden = true
        if isSearching{
            employeeDetailVC.userId = arrFilteredUsers[indexPath.row].userId
        }else{
            employeeDetailVC.userId = arrUsers[indexPath.row].userId
        }
        
        self.navigationController?.pushViewController(employeeDetailVC, animated: true)
    }
    
    
    //MARK: - CollectionView Delegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWarehouses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collView.dequeueReusableCell(withReuseIdentifier: "SearchEmployeeCell", for: indexPath) as! SearchEmployeeCell
        cell.lblName.text = arrWarehouses[indexPath.row].descriptionName
        
        DispatchQueue.main.async {
            cell.viewain.layer.cornerRadius = cell.viewain.frame.height / 2
            cell.viewain.clipsToBounds = true
        }
        
        if selectedIndex == indexPath.row
        {
            cell.viewain.backgroundColor = .black
            cell.lblName.textColor = .white
            
        }
        else
        {
            cell.viewain.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
            cell.lblName.textColor = .black
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collView.reloadData()
        callGetUsersAPI(masterID: arrWarehouses[indexPath.row].masterId)
    }
    
    //MARK: - IBActions
    
    @IBAction func TapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedClrea(_ sender: Any) {
        txtSearch.text = ""
        imgCancel.isHidden = true
        btnCancle.isHidden = true
    }
    //MARK: - API Calling
    func callGetWarehousesAPI(){
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(SEARCH_EMPLOYEE, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.arrWarehouses.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = SearchEmployee(fromDictionary: obj as! NSDictionary)
                        self.arrWarehouses.append(dicData)
                        
                    }
                    
                    self.collView.reloadData()
                    
                }
                else
                {
                    self.arrWarehouses.removeAll()
                    self.collView.reloadData()
                }
            }
            else
            {
                self.arrWarehouses.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    func callGetUsersAPI(masterID: Int){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_USERS + "=\(masterID)", parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.tblView.isHidden = false
                self.arrUsers.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = UsersDataModel(fromDictionary: obj as! NSDictionary)
                        self.arrUsers.append(dicData)
                    }
                    
                    self.tblView.reloadData()
                    
                }
                else
                {
                    self.arrUsers.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrUsers.removeAll()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
}

extension SearchEmployeeVC: UITextFieldDelegate{
    
    func filterContentForSearchText(_ searchText: String) {
        
        arrFilteredUsers = arrUsers.filter({ (obj: UsersDataModel) -> Bool in
            let nameMatch = obj.fullName!.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            let idMatch = obj.employeeId!.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            let emailMatch = obj.email!.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return nameMatch != nil || idMatch != nil || emailMatch != nil}
        )}
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text != "" {
            if let searchText = textField.text {
                isSearching = true
                filterContentForSearchText(searchText)
                tblView.reloadData()  // replace current table view with search results table view
            }
        }else{
            isSearching = false
            arrFilteredUsers.removeAll()
            tblView.reloadData()
        }
    }
}


extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(
            red:   .random(),
            green: .random(),
            blue:  .random(),
            alpha: 1.0
        )
    }
}


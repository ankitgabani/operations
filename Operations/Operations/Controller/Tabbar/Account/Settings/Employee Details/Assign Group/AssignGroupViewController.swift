//
//  AssignGroupViewController.swift
//  Operations
//
//  Created by iMac on 10/03/22.
//

import UIKit

class AssignGroupViewController: UIViewController {
    
    @IBOutlet weak var groupsTableView: UITableView!
    
    var headerArray = ["Assign groups","Assign Permission"]
    
    var arrGroupData = NSMutableArray()
    var arrPermissionData: [OPUserPermissionsPermission] = [OPUserPermissionsPermission]()
    
    var hiddenSections = Set<Int>()
    var userID : Int?
    var dicUserRole = OPUserRole()
    var dicUserPermission = OPUserPermissions()
    
    var arrPermission = NSMutableArray()
    var dic = NSMutableDictionary()
    var dicTop = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callUserRoleAPI()
    }
    
    //MARK: - API Calling
    
    func callAssignRolesAPI()
    {
        
        var param = [String : Any]()
        
        let arrPermission = NSMutableArray()
        let arrayGroupData = NSMutableArray()
        
        for index in 0..<self.arrPermissionData.count{
            
            let permission = self.arrPermissionData[index]
            
            let dic = NSMutableDictionary()
            dic.setValue(false, forKey: "disableOn")
            dic.setValue(permission.isSelected, forKey: "isSelected")
            dic.setValue(permission.permissionCode, forKey: "permissionCode")
            dic.setValue(permission.permissionId, forKey: "permissionId")
            dic.setValue(permission.permissionName, forKey: "permissionName")
            
            arrPermission.add(dic)
        }
        
        
        
        let arrGroup = self.dicUserRole.groups
        
        for index in 0..<arrGroup!.count
        {
            
            let group = arrGroup![index]
            
            let dic = NSMutableDictionary()
            dic.setValue(group.groupId, forKey: "groupId")
            dic.setValue(group.groupName, forKey: "groupName")
            
            if group.isGroupSelected == true
            {
                if group.isDefault{
                    dic.setValue(1, forKey: "isDefault")
                    
                }
                else{
                    dic.setValue(0, forKey: "isDefault")
                    
                }
                
                if group.isGroupSelected{
                    dic.setValue(1, forKey: "isGroupSelected")
                    
                }
                else{
                    dic.setValue(0, forKey: "isGroupSelected")
                    
                }
                
                let permissions = dicUserPermission.permissions
                
                let arrpermission = group.permissions!
                
                let arrayP = NSMutableArray()
                
                for j in 0..<arrpermission.count{
                    
                    let permission = arrpermission[j]
                    let type2Array = permissions?.filter(){ $0.permissionId == permission.permissionId}
                    
                    if type2Array!.count > 0{
                        
                        let dicPermission = NSMutableDictionary()
                        let dicdata = type2Array?.first
                        if dicdata!.isSelected == true
                        {
                            
                            dicPermission.setValue(true, forKey: "isSelected")
                            
                        }
                        else
                        {
                            
                            if self.dic.value(forKey: "\(dicdata!.permissionId!)") != nil{
                                
                                let value = self.dic.value(forKey: "\(dicdata!.permissionId!)") as! Bool
                                
                                if value == true{
                                    dicPermission.setValue(true, forKey: "isSelected")
                                    
                                    
                                }
                                else{
                                    dicPermission.setValue(false, forKey: "isSelected")
                                    
                                    
                                }
                            }
                            else{
                                
                                dicPermission.setValue(false, forKey: "isSelected")
                                
                                
                            }
                        }
                        
                        
                        dicPermission.setValue(false, forKey: "disableOn")
                        dicPermission.setValue(permission.permissionCode, forKey: "permissionCode")
                        dicPermission.setValue(permission.permissionId, forKey: "permissionId")
                        dicPermission.setValue(permission.permissionName, forKey: "permissionName")
                        
                        arrayP.add(dicPermission)
                    }
                    
                    
                }
                dic.setValue(arrayP, forKey: "permissions")
                arrayGroupData.add(dic)
                
            }
            
        }
        
        param = ["groups": arrayGroupData,"permissions": arrPermission,"userId":"\(userID ?? 0)"]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(ASSIGN_ROLES, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String
                
                if code == 2000
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callUserRoleAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary("users/\(userID ?? 0)" + GET_USERS_ROLE , parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                
                let arrgroups = response?.value(forKey: "groups") as? NSArray
                
                self.arrGroupData = (arrgroups?.mutableCopy() as? NSMutableArray)!
                
                if response != nil
                {
                    self.dicUserRole = OPUserRole(fromDictionary: response!)
                }
                
                
                
                
                for i in 0..<self.dicUserRole.groups!.count{
                    
                    let dicdata = self.dicUserRole.groups[i]
                    
                    if dicdata.permissions.count > 0{
                        
                        for index in 0..<dicdata.permissions.count{
                            
                            let permission = dicdata.permissions[index]
                                 
                            if dicdata.isGroupSelected == true && dicdata.isDefault == false{
                                self.dic.setValue(true, forKey: "\(permission.permissionId!)")
                              // true
                                
                            }
                            else if dicdata.isGroupSelected == false && dicdata.isDefault == false{
                                
                              // false
                                
                            }
                            else if dicdata.isGroupSelected == false && dicdata.isDefault == true{
                                
                              // true
                                self.dic.setValue(true, forKey: "\(permission.permissionId!)")
                            }
                        }
                        self.groupsTableView.reloadData()
                    }
                }
                
                
                
                self.callUserPermissionaAPI()
                
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callUserPermissionaAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary("users/\(userID ?? 0)" + GET_USERS_PERMISSIONS , parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    let arrpermissions = response?.value(forKey: "permissions") as? NSArray
                    
                    for obj in arrpermissions!
                    {
                        let dicData = OPUserPermissionsPermission(fromDictionary: obj as! NSDictionary)
                        
                        if dicData.isSelected == true
                        {
                            self.arrPermissionData.append(dicData)
                        }
                        
                    }
                    
                    self.dicUserPermission = OPUserPermissions(fromDictionary: response!)
                    
                    
                    
                }
                self.groupsTableView.delegate = self
                self.groupsTableView.dataSource = self
                
                self.groupsTableView.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        print("doneBtnClicked")
        
        callAssignRolesAPI()
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        print("backBtnClicked")
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnAssignGroupClicked(_ sender: UIButton){
        print("btnAssignGroupClicked \(sender.tag)")
    }
    
    @objc func btnAssignPermissionClicked(_ sender: UIButton){
        print("btnAssignPermissionClicked \(sender.tag)")
    }
    
    @objc func assignGroupArrowClicked(_ sender: UIButton){
        print("assignGroupArrowClicked \(sender.tag)")
    }
    
    @objc func assignGroupSwitchClicked(_ sender: UISwitch){
        print("assignGroupSwitchClicked \(sender.tag)")
        
        //self.dicTop = NSMutableDictionary()
        // self.dic = NSMutableDictionary()
        
        if sender.isOn
        {
            
            var cell = sender.superview?.superview?.superview as! AsssignGroupCell
            
            let indexPath = self.groupsTableView.indexPath(for: cell)
            
            self.dicTop.setObject(true, forKey: indexPath as! NSCopying)
            
            let dicdata = dicUserRole.groups[indexPath!.row]
            dicdata.isGroupSelected  = true
            
            if dicdata.permissions.count > 0{
                
                for index in 0..<dicdata.permissions.count{
                    
                    let permission = dicdata.permissions[index]
                    
                    self.dic.setValue(true, forKey: "\(permission.permissionId!)")
                }
                
                self.groupsTableView.reloadData()
                
            }
        }
        else{
            
            var cell = sender.superview?.superview?.superview as! AsssignGroupCell
            
            let indexPath = self.groupsTableView.indexPath(for: cell)
            
            self.dicTop.setObject(false, forKey: indexPath as! NSCopying)
            
            let dicdata = dicUserRole.groups[indexPath!.row]
            dicdata.isGroupSelected  = false
            
            if dicdata.permissions.count > 0{
                
                for index in 0..<dicdata.permissions.count{
                    
                    let permission = dicdata.permissions[index]
                    
                    self.dic.setValue(false, forKey: "\(permission.permissionId!)")
                }
                
                self.groupsTableView.reloadData()
                
            }
            
        }
        
        
    }
    
    @objc func assignPermissionSwitchClicked(_ sender: UISwitch){
        print("assignPermissionSwitchClicked \(sender.tag)")
        
        let dicdata = dicUserPermission.permissions[sender.tag]
        
        if sender.isOn == true
        {
            dicdata.isSelected = true
            self.arrPermissionData.append(dicdata)
            
        }
        else
        {
            let type2Array = self.arrPermissionData.filter(){ $0.permissionId == dicdata.permissionId}
            
            let index = self.arrPermissionData.firstIndex(of: type2Array.first!)
            
            dicdata.isSelected = false
            self.arrPermissionData.remove(at: index!)
            
        }
        
    }
    
    
}
extension AssignGroupViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return dicUserRole.groups.count
        }else{
            return dicUserPermission.permissions.count+1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AsssignGroupCell") as! AsssignGroupCell
            
            let dicdata = dicUserRole.groups[indexPath.row]
            
            cell.groupLbl.text = dicdata.groupName
            cell.arrowBtn.addTarget(self, action: #selector(assignGroupArrowClicked(_:)), for: .touchUpInside)
            cell.groupSwitch.addTarget(self, action: #selector(assignGroupSwitchClicked(_:)), for: .valueChanged)
            cell.groupSwitch.tag = indexPath.row
            cell.arrowBtn.tag = indexPath.row
            
            if dicdata.isGroupSelected == true && dicdata.isDefault == true
            {
                cell.groupSwitch.isUserInteractionEnabled = false
                
                cell.groupSwitch.isOn = true
                cell.groupSwitch.onTintColor = UIColor(red: 0/255, green: 191/255, blue: 87/255, alpha: 1)
                self.dicTop.setObject(true, forKey: indexPath as! NSCopying)
                
                //                let dicdata = dicUserRole.groups[indexPath.row]
                //
                //                if dicdata.permissions.count > 0{
                //
                //                    for index in 0..<dicdata.permissions.count{
                //
                //                        let permission = dicdata.permissions[index]
                //
                //                        self.dic.setValue(true, forKey: "\(permission.permissionId!)")
                //                    }
                //
                //                  // self.groupsTableView.reloadData()
                //                }
            }
            else
            {
                if dicdata.isGroupSelected == true && dicdata.isDefault == false{
                    
                    cell.groupSwitch.onTintColor = UIColor.black
                    cell.groupSwitch.layer.cornerRadius = 16
                    
                    
                    cell.groupSwitch.setOn(true, animated: true)
                    cell.groupSwitch.isUserInteractionEnabled = true
                    
                }
                else if dicdata.isGroupSelected == false && dicdata.isDefault == false{
                    
                    
                    cell.groupSwitch.setOn(false, animated: true)
                    cell.groupSwitch.isUserInteractionEnabled = true
                    
                }
                else if dicdata.isGroupSelected == false && dicdata.isDefault == true{
                    
                    cell.groupSwitch.onTintColor = UIColor(red: 0/255, green: 191/255, blue: 87/255, alpha: 1)
                    cell.groupSwitch.isUserInteractionEnabled = false
                    cell.groupSwitch.isOn = true
                    
                }
                
            }
            
            //  cell.groupSwitch.onTintColor = UIColor(red: 0/255, green: 191/255, blue: 87/255, alpha: 1)
            
            //            if self.dicTop.object(forKey: indexPath) != nil{
            //
            //                let value = self.dicTop.object(forKey: indexPath) as! Bool
            //
            //                if value == true{
            //                    cell.groupSwitch.isOn = true
            //
            //                }
            //                else{
            //
            //                    cell.groupSwitch.isOn = false
            //                }
            //
            //            }
            //            else{
            //
            //                cell.groupSwitch.isOn = false
            //            }
            return cell
            
        }else{
            
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "searchViewTableCell") as! searchViewTableCell
                cell.searchBar.delegate = self
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AsssignPermissionCell") as! AsssignPermissionCell
                
                let dicdata = dicUserPermission.permissions[indexPath.row-1]
                
                cell.permissionLbl.text = dicdata.permissionName
                cell.groupSwitch.tag = indexPath.row-1
                
                cell.groupSwitch.addTarget(self, action: #selector(assignPermissionSwitchClicked(_:)), for: .valueChanged)
                
                if dicdata.permissionName == "ASM AND TL Training"{
                    
                    print("found")
                }
                
                if dicdata.isSelected == true
                {
                    cell.groupSwitch.isOn = dicdata.isSelected
                    cell.groupSwitch.isUserInteractionEnabled = true
                    cell.groupSwitch.onTintColor = .black
                    
                }
                else
                {
                    //cell.groupSwitch.isOn = dicdata.isSelected
                    
                    if self.dic.value(forKey: "\(dicdata.permissionId!)") != nil{
                        
                        let value = self.dic.value(forKey: "\(dicdata.permissionId!)") as! Bool
                        
                        if value == true{
                            cell.groupSwitch.setOn(true, animated: true)
                            cell.groupSwitch.isUserInteractionEnabled = false
                            cell.groupSwitch.onTintColor = UIColor(red: 0/255, green: 191/255, blue: 87/255, alpha: 1)
                            
                        }
                        else{
                            cell.groupSwitch.setOn(false, animated: true)
                            cell.groupSwitch.isUserInteractionEnabled = true
                            cell.groupSwitch.onTintColor = .black
                            
                        }
                    }
                    else{
                        
                        cell.groupSwitch.setOn(false, animated: true)
                        cell.groupSwitch.isUserInteractionEnabled = true
                        cell.groupSwitch.onTintColor = .black
                        
                    }
                }
                
                
                return cell
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0{
            let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
            headerView.lblTitle.text = headerArray[section]
            headerView.btnOpen.addTarget(self, action: #selector(btnAssignGroupClicked(_:)), for: .touchUpInside)
            return headerView
        }else{
            
            let headerView = Bundle.main.loadNibNamed("AssignGroupHeaderView", owner: self, options: [:])?.first as! AssignGroupHeaderView
            headerView.lblTitle.text = headerArray[section]
            headerView.btnOpen.addTarget(self, action: #selector(btnAssignPermissionClicked(_:)), for: .touchUpInside)
            return headerView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
}


extension AssignGroupViewController: UITextFieldDelegate{
    
    
}

class AsssignGroupCell : UITableViewCell{
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupLbl: UILabel!
    @IBOutlet weak var groupSwitch: UISwitch!
    @IBOutlet weak var arrowBtn: UIButton!
}

class AsssignPermissionCell : UITableViewCell{
    @IBOutlet weak var permissionLbl: UILabel!
    @IBOutlet weak var groupSwitch: UISwitch!
}

class searchViewTableCell: UITableViewCell{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchBar: UITextField!
    
}

//
//  DetailsHeaderView.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class DetailsHeaderView: UITableViewHeaderFooterView {

    @IBOutlet var lblTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

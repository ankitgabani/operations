//
//  FamilyCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class FamilyCell: UITableViewCell {

    @IBOutlet var lblRelationship: UILabel!
    @IBOutlet var lblRelative: UILabel!
    @IBOutlet var lblAge: UILabel!
    @IBOutlet var lblProfession: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  SearchEmployeCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class SearchEmployeCell: UITableViewCell {

    @IBOutlet var lblGmail: UILabel!
    @IBOutlet var lblCode: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet weak var lblInitialLetter: UILabel!
    @IBOutlet var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

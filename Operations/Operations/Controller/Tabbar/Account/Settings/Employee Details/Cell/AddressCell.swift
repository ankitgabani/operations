//
//  AddressCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet var lblPermanent: UILabel!
    @IBOutlet var lblPincode: UILabel!
    @IBOutlet var lblHomeTown: UILabel!
    @IBOutlet var lblResidential: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

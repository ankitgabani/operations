//
//  ButtonsCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class ButtonsCell: UITableViewCell {

    @IBOutlet weak var btnEditDetails: UIButton!
    
    @IBOutlet weak var btnAssignGroup: UIButton!
    
    @IBOutlet weak var btnDeactiveUser: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

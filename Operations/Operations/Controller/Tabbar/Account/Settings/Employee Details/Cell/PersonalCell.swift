//
//  PersonalCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class PersonalCell: UITableViewCell {

    @IBOutlet var imgPro: UIImageView!

    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblNAme: UILabel!
    @IBOutlet weak var lblUserId: UILabel!
    @IBOutlet var lblAdharNo: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblMobile: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

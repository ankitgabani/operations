//
//  AllQueriesViewController.swift
//  Operations
//
//  Created by iMac on 14/03/22.
//

import UIKit

class AllQueriesViewController: UIViewController {

    private enum Constants {
        static let underlineViewColor: UIColor = .white
        static let underlineViewHeight: CGFloat = 2
    }

    @IBOutlet weak var segmentedControlContainerView: UIView!
    @IBOutlet weak var allQueriesTableView: UITableView!
    
    var arrAllQueries = OPAllQueries()
    
    // Customised segmented control
    private lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = UISegmentedControl()

        // Remove background and divider colors
        segmentedControl.backgroundColor = .black
        segmentedControl.tintColor = .black
        segmentedControl.selectedSegmentTintColor = .clear
        
        // Append segments
        segmentedControl.insertSegment(withTitle: "OPEN", at: 0, animated: true)
        segmentedControl.insertSegment(withTitle: "CLOSE", at: 1, animated: true)
        // Select first segment by default
        segmentedControl.selectedSegmentIndex = 0

        // Change text color and the font of the NOT selected (normal) segment
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .regular)], for: .normal)

        // Change text color and the font of the selected segment
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .bold)], for: .selected)

        // Set up event handler to get notified when the selected segment changes
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)

        // Return false because we will set the constraints with Auto Layout
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()

    // The underline view below the segmented control
    private lazy var bottomUnderlineView: UIView = {
        let underlineView = UIView()
        underlineView.backgroundColor = Constants.underlineViewColor
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        return underlineView
    }()

    private lazy var leadingDistanceConstraint: NSLayoutConstraint = {
        return bottomUnderlineView.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor)
    }()

    var selectedIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add subviews to the view hierarchy
        // (both segmentedControl and bottomUnderlineView are subviews of the segmentedControlContainerView)
        view.addSubview(segmentedControlContainerView)
        segmentedControlContainerView.addSubview(segmentedControl)
        segmentedControlContainerView.addSubview(bottomUnderlineView)

        // Constrain the segmented control to the container view
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: segmentedControlContainerView.topAnchor),
            segmentedControl.leadingAnchor.constraint(equalTo: segmentedControlContainerView.leadingAnchor),
            segmentedControl.centerXAnchor.constraint(equalTo: segmentedControlContainerView.centerXAnchor),
            segmentedControl.centerYAnchor.constraint(equalTo: segmentedControlContainerView.centerYAnchor)
            ])

        // Constrain the underline view relative to the segmented control
        NSLayoutConstraint.activate([
            bottomUnderlineView.bottomAnchor.constraint(equalTo: segmentedControl.bottomAnchor),
            bottomUnderlineView.heightAnchor.constraint(equalToConstant: Constants.underlineViewHeight),
            leadingDistanceConstraint,
            bottomUnderlineView.widthAnchor.constraint(equalTo: segmentedControl.widthAnchor, multiplier: 1 / CGFloat(segmentedControl.numberOfSegments))
            ])
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callGetAllQueriesAPI()
    }

    @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        changeSegmentedControlLinePosition()
    }

    func callGetAllQueriesAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary(GET_ALL_QUERIES, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    self.arrAllQueries = OPAllQueries(fromDictionary: response!)
                }
                self.allQueriesTableView.delegate = self
                self.allQueriesTableView.dataSource = self
                self.allQueriesTableView.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    
    // Change position of the underline
    private func changeSegmentedControlLinePosition() {
        selectedIndex = segmentedControl.selectedSegmentIndex
        
        let segmentWidth = segmentedControl.frame.width/CGFloat(segmentedControl.numberOfSegments)
        
        let leadingDistance = Int(segmentWidth) * (selectedIndex)
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.leadingDistanceConstraint.constant = CGFloat(leadingDistance)
            self?.view.layoutIfNeeded()
        })
        allQueriesTableView.reloadData()
    }

    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AllQueriesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 0{
           return  2 //arrAllQueries.openQueries.count
        }else{
           return 1 // arrAllQueries.closeQueries.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllQueriesTableViewCell") as! AllQueriesTableViewCell
        
        if selectedIndex == 0{
//            let dicData = arrAllQueries.openQueries[indexPath.row]
//            cell.lblStatus.text = "Status: \(dicData.queryStatus ?? "")"
//            cell.lblDate.text = dicData.queryDate
//            cell.lblSentTo.text = "Sent To: \(dicData.querySendTo ?? "")"
//            cell.lblSentBy.text = "Sent By: \(dicData.querySendBy ?? "")"
//            cell.lblQueryNumber.text = "Query: #\(dicData.queryId ?? 0)"
//            cell.lblOpenDays.isHidden = false
//            cell.lblOpenDays.text = dicData.queryOpenDays
//            cell.mainView.backgroundColor = UIColor(red: 242/255, green: 220/255, blue: 144/255, alpha: 1)
        }else{
//            let dicData = arrAllQueries.closeQueries[indexPath.row]
//            cell.lblStatus.text = "Status: \(dicData.queryStatus ?? "")"
//            cell.lblDate.text = dicData.queryDate
//            cell.lblSentTo.text = "Sent To: \(dicData.querySendTo ?? "")"
//            cell.lblSentBy.text = "Sent By: \(dicData.querySendBy ?? "")"
//            cell.lblQueryNumber.text = "Query: #\(dicData.queryId ?? 0)"
//            cell.lblOpenDays.isHidden = true
//            cell.mainView.backgroundColor = UIColor(red: 151/255, green: 202/255, blue: 141/255, alpha: 1)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let QueryDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "QueryDetailsViewController") as! QueryDetailsViewController
        
        if selectedIndex == 0{
          //  QueryDetailsVC.queryID = arrAllQueries.openQueries[indexPath.row].queryId
        }else{
           // QueryDetailsVC.queryID = arrAllQueries.closeQueries[indexPath.row].queryId
        }
        self.navigationController?.pushViewController(QueryDetailsVC, animated: true)
    }
}



class AllQueriesTableViewCell: UITableViewCell{
    
    @IBOutlet weak var lblQueryNumber: UILabel!
    @IBOutlet weak var lblSentTo: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSentBy: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblOpenDays: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

//
//  CloseQueryViewController.swift
//  Operations
//
//  Created by iMac on 14/03/22.
//

import UIKit

class CloseQueryViewController: UIViewController {

    @IBOutlet weak var remarksTxtField: UITextField!
    @IBOutlet weak var mainView: UIView!
    
    var str_actionId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.clipsToBounds = true
        mainView.cornerRadius = 15
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeQueryBtnClicked(_ sender: Any) {
        
        if remarksTxtField.text == ""
        {
            self.view.makeToast("Please enter remarks.")
        }
        else
        {
            callQueryActionAPI()
        }
        
    }
    
    func callQueryActionAPI()
    {
        
        var param = [String : Any]()
        
        param = ["actionBy":appDelegate.dicLoginDetails?.userId!,"actionId":str_actionId,"actionType":"Closed","actionValue":remarksTxtField.text!]

        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(QUERY_ACTION, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            APIClient.sharedInstance.hideIndicator()

            if error == nil
            {
                let code = response?.value(forKey: "code") as? Int
                let message = response?.value(forKey: "message") as? String

                if code == 2001
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                    vc.str_Name = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
                else
                {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
                    vc.str_Title = message!
                    vc.vcController = self
                    self.present(vc, animated: true, completion: nil)
                }
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

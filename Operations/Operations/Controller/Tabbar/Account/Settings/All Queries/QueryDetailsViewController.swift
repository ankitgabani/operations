//
//  QueryDetailsViewController.swift
//  Operations
//
//  Created by iMac on 14/03/22.
//

import UIKit

class QueryDetailsViewController: UIViewController {

    @IBOutlet weak var topMainView: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var queryDetailTableView: UITableView!
    @IBOutlet weak var btnCLosedQuery: UIButton!
    
    var queryID : Int?
    var queryDetails = OPQueryDetails()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callGetQueryDetailAPI()
    }

    func callGetQueryDetailAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary(GET_QUERY_DETAIL + "\(queryID ?? 0)", parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    self.queryDetails = OPQueryDetails(fromDictionary: response!)
                    self.setScreenData(dict: self.queryDetails)
                }
                self.queryDetailTableView.delegate = self
                self.queryDetailTableView.dataSource = self
                self.queryDetailTableView.reloadData()
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func setScreenData(dict : OPQueryDetails){
        
        lblStatus.text = "Query ".uppercased() + queryDetails.queryStatus.uppercased()
        lblNumber.text = "#\(queryDetails.queryId ?? 0)"
        lblDate.text = queryDetails.queryDate
        if queryDetails.queryStatus == "Closed"
        {
            btnCLosedQuery.isHidden = true
            topMainView.backgroundColor = UIColor(red: 80/255, green: 134/255, blue: 60/255, alpha: 1)
        }
        else
        {
            btnCLosedQuery.isHidden = false
            topMainView.backgroundColor = UIColor(red: 235/255, green: 194/255, blue: 90/255, alpha: 1)
        }
    }
    
    
    @IBAction func closeQueryClicked(_ sender: Any) {
        let CloseQueryVC = self.storyboard?.instantiateViewController(withIdentifier: "CloseQueryViewController") as! CloseQueryViewController
        CloseQueryVC.str_actionId = "\(queryID ?? 0)"
        CloseQueryVC.modalPresentationStyle = .overCurrentContext
        self.present(CloseQueryVC, animated: true, completion: nil)
        
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension QueryDetailsViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if queryDetails.queryStatus == "Closed"{
            return 8
        }else{
            return 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QueryDetailTableViewCell") as! QueryDetailTableViewCell
        
        switch (indexPath.row)  {
        case 0:
            cell.detailDataLbl.text = "#\(queryDetails.queryId ?? 0)"
            cell.detailTitleLbl.text = "Query Number:"
            break
        case 1:
            cell.detailDataLbl.text = queryDetails.queryStatus
            cell.detailTitleLbl.text = "Query Status"
            break
        case 2:
            cell.detailDataLbl.text = queryDetails.queryDate
            cell.detailTitleLbl.text = "Created Date"
            break
        case 3:
            cell.detailDataLbl.text = queryDetails.querySendBy
            cell.detailTitleLbl.text = "Send By"
            break
        case 4:
            cell.detailDataLbl.text = queryDetails.querySendTo
            cell.detailTitleLbl.text = "Send To"
            break
        case 5:
            cell.detailDataLbl.text = queryDetails.closedBy
            cell.detailTitleLbl.text = "Close By"
            break
        case 6:
            cell.detailDataLbl.text = queryDetails.closedOn
            cell.detailTitleLbl.text = "Close Date"
            break
        case 7:
            cell.detailDataLbl.text = queryDetails.closedRemark
            cell.detailTitleLbl.text = "Close Remarks"
            break
        default: break
        
        }
        return cell
    }
}
class QueryDetailTableViewCell: UITableViewCell{
    
    @IBOutlet weak var detailTitleLbl: UILabel!
    @IBOutlet weak var detailDataLbl: UILabel!
    
    
}

//
//  SettingCell.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit

class SettingCell: UICollectionViewCell {

    @IBOutlet var imgView: UIImageView!
    @IBOutlet var viewCount: UIView!
    @IBOutlet var lblCount: UILabel!
    @IBOutlet var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewCount.layer.cornerRadius = viewCount.frame.size.height/2
    }

}

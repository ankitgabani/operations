//
//  UploadDocumentVC.swift
//  Operations
//
//  Created by Sahil Moradiya on 03/03/22.
//

import UIKit
import DropDown
import AVFoundation
import QuickLook
import Alamofire
import UniformTypeIdentifiers
import MobileCoreServices
import UIKit
import MobileCoreServices


class UploadDocumentVC: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIDocumentPickerDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet var txtDocumentType: UITextField!
    @IBOutlet var viewDocumentType: UIView!
    @IBOutlet weak var btnDropDown: UIButton!
    
    
    @IBOutlet weak var txtTraininType: UITextField!
    @IBOutlet weak var viewTrainingType: UIView!
    @IBOutlet weak var viewTrainingTypeHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewTrainingTypeTopconat: NSLayoutConstraint!
    @IBOutlet weak var viewTargetTraiing: UIView!
    
    
    @IBOutlet weak var txtNewTrainingName: UITextField!
    @IBOutlet weak var viewTrainingName: UIView!
    @IBOutlet weak var viewTrainingNameHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewTrainingNameTopconat: NSLayoutConstraint!
    
    var arrData: [OPDocumentList] = [OPDocumentList]()
    var arrTraining: [OPTrainingList] = [OPTrainingList]()
    
    let dropDownWorking = DropDown()
    let dropDownTraining = DropDown()
    
    var str_MasterID = 0
    var str_Code = ""
    var str_Path = ""
    var mimeType : String = ""
    var lastPath : String = ""

    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewTrainingType.isHidden = true
        viewTrainingTypeHeightCont.constant = 0
        viewTrainingTypeTopconat.constant = 0
        
        viewTrainingName.isHidden = true
        viewTrainingNameHeightCont.constant = 0
        viewTrainingNameTopconat.constant = 0
        
        callGetDocumentListAPI()
        
        callGetTrainingListAPI()
    }
    
    
    func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray
    {
        
        // the number of elements:
        let count = imageData.length / MemoryLayout<UInt8>.size
        
        // create array of appropriate length:
        var bytes = [UInt8](repeating: 0, count: count)
        // copy bytes into array
        imageData.getBytes(&bytes, length:count)
        
        let byteArray:NSMutableArray = NSMutableArray()
        
        for i in 0..<bytes.count {
            byteArray.add(NSNumber(value: bytes[i]))
        }
        
        return byteArray
    }
    
    @IBAction func clickedDropDown(_ sender: Any) {
    }
    @IBAction func TapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func TapOnDocumentType(_ sender: Any) {
        
        let arrName = NSMutableArray()
        
        for objDic in arrData
        {
            arrName.add(objDic.descriptionField!)
        }
        
        dropDownWorking.dataSource = arrName as! [String]
        dropDownWorking.anchorView = viewDocumentType
        dropDownWorking.direction = .any
        dropDownWorking.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtDocumentType.text = item
            
            for objData in arrData
            {
                if objData.descriptionField == item
                {
                    self.str_MasterID = objData.masterId
                    self.str_Code = objData.code
                }
            }
            
            if item == "Training Document"
            {
                viewTrainingType.isHidden = false
                viewTrainingTypeHeightCont.constant = 50
                viewTrainingTypeTopconat.constant = 20
            }
            else
            {
                viewTrainingType.isHidden = true
                viewTrainingTypeHeightCont.constant = 0
                viewTrainingTypeTopconat.constant = 0
            }
        }
        
        dropDownWorking.show()
    }
    
    @IBAction func clickedTraningType(_ sender: Any)
    {
        let arrName = NSMutableArray()
        
        for objDic in arrTraining
        {
            arrName.add(objDic.descriptionField!)
        }
        
        arrName.add("Create New Training")
        
        dropDownTraining.dataSource = arrName as! [String]
        dropDownTraining.anchorView = viewTargetTraiing
        dropDownTraining.direction = .any
        dropDownTraining.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            txtTraininType.text = item
            
            if item == "Create New Training"
            {
                self.viewTrainingName.isHidden = false
                self.viewTrainingNameHeightCont.constant = 50
                self.viewTrainingNameTopconat.constant = 20
            }
            else
            {
                self.viewTrainingName.isHidden = true
                self.viewTrainingNameHeightCont.constant = 0
                self.viewTrainingNameTopconat.constant = 0
            }
            
        }
        
        dropDownTraining.show()
    }
    
    
    @IBAction func TapOnUploadDocument(_ sender: Any)
    {
        if txtDocumentType.text == ""
        {
            self.view.makeToast("Please select document type.")
        }
        else if txtDocumentType.text == "Training Document"
        {
            if txtTraininType.text == ""
            {
                self.view.makeToast("Please select Training type.")
            }
            else if txtTraininType.text == "Create New Training"
            {
                if txtNewTrainingName.text == ""
                {
                    self.view.makeToast("Please enter Training Name.")
                }
                else
                {
                    if self.str_Code == "INVENTORY" || self.str_Code == "B2BINVENTORY" || self.str_Code == "CUSTOMER_DATA" || self.str_Code == "BARCODE_DATA" || self.str_Code == "PACKAGING_DETAIL"
                    {
                        openQlPreviewSheet()
                    }
                    else
                    {
                        openQlPreviewPDF()
                    }
                }
            }
            else
            {
                if self.str_Code == "INVENTORY" || self.str_Code == "B2BINVENTORY" || self.str_Code == "CUSTOMER_DATA" || self.str_Code == "BARCODE_DATA" || self.str_Code == "PACKAGING_DETAIL"
                {
                    openQlPreviewSheet()
                }
                else
                {
                    openQlPreviewPDF()
                }
            }
        }
        else
        {
            if self.str_Code == "INVENTORY" || self.str_Code == "B2BINVENTORY" || self.str_Code == "CUSTOMER_DATA" || self.str_Code == "BARCODE_DATA" || self.str_Code == "PACKAGING_DETAIL"
            {
                openQlPreviewSheet()
            }
            else
            {
                openQlPreviewPDF()
            }
        }
        
    }
    
    func openQlPreviewPDF() {
        
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func openQlPreviewSheet() {
        
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypeSpreadsheet)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            
            ctx.cgContext.drawPDFPage(page)
        }
        
        return img
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let cico = url as URL
        print(cico)
        print(url)
        
        print(url.lastPathComponent)
        
        print(url.pathExtension)
        
        let url_New = URL(string: url.relativeString)
        
        self.mimeType = url.mimeType()
        self.lastPath = url.pathExtension
        
        do{
            let data = try Data.init(contentsOf: url)
            
            myImageUploadRequest(uploadData: data, imgKey: "documentContent", strPath: url.pathExtension)

        }
        catch{
            
        }
        
//        if url.pathExtension == "xlsx"
//        {
//
//
//
//        }
//        else
//        {
//            let new_img = drawPDFfromURL(url: url_New!)
//            myImageUploadRequest(imageToUpload: new_img!, imgKey: "documentContent", strPath: url.pathExtension)
//        }
       
    }
    
//
//    func uploadWithAlamofire(filePath : String ) {
//
//        let url = URL(fileURLWithPath: filePath)//"/foo/bar/file.text")
//        let dirUrl = url.deletingLastPathComponent()
//        print(dirUrl.path+"/appImportContacts.xls")
//        // Output: /foo/bar
//
//
//        let filePath = dirUrl.path+"/appImportContacts.xls"
//
//        var bytes = [UInt8]()
//
//        if let data = NSData(contentsOfFile: filePath) {
//
//            var buffer = [UInt8](repeating: 0, count: data.length)
//            data.getBytes(&buffer, length: data.length)
//            bytes = buffer
//        }
//
//
//        AF.upload(multipartFormData: {
//            multipartFormData in
//            multipartFormData.append(Data(fromArray: bytes), withName: "appImportContacts",fileName: "appImportContacts.xls", mimeType: "application/octet-stream")
//        },
//                         to:BASE_URL + DOCUMENT_UPLOAD)
//        {
//            (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseJSON { response in
//                    print(response.result.value)
//                }
//
//            case .failure(let encodingError):
//                print(encodingError)
//            }
//        }
//    }
    
    func myImageUploadRequest(uploadData: Data, imgKey: String, strPath: String) {
        
        let myUrl = NSURL(string: BASE_URL + DOCUMENT_UPLOAD );
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        var param = ["": ""]
        
        if txtTraininType.text == "Create New Training"
        {
            param = ["documentId": "\(self.str_MasterID)","description":self.txtNewTrainingName.text ?? "","documentCode": self.str_Code,"documentExtension": strPath,"actionBy":"1"]
        }
        else
        {
            param = ["documentId":"\(self.str_MasterID)","description":self.txtDocumentType.text ?? "","documentCode": self.str_Code,"documentExtension": strPath,"actionBy":"1"]
        }
        
        let boundary = generateBoundaryString()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
//        let imageData = imageToUpload.jpegData(compressionQuality: 0.4)
//        if imageData == nil  {
//            return
//        }
        
        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: imgKey, imageDataKey: uploadData as NSData, boundary: boundary, imgKey: imgKey) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            APIClient.sharedInstance.hideIndicator()
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // print reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("response data = \(responseString!)")
            
            do{
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        vc.str_Name = "Document \(self.txtDocumentType.text ?? "") Uploaded"
                        vc.vcController = self
                        //     vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                        
                    }
                    
                    
                    // try to read out a string array
                    //                    if let dic = json["user"] as? NSDictionary {
                    //                        let Img = dic.value(forKey: "user_image")
                    //                            as! String
                    //                        self.isSelectedProPic = false
                    //generateBoundaryString
                    //                        UserDefaults.standard.setValue(Img, forKey: "profilepic")
                    //                        UserDefaults.standard.synchronize()
                    //                    }
                }
                
                
            }catch{
                
            }
            
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "\(imgKey).\(self.lastPath)"
        let mimetype = self.mimeType
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
   
    
    func callUploadDocumentAPI(documentContent: NSMutableArray, strPath: String)
    {
        
        var param = [String : Any]()
        
        if txtTraininType.text == "Create New Training"
        {
            param = ["documentId":self.str_MasterID,"documentContent":documentContent,"description":self.txtNewTrainingName.text ?? "","documentCode": self.str_Code,"documentExtension": strPath,"actionBy":"1"]
        }
        else
        {
            param = ["documentId":self.str_MasterID,"documentContent":documentContent,"description":self.txtDocumentType.text ?? "","documentCode": self.str_Code,"documentExtension": strPath,"actionBy":"1"]
        }
        
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderPost(DOCUMENT_UPLOAD, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                vc.str_Name = "Document \(self.txtDocumentType.text ?? "") Uploaded"
                //   vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                vc.str_Name = "Document \(self.txtDocumentType.text ?? "") Uploaded"
                vc.modalPresentationStyle = .fullScreen
                //  self.present(vc, animated: true, completion: nil)
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callGetDocumentListAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_DOCUMENT_TYPE, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    
                    for objData in response!
                    {
                        let dicData = OPDocumentList(fromDictionary: objData as! NSDictionary)
                        self.arrData.append(dicData)
                    }
                    
                    self.dropDownWorking.reloadAllComponents()
                }
                
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
    
    func callGetTrainingListAPI(){
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_TRAINING_TYPE, parameters: [:]) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                if response != nil
                {
                    
                    for objData in response!
                    {
                        let dicData = OPTrainingList(fromDictionary: objData as! NSDictionary)
                        self.arrTraining.append(dicData)
                    }
                    
                    self.dropDownTraining.reloadAllComponents()
                }
                
            }
            else
            {
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension Data {
    
    init<T>(fromArray values: [T]) {
        var values = values
        self.init(buffer: UnsafeBufferPointer(start: &values, count: values.count))
    }
    
    func toArray<T>(type: T.Type) -> [T] {
        return self.withUnsafeBytes {
            [T](UnsafeBufferPointer(start: $0, count: self.count/MemoryLayout<T>.stride))
        }
    }
}
extension NSURL {
    public func mimeType() -> String {
        if let pathExt = self.pathExtension,
            let mimeType = UTType(filenameExtension: pathExt)?.preferredMIMEType {
            return mimeType
        }
        else {
            return "application/octet-stream"
        }
    }
}

extension URL {
    public func mimeType() -> String {
        if let mimeType = UTType(filenameExtension: self.pathExtension)?.preferredMIMEType {
            return mimeType
        }
        else {
            return "application/octet-stream"
        }
    }
}

extension NSString {
    public func mimeType() -> String {
        if let mimeType = UTType(filenameExtension: self.pathExtension)?.preferredMIMEType {
            return mimeType
        }
        else {
            return "application/octet-stream"
        }
    }
}

extension String {
    public func mimeType() -> String {
        return (self as NSString).mimeType()
    }
}

//
//  TabbarController.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class TabbarController: UITabBarController {

    override func viewDidLoad() { 
        super.viewDidLoad()

        UITabBar.appearance().tintColor = .black
        
        tabBarController?.tabBar.backgroundColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithDefaultBackground()
            tabBarAppearance.backgroundColor = UIColor.white
            UITabBar.appearance().standardAppearance = tabBarAppearance

            if #available(iOS 15.0, *) {
                UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
}

//
//	OPOrderList.swift
//
//	Create by Ankit Gabani on 12/2/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPOrderList : NSObject, NSCoding{

	var barcode : String!
	var bundleQty : Int!
	var inventoryId : Int!
	var refNo : String!
	var rsp : String!
	var styleNo : String!
	var wareHouseId : Int!
	var wareHouseName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		barcode = dictionary["barcode"] as? String == nil ? "" : dictionary["barcode"] as? String
		bundleQty = dictionary["bundleQty"] as? Int == nil ? 0 : dictionary["bundleQty"] as? Int
		inventoryId = dictionary["inventoryId"] as? Int == nil ? 0 : dictionary["inventoryId"] as? Int
		refNo = dictionary["refNo"] as? String == nil ? "" : dictionary["refNo"] as? String
		rsp = dictionary["rsp"] as? String == nil ? "" : dictionary["rsp"] as? String
		styleNo = dictionary["styleNo"] as? String == nil ? "" : dictionary["styleNo"] as? String
		wareHouseId = dictionary["wareHouseId"] as? Int == nil ? 0 : dictionary["wareHouseId"] as? Int
		wareHouseName = dictionary["wareHouseName"] as? String == nil ? "" : dictionary["wareHouseName"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if barcode != nil{
			dictionary["barcode"] = barcode
		}
		if bundleQty != nil{
			dictionary["bundleQty"] = bundleQty
		}
		if inventoryId != nil{
			dictionary["inventoryId"] = inventoryId
		}
		if refNo != nil{
			dictionary["refNo"] = refNo
		}
		if rsp != nil{
			dictionary["rsp"] = rsp
		}
		if styleNo != nil{
			dictionary["styleNo"] = styleNo
		}
		if wareHouseId != nil{
			dictionary["wareHouseId"] = wareHouseId
		}
		if wareHouseName != nil{
			dictionary["wareHouseName"] = wareHouseName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         barcode = aDecoder.decodeObject(forKey: "barcode") as? String
         bundleQty = aDecoder.decodeObject(forKey: "bundleQty") as? Int
         inventoryId = aDecoder.decodeObject(forKey: "inventoryId") as? Int
         refNo = aDecoder.decodeObject(forKey: "refNo") as? String
         rsp = aDecoder.decodeObject(forKey: "rsp") as? String
         styleNo = aDecoder.decodeObject(forKey: "styleNo") as? String
         wareHouseId = aDecoder.decodeObject(forKey: "wareHouseId") as? Int
         wareHouseName = aDecoder.decodeObject(forKey: "wareHouseName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if barcode != nil{
			aCoder.encode(barcode, forKey: "barcode")
		}
		if bundleQty != nil{
			aCoder.encode(bundleQty, forKey: "bundleQty")
		}
		if inventoryId != nil{
			aCoder.encode(inventoryId, forKey: "inventoryId")
		}
		if refNo != nil{
			aCoder.encode(refNo, forKey: "refNo")
		}
		if rsp != nil{
			aCoder.encode(rsp, forKey: "rsp")
		}
		if styleNo != nil{
			aCoder.encode(styleNo, forKey: "styleNo")
		}
		if wareHouseId != nil{
			aCoder.encode(wareHouseId, forKey: "wareHouseId")
		}
		if wareHouseName != nil{
			aCoder.encode(wareHouseName, forKey: "wareHouseName")
		}

	}

}
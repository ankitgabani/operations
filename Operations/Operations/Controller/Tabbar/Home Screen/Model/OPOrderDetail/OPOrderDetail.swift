//
//	OPOrderDetail.swift
//
//	Create by Ankit Gabani on 12/2/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class OPOrderDetail : NSObject, NSCoding{

	var barcode : String!
	var imagePath : String!
	var itemDetail : [OPOrderDetailItemDetail]!
	var rsp : String!
	var styleNo : String!
	var title : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		barcode = dictionary["barcode"] as? String == nil ? "" : dictionary["barcode"] as? String
		imagePath = dictionary["imagePath"] as? String == nil ? "" : dictionary["imagePath"] as? String
		itemDetail = [OPOrderDetailItemDetail]()
		if let itemDetailArray = dictionary["itemDetail"] as? [NSDictionary]{
			for dic in itemDetailArray{
				let value = OPOrderDetailItemDetail(fromDictionary: dic)
				itemDetail.append(value)
			}
		}
		rsp = dictionary["rsp"] as? String == nil ? "" : dictionary["rsp"] as? String
		styleNo = dictionary["styleNo"] as? String == nil ? "" : dictionary["styleNo"] as? String
		title = dictionary["title"] as? String == nil ? "" : dictionary["title"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if barcode != nil{
			dictionary["barcode"] = barcode
		}
		if imagePath != nil{
			dictionary["imagePath"] = imagePath
		}
		if itemDetail != nil{
			var dictionaryElements = [NSDictionary]()
			for itemDetailElement in itemDetail {
				dictionaryElements.append(itemDetailElement.toDictionary())
			}
			dictionary["itemDetail"] = dictionaryElements
		}
		if rsp != nil{
			dictionary["rsp"] = rsp
		}
		if styleNo != nil{
			dictionary["styleNo"] = styleNo
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         barcode = aDecoder.decodeObject(forKey: "barcode") as? String
         imagePath = aDecoder.decodeObject(forKey: "imagePath") as? String
         itemDetail = aDecoder.decodeObject(forKey: "itemDetail") as? [OPOrderDetailItemDetail]
         rsp = aDecoder.decodeObject(forKey: "rsp") as? String
         styleNo = aDecoder.decodeObject(forKey: "styleNo") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if barcode != nil{
			aCoder.encode(barcode, forKey: "barcode")
		}
		if imagePath != nil{
			aCoder.encode(imagePath, forKey: "imagePath")
		}
		if itemDetail != nil{
			aCoder.encode(itemDetail, forKey: "itemDetail")
		}
		if rsp != nil{
			aCoder.encode(rsp, forKey: "rsp")
		}
		if styleNo != nil{
			aCoder.encode(styleNo, forKey: "styleNo")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}

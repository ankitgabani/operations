//
//	OPOrderDetailItemDetail.swift
//
//	Create by Ankit Gabani on 12/2/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPOrderDetailItemDetail : NSObject, NSCoding{

	var id : Int!
	var packQty : Int!
	var purchase : Float!
	var quantity : Int!
	var referenceNo : String!
	var warehouseId : Int!
	var warehouseName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		id = dictionary["id"] as? Int == nil ? 0 : dictionary["id"] as? Int
		packQty = dictionary["packQty"] as? Int == nil ? 0 : dictionary["packQty"] as? Int
		purchase = dictionary["purchase"] as? Float == nil ? 0 : dictionary["purchase"] as? Float
		quantity = dictionary["quantity"] as? Int == nil ? 0 : dictionary["quantity"] as? Int
		referenceNo = dictionary["referenceNo"] as? String == nil ? "" : dictionary["referenceNo"] as? String
		warehouseId = dictionary["warehouseId"] as? Int == nil ? 0 : dictionary["warehouseId"] as? Int
		warehouseName = dictionary["warehouseName"] as? String == nil ? "" : dictionary["warehouseName"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["id"] = id
		}
		if packQty != nil{
			dictionary["packQty"] = packQty
		}
		if purchase != nil{
			dictionary["purchase"] = purchase
		}
		if quantity != nil{
			dictionary["quantity"] = quantity
		}
		if referenceNo != nil{
			dictionary["referenceNo"] = referenceNo
		}
		if warehouseId != nil{
			dictionary["warehouseId"] = warehouseId
		}
		if warehouseName != nil{
			dictionary["warehouseName"] = warehouseName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         packQty = aDecoder.decodeObject(forKey: "packQty") as? Int
         purchase = aDecoder.decodeObject(forKey: "purchase") as? Float
         quantity = aDecoder.decodeObject(forKey: "quantity") as? Int
         referenceNo = aDecoder.decodeObject(forKey: "referenceNo") as? String
         warehouseId = aDecoder.decodeObject(forKey: "warehouseId") as? Int
         warehouseName = aDecoder.decodeObject(forKey: "warehouseName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if packQty != nil{
			aCoder.encode(packQty, forKey: "packQty")
		}
		if purchase != nil{
			aCoder.encode(purchase, forKey: "purchase")
		}
		if quantity != nil{
			aCoder.encode(quantity, forKey: "quantity")
		}
		if referenceNo != nil{
			aCoder.encode(referenceNo, forKey: "referenceNo")
		}
		if warehouseId != nil{
			aCoder.encode(warehouseId, forKey: "warehouseId")
		}
		if warehouseName != nil{
			aCoder.encode(warehouseName, forKey: "warehouseName")
		}

	}

}
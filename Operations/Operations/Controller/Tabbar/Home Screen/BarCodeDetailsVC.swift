//
//  BarCodeDetailsVC.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class BarCodeDetailsVC: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblTopBarCode: UILabel!
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblBarCode: UILabel!
    @IBOutlet weak var lblStyle: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
        
    var dicBarCode = OPOrderDetail()
    
    var arrOPOrderDetailItem: [OPOrderDetailItemDetail] = [OPOrderDetailItemDetail]()
    
    var dicOPOrderList: OPOrderList?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        callOrderListAPI(barcode: dicOPOrderList?.barcode ?? "")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }

    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "BarCodeDetailCell") as! BarCodeDetailCell
        
//        let dicData = arrOPOrderDetailItem[indexPath.row]
//        
//        let referenceNo = dicData.referenceNo ?? ""
//        let quantity = dicData.quantity ?? 0
//        let warehouseName = dicData.warehouseName ?? ""
//        
//        cell.lblRefe.text = referenceNo
//        cell.lblQut.text = "\(quantity)"
//        cell.lblLocation.text = warehouseName
//        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmOrderVC") as! ConfirmOrderVC
        vc.dicBarCode = self.dicBarCode
        vc.selectedIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func callOrderListAPI(barcode: String) {
                        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetDictionary(SEARCH_DETAILS + barcode, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String

            
            if error == nil
            {
                
                
                self.dicBarCode = OPOrderDetail(fromDictionary: response!)
                
                self.lblPrice.text = self.dicBarCode.rsp ?? ""
                self.lblBarCode.text = self.dicBarCode.barcode ?? ""
                self.lblStyle.text = self.dicBarCode.styleNo ?? ""
                self.lblTopBarCode.text = self.dicBarCode.barcode ?? ""
                
                
                for obj in self.dicBarCode.itemDetail
                {
                    self.arrOPOrderDetailItem.append(obj)
                }
                
                self.tblView.reloadData()
            }
            else
            {
                self.tblView.reloadData()
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
}

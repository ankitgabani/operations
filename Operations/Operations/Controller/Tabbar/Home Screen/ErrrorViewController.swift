//
//  ErrrorViewController.swift
//  Operations
//
//  Created by Ankit Gabani on 02/04/22.
//

import UIKit

class ErrrorViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    
    var str_Title = ""
    var vcController = UIViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = str_Title
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickedOk(_ sender: Any) {
        //     self.dismiss(animated: true, completion: nil)
             
             self.dismiss(animated: true) {
                 self.vcController.navigationController?.popViewController(animated: true)
             }
    }
    
}

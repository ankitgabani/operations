//
//  PopUpViewController.swift
//  Operations
//
//  Created by Ankit Gabani on 01/02/22.
//

import UIKit

class PopUpViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    
    var str_Name = ""
    var vcController = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = str_Name

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedOk(_ sender: Any) {
   //     self.dismiss(animated: true, completion: nil)
        
        self.dismiss(animated: true) {
            self.vcController.navigationController?.popViewController(animated: true)
        }
    }
    
}

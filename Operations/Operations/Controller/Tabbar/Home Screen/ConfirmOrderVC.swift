//
//  ConfirmOrderVC.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class ConfirmOrderVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lblBarcode: UILabel!
    @IBOutlet weak var lblReference: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblStyleBo: UILabel!
    @IBOutlet weak var lblRSP: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    var dicBarCode = OPOrderDetail()
        
    var selectedIndex = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
        let barcode = dicBarCode.barcode ?? ""
        let title = dicBarCode.title ?? ""
        let rsp = dicBarCode.rsp ?? ""
        let styleNo = dicBarCode.styleNo ?? ""

        
//        let dicBarCodeRefe = dicBarCode.itemDetail[selectedIndex]
//
//        lblReference.text = dicBarCodeRefe.referenceNo ?? ""
//        lblQuantity.text = "\(dicBarCodeRefe.quantity ?? 0)"
//        lblLocation.text = dicBarCodeRefe.warehouseName ?? ""
//
//        lblBarcode.text = barcode
//        lblTitle.text = barcode
//
//        lblStyleBo.text = styleNo
//        lblRSP.text = "\(rsp) ₹"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedPlaceOrder(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickedOk(_ sender: Any) {
    }
    
}

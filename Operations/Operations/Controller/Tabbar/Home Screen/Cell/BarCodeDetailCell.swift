//
//  BarCodeDetailCell.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class BarCodeDetailCell: UITableViewCell {

    @IBOutlet weak var lblRefe: UILabel!
    @IBOutlet weak var lblQut: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

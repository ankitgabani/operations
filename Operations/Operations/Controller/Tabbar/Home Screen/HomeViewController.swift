//
//  HomeViewController.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit
import SwiftUI

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet
    @IBOutlet weak var viewSearchTop: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblDateTime: UILabel!
    
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var btnCancle: UIButton!
        
    @IBOutlet weak var btnAdd: UIButton!
    var arrOrderBarcode: [OPOrderList] = [OPOrderList]()
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.callRecentInventoryUpdateAPI()

        imgCancel.isHidden = true
        btnCancle.isHidden = true
        
        tblView.delegate = self
        tblView.dataSource = self
        
        UITabBar.appearance().unselectedItemTintColor = UIColor.black
        
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
                
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
//    private func readJson() {
//        do {
//            if let file = Bundle.main.url(forResource: "searchList", withExtension: "json")
//            {
//                let data = try Data(contentsOf: file)
//                let json = try JSONSerialization.jsonObject(with: data, options: [])
//                if let object = json as? [String: Any]
//                {
//                    // json is a dictionary
//                    print(object)
//                } else if let object = json as? [Any]
//                {
//                    // json is an array
//                    arrBarCode = ((object as NSArray).mutableCopy() as? NSMutableArray)!
//                    print(object)
//                } else
//                {
//                    print("JSON is invalid")
//                }
//            } else
//            {
//                print("no file")
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField) {
        
        if txtSearch.text!.count > 0
        {
            imgCancel.isHidden = false
            btnCancle.isHidden = false
        }
        else
        {
            imgCancel.isHidden = true
            btnCancle.isHidden = true
        }
        
        if txtSearch.text!.count > 3 {
            imgCancel.isHidden = false
            btnCancle.isHidden = false
            
            self.callOrderListAPI(barcode: self.txtSearch.text ?? "")
            
        }
        else
        {
            arrOrderBarcode.removeAll()
        }
        self.tblView.reloadData()
    }
   
    //MARK: - Action Method
    @IBAction func clickedClrea(_ sender: Any) {
        arrOrderBarcode.removeAll()
        imgCancel.isHidden = true
        btnCancle.isHidden = true
        txtSearch.text = ""
        tblView.reloadData()
    }
    
    @IBAction func clickedAdd(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceOrderVC") as! PlaceOrderVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - UItableview Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return  5//arrOrderBarcode.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "SerachBarCodeTableCell") as! SerachBarCodeTableCell
        
//        let dicData = arrOrderBarcode[indexPath.row]
//
//        let barCode = dicData.barcode ?? ""
//        let bundleQty = dicData.bundleQty ?? 0
//        let rsp = dicData.rsp ?? ""
//
//        cell.lblCode.text = barCode
//        cell.lblQut.text = "Q: \(bundleQty)"
//        cell.lblPrice.text = "₹ \(rsp)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BarCodeDetailsVC") as! BarCodeDetailsVC
//        vc.dicOPOrderList = arrOrderBarcode[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - API Calling
    func callOrderListAPI(barcode: String) {
        
        self.view.hideAllToasts()
                
        let param = ["barcode": barcode]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(SEARCH_BARCODE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String

            if error == nil
            {
                self.arrOrderBarcode.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPOrderList(fromDictionary: obj as! NSDictionary)
                        self.arrOrderBarcode.append(dicData)
                    }
                    
                    self.tblView.reloadData()

                }
                else
                {
                    self.arrOrderBarcode.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrOrderBarcode.removeAll()
                self.tblView.reloadData()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    
    func callRecentInventoryUpdateAPI() {
                        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetstring(RECENT_INVENRORY_UPDATE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            if error == nil
            {
                self.lblDateTime.text = "Inventory last updated on \"\(response ?? "")\""
                
            }
            else
            {
                
                APIClient.sharedInstance.hideIndicator()
            }
        }
    }
}

struct HomeView: View {
    var body: some View {
        HomeViewController1()
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct HomeViewController1: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> some UIViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(identifier: "TabbarController")
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
}

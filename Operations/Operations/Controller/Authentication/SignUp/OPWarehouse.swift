//
//	OPWarehouse.swift
//
//	Create by Ankit Gabani on 16/2/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPWarehouse : NSObject, NSCoding{

	var canRegister : Bool!
	var code : String!
	var descriptionField : String!
	var isActive : Bool!
	var masterId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		canRegister = dictionary["canRegister"] as? Bool == nil ? false : dictionary["canRegister"] as? Bool
		code = dictionary["code"] as? String == nil ? "" : dictionary["code"] as? String
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		isActive = dictionary["isActive"] as? Bool == nil ? false : dictionary["isActive"] as? Bool
		masterId = dictionary["masterId"] as? Int == nil ? 0 : dictionary["masterId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if canRegister != nil{
			dictionary["canRegister"] = canRegister
		}
		if code != nil{
			dictionary["code"] = code
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if isActive != nil{
			dictionary["isActive"] = isActive
		}
		if masterId != nil{
			dictionary["masterId"] = masterId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         canRegister = aDecoder.decodeObject(forKey: "canRegister") as? Bool
         code = aDecoder.decodeObject(forKey: "code") as? String
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         isActive = aDecoder.decodeObject(forKey: "isActive") as? Bool
         masterId = aDecoder.decodeObject(forKey: "masterId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if canRegister != nil{
			aCoder.encode(canRegister, forKey: "canRegister")
		}
		if code != nil{
			aCoder.encode(code, forKey: "code")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "isActive")
		}
		if masterId != nil{
			aCoder.encode(masterId, forKey: "masterId")
		}

	}

}
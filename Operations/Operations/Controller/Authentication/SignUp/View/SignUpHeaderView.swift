//
//  SignUpHeaderView.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class SignUpHeaderView: UIView {

    @IBOutlet weak var btnOpen: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgSrrow: UIImageView!
    
    @IBOutlet weak var lblSignuo: UILabel!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }

}

//
//  QualificationTableCell.swift
//  Operations
//
//  Created by Ankit Gabani on 02/02/22.
//

import UIKit

class QualificationTableCell: UITableViewCell {
    
    @IBOutlet weak var viewOtherHiestQuali: UIView!
    @IBOutlet weak var txtOtherHiestQuali: UITextField!
    @IBOutlet weak var HiestQualiHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var HiestQualiTopCont: NSLayoutConstraint!

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var txtHieghestQualification: UITextField!
    @IBOutlet weak var btnHieghestQualification: UIButton!
    @IBOutlet weak var viewTargetHieghestQualifiction: UIView!
    
    @IBOutlet weak var txtMarksHighest: UITextField!
    
    @IBOutlet weak var viewAddMore: UIView!
    @IBOutlet weak var btnAddMnoe: UIButton!
    
    @IBOutlet weak var viewclose: UIView!
    @IBOutlet weak var btnclose: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

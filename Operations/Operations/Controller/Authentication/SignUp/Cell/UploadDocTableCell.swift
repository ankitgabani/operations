//
//  UploadDocTableCell.swift
//  Operations
//
//  Created by Ankit Gabani on 02/02/22.
//

import UIKit

class UploadDocTableCell: UITableViewCell {

    @IBOutlet weak var viewOtherDocument: UIView!
    @IBOutlet weak var txtOtherDocument: UITextField!
    @IBOutlet weak var DocumentHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var DocumentTopCont: NSLayoutConstraint!
    
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var txtDocument: UITextField!
    @IBOutlet weak var viewTargetDocumentc: UIView!
    
    @IBOutlet weak var btnUploadDocument: UIButton!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var view7: UIView!
    
    @IBOutlet weak var btnTrash1: UIButton!
    @IBOutlet weak var btnTrash2: UIButton!
    @IBOutlet weak var btnTrash3: UIButton!
    @IBOutlet weak var btnTrash4: UIButton!
    @IBOutlet weak var btnTrash5: UIButton!
    @IBOutlet weak var btnTrash6: UIButton!
    @IBOutlet weak var btnTrash7: UIButton!
    
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblName2: UILabel!
    @IBOutlet weak var lblName3: UILabel!
    @IBOutlet weak var lblName4: UILabel!
    @IBOutlet weak var lblName5: UILabel!
    @IBOutlet weak var lblName6: UILabel!
    @IBOutlet weak var lblName7: UILabel!
    
    
    @IBOutlet weak var view1Cont: NSLayoutConstraint!
    @IBOutlet weak var view2Cont: NSLayoutConstraint!
    @IBOutlet weak var view3Cont: NSLayoutConstraint!
    @IBOutlet weak var view4Cont: NSLayoutConstraint!
    @IBOutlet weak var view5Cont: NSLayoutConstraint!
    @IBOutlet weak var view6Cont: NSLayoutConstraint!
    @IBOutlet weak var view7Cont: NSLayoutConstraint!
    


    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

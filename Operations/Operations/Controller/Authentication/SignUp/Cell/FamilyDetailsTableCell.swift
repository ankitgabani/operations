//
//  FamilyDetailsTableCell.swift
//  Operations
//
//  Created by Ankit Gabani on 01/02/22.
//

import UIKit

class FamilyDetailsTableCell: UITableViewCell {
    
    
    @IBOutlet weak var viewOtherRelationShip: UIView!
    @IBOutlet weak var txtOtherRelationShip: UITextField!
    @IBOutlet weak var relatonshipHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var relationShipTopCont: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewOtherpro: UIView!
    @IBOutlet weak var txtOtherPro: UITextField!
    @IBOutlet weak var ProHieghtCont: NSLayoutConstraint!
    @IBOutlet weak var ProTopCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var viewAddMore: UIView!
    @IBOutlet weak var btnAddMore: UIButton!
    
    @IBOutlet weak var viewcancel: UIView!
    @IBOutlet weak var btnCacnel: UIButton!
    
    
    @IBOutlet weak var viewRelationTarget: UIView!
    @IBOutlet weak var btnRelationshiopDrop: UIButton!
    @IBOutlet weak var txtRelationShip: UITextField!
    
    @IBOutlet weak var txtRelativeNAme: UITextField!
    @IBOutlet weak var viewRelativeName: UIView!
    @IBOutlet weak var lblErrorRelativeName: UILabel!
    
    
    @IBOutlet weak var viewAge: UIView!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var lblErrorAge: UILabel!
    
    
    @IBOutlet weak var viewProfessionTarget: UIView!
    @IBOutlet weak var btnProfession: UIButton!
    @IBOutlet weak var txtProfesssion: UITextField!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

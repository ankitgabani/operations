//
//  PersonalDetailsCell.swift
//  Operations
//
//  Created by Ankit Gabani on 27/01/22.
//

import UIKit

class PersonalDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var viewID: UIView!
    @IBOutlet weak var viewHeighCont: NSLayoutConstraint!
    @IBOutlet weak var viewTopCont: NSLayoutConstraint!
    
    @IBOutlet weak var mainVIew: UIView!
    
    @IBOutlet weak var btnChooseOption: UIButton!
    
    @IBOutlet weak var txtID: UITextField!

    
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var lblFirstNameError: UILabel!
    
    
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var lblErrorLast: UILabel!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblErrorEmail: UILabel!
    
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var lblErrorMobile: UILabel!
    
    
    @IBOutlet weak var viewWorkingLocation: UIView!
    @IBOutlet weak var txtWorkingLocation: UITextField!
    @IBOutlet weak var btnWorkingLocation: UIButton!
    @IBOutlet weak var viewTarget: UIView!
    
    @IBOutlet weak var txtOtherWorkingLocation: UITextField!
    
    @IBOutlet weak var viewOtherWorking: UIView!
    @IBOutlet weak var viewOtherWorkingHieghtcont: NSLayoutConstraint!
    @IBOutlet weak var viewOtherTopCont: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var viewAadhaarNu: UIView!
    @IBOutlet weak var txtAadharNo: UITextField!
    @IBOutlet weak var lblErrorAadhar: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

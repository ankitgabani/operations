//
//  PasswordDetailsTableCell.swift
//  Operations
//
//  Created by Ankit Gabani on 01/02/22.
//

import UIKit

class PasswordDetailsTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblErrorPassword: UILabel!
    
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var lblErrorConfirmPassword: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

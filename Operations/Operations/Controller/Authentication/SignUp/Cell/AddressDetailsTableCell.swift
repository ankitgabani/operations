//
//  AddressDetailsTableCell.swift
//  Operations
//
//  Created by Ankit Gabani on 01/02/22.
//

import UIKit

class AddressDetailsTableCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var viewPincode: UIView!
    @IBOutlet weak var txtPinCode: UITextField!
    @IBOutlet weak var lblErrorPincode: UILabel!
    
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var txtHome: UITextField!
    
    
    @IBOutlet weak var viewResentAdd: UIView!
    @IBOutlet weak var txtResidentAddress: UITextView!
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnCheck: UIButton!
    
    @IBOutlet weak var viewPer: UIView!
    @IBOutlet weak var txtPer: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  SignUpViewController.swift
//  Operations
//
//  Created by Ankit Gabani on 26/01/22.
//

import UIKit
import SwiftUI
import DropDown
import AVFoundation
import Toast_Swift
import SVProgressHUD
import IQKeyboardManager

class SignUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate,                                                             UIImagePickerControllerDelegate,UITextFieldDelegate, UITextViewDelegate
{
    //MARK: - IBOutlet
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    var arrHeaderView = ["PERSONAL DETAILS","PASSWORD DETAILS","ADDRESS DETAILS","FAMILY DETAILS","QUALIFICATION DETAILS","UPLOAD DOCUMENTS"]
    
    let arrUploadDocument = NSMutableArray()
    
    var arrRelation: [OPSignupList] = [OPSignupList]()
    var arrProfessions: [OPSignupList] = [OPSignupList]()
    var arrQualifications: [OPSignupList] = [OPSignupList]()
    var arrWarehouse: [OPWarehouse] = [OPWarehouse]()
    
    var selectedHeader = -1
    
    var addFamilyDetails = 1
    var addQualificationDetails = 1
    
    var dicObject = NSMutableDictionary()
    
    var isRelativeName = false
    
    var strWarehouseID = 0
    
    var strprofileImageByteArray = NSData()
    
    var strAdharImageByteArray = NSData()
    
    
    let arrUploadAllDocument = NSMutableArray()
    
    let arrNewUploadAllDocument = NSMutableArray()
    
    
    let strCurrentShowDoc = "Aadhaar"
    
    var strFirstName = ""
    var strLastName = ""
    var strEmailName = ""
    var strPhoneName = ""
    var strWorkingLocationName = ""
    var strAdharName = ""
    
    var strPasswordName = ""
    var strConfirmPasswordName = ""
    
    var strPinCode = ""
    var strHome = ""
    var strResident = ""
    var strPerment = ""
    var strSameAddress = false
    
    var strRelationShip = ""
    var strRelativeName = ""
    var strAge = ""
    var strProfession = ""
    
    var strHeightQua = ""
    var strMarks = ""
    
    
    
    var isUploadDoc = false
    
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            SVProgressHUD.setOffsetFromCenter(UIOffset(horizontal: self.tblView.frame.size.width  / 2, vertical:  self.tblView.frame.size.height / 2))
            
        }
        IQKeyboardManager.shared().isEnabled = false
        
        arrUploadDocument.add("Aadhaar")
        
        viewPopup.isHidden = true
        
        btnSignup.layer.cornerRadius = 8
        btnSignup.clipsToBounds = true
        btnSignup.layer.borderColor = UIColor.black.cgColor
        btnSignup.layer.borderWidth = 1
        
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "PersonalDetailsCell", bundle: nil), forCellReuseIdentifier: "PersonalDetailsCell")
        tblView.register(UINib(nibName: "PasswordDetailsTableCell", bundle: nil), forCellReuseIdentifier: "PasswordDetailsTableCell")
        tblView.register(UINib(nibName: "AddressDetailsTableCell", bundle: nil), forCellReuseIdentifier: "AddressDetailsTableCell")
        tblView.register(UINib(nibName: "FamilyDetailsTableCell", bundle: nil), forCellReuseIdentifier: "FamilyDetailsTableCell")
        tblView.register(UINib(nibName: "QualificationTableCell", bundle: nil), forCellReuseIdentifier: "QualificationTableCell")
        tblView.register(UINib(nibName: "UploadDocTableCell", bundle: nil), forCellReuseIdentifier: "UploadDocTableCell")
        
        callRelationsListAPI()
        callProfessionsListAPI()
        callQualificationsListAPI()
        callWarehouseListAPI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = false
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    //MARK: - TableView cell Action Method
    @objc func clickedChooseProfile(_ sender: UIButton)
    {
        isUploadDoc = false
        self.viewPopup.isHidden = false
    }
    
    @objc func clickedUploadDoc(_ sender: UIButton)
    {
        isUploadDoc = true
        self.viewPopup.isHidden = false
    }
    
    @objc func clickedPostdFamily(_ sender: UIButton)
    {
        
        let dropDown = sender.accessibilityElements?.first as! DropDown
        
        dropDown.show()
    }
    
    @objc func clickedPostdPro(_ sender: UIButton)
    {
        
        let dropDown = sender.accessibilityElements?.first as! DropDown
        
        dropDown.show()
    }
    
    @objc func clickedPostdQua(_ sender: UIButton)
    {
        
        let dropDown = sender.accessibilityElements?.first as! DropDown
        
        dropDown.show()
    }
    
    @objc func clickedPostdHisg(_ sender: UIButton)
    {
        
        let dropDown = sender.accessibilityElements?.first as! DropDown
        
        dropDown.show()
    }
    
    @objc func clickedAddFamily(_ sender: UIButton)
    {
        if self.dicObject.count == 0{
            return
        }
        
        for key in self.dicObject.allKeys
        {
            
            if self.dicObject.object(forKey: key) != nil{
                
                let object = self.dicObject.object(forKey: key) as! NSDictionary
                
                if (object.value(forKey: "relationship") as? String) == nil{
                    
                    return
                }
                if (object.value(forKey: "relationship") as? String) != nil{
                    
                    let value = (object.value(forKey: "relationship") as? String)
                    
                    if value == "Other"{
                        
                        let valueOther = (object.value(forKey: "otherRelationship") as? String)
                        
                        if valueOther == nil{
                            
                            return
                        }
                    }
                }
                if (object.value(forKey: "profession") as? String) == nil{
                    
                    return
                }
                if (object.value(forKey: "profession") as? String) != nil{
                    
                    let value = (object.value(forKey: "profession") as? String)
                    
                    if value == "Other"{
                        
                        let valueOther = (object.value(forKey: "otherProfession") as? String)
                        
                        if valueOther == nil{
                            
                            return
                        }
                    }
                }
                if (object.value(forKey: "age") as? String) == nil{
                    
                    return
                }
                if (object.value(forKey: "relativeName") as? String) == nil{
                    
                    return
                }
            }
            
        }
        let index = IndexPath(row: sender.tag, section: 3)
        addFamilyDetails += 1
        
        let dicObject = NSMutableDictionary()
        dicObject.setValue(nil, forKey: "relationship")
        dicObject.setValue(nil, forKey: "profession")
        dicObject.setValue(nil, forKey: "age")
        dicObject.setValue(nil, forKey: "relativeName")
        
        self.dicObject.setObject(dicObject, forKey: addFamilyDetails as NSCopying)
        
        tblView.reloadData()
    }
    
    @objc func clickedCancelFamily(_ sender: UIButton)
    {
        
        var cell : FamilyDetailsTableCell!
        
        var tempView : UIView = sender
        
        while(true){
            
            if tempView.isKind(of: FamilyDetailsTableCell.self){
                
                cell = tempView as? FamilyDetailsTableCell
                break
            }
            else{
                tempView = tempView.superview!
            }
        }
        
        let indexPath = tblView.indexPath(for: cell)!
        if addFamilyDetails > 1{
            addFamilyDetails -= 1
        }
        //        tblView.deleteRows(at: [indexPath], with: .none)
        let indexPP = IndexPath(row: indexPath.row+1, section: 3)
        tblView.deleteRows(at: [indexPP], with: .none)
        self.dicObject.removeObject(forKey: indexPath.row+1)
        
        let newDic = NSMutableDictionary()
        newDic.addEntries(from: self.dicObject as! [AnyHashable : Any])
        
        self.dicObject = NSMutableDictionary()
        
        let allKey = newDic.allKeys
        
        var index = 0
        
        for key in allKey{
            
            var newKey = index
            self.dicObject.setObject(newDic.object(forKey: key), forKey: newKey as! NSCopying)
            
            index = index + 1
            
        }
        
        
        tblView.reloadData()
        
    }
    
    
    @objc func clickedCancelQualification(_ sender: UIButton)
    {
        
        var cell : QualificationTableCell!
        
        var tempView : UIView = sender
        
        while(true){
            
            if tempView.isKind(of: QualificationTableCell.self){
                
                cell = tempView as? QualificationTableCell
                break
            }
            else{
                tempView = tempView.superview!
            }
        }
        
        let indexPath = tblView.indexPath(for: cell)!
        if addQualificationDetails > 1{
            addQualificationDetails -= 1
        }
        //  tblView.deleteRows(at: [indexPath], with: .none)
        
        let indexPP = IndexPath(row: indexPath.row+1, section: 4)
        let cellNext = tblView.cellForRow(at: indexPP) as! QualificationTableCell
        
        tblView.deleteRows(at: [indexPP], with: .none)
        
        
        if cell.txtHieghestQualification.text == "Other"
        {
            self.arrUploadDocument.remove(cell.txtOtherHiestQuali.text ?? "")
        }
        
        self.arrUploadDocument.remove(cellNext.txtHieghestQualification.text ?? "")
        
        self.dicObject.removeObject(forKey: indexPath.row+1)
        
        let newDic = NSMutableDictionary()
        newDic.addEntries(from: self.dicObject as! [AnyHashable : Any])
        
        self.dicObject = NSMutableDictionary()
        
        let allKey = newDic.allKeys
        
        var index = 0
        
        for key in allKey{
            
            var newKey = index
            self.dicObject.setObject(newDic.object(forKey: key), forKey: newKey as! NSCopying)
            
            index = index + 1
            
        }
        
        tblView.reloadData()
        
    }
    
    @objc func clickedAddQualification(_ sender: UIButton)
    {
        
        //        if self.dicObject.count == 0{
        //            return
        //        }
        //
        //        for key in self.dicObject.allKeys{
        //
        //            if self.dicObject.object(forKey: key) != nil{
        //
        //                let object = self.dicObject.object(forKey: key) as! NSDictionary
        //
        //                if (object.value(forKey: "qualification") as? String) == nil{
        //
        //                    return
        //                }
        //                if (object.value(forKey: "qualification") as? String) != nil{
        //
        //                    let value = (object.value(forKey: "qualification") as? String)
        //
        //                    if value == "Other"{
        //
        //                        let valueOther = (object.value(forKey: "otherQualification") as? String)
        //
        //                        if valueOther == nil{
        //
        //                            return
        //                        }
        //                    }
        //                }
        //                if (object.value(forKey: "marks") as? String) == nil{
        //
        //                    return
        //                }
        //            }
        //
        //        }
        
        addQualificationDetails += 1
        
        tblView.reloadData()
    }
    
    @objc func clickedResidentAddress(_ sender: UIButton)
    {
        let index = IndexPath(row: 0, section: sender.tag)
        
        let cell = tblView.cellForRow(at: index) as! AddressDetailsTableCell
        
        if cell.imgCheck.image == UIImage(named: "check-1")
        {
            cell.imgCheck.image = UIImage(named: "unCheck")
            cell.txtPer.text = ""
            self.strPerment = ""
            cell.txtPer.isUserInteractionEnabled = true
        }
        else
        {
            cell.imgCheck.image = UIImage(named: "check-1")
            cell.txtPer.text = cell.txtResidentAddress.text
            self.strPerment = cell.txtResidentAddress.text
            cell.txtPer.isUserInteractionEnabled = false
        }
        
    }
    
    @objc func changeTextfield(_ textfield:UITextField) {
        
        if textfield.tag == 0
        {
            let index = IndexPath(row: 0, section: textfield.tag)
            
            let cell = tblView.cellForRow(at: index) as! PersonalDetailsCell
            
            if textfield == cell.txtFirstName
            {
                if cell.txtFirstName.text == ""
                {
                    cell.lblFirstNameError.isHidden = false
                    cell.viewFirstName.borderColor = .red
                }
                else if cell.txtFirstName.text!.count < 2
                {
                    cell.lblFirstNameError.isHidden = false
                    cell.viewFirstName.borderColor = .red
                }
                else if cell.txtFirstName.text!.count > 40
                {
                    cell.lblFirstNameError.isHidden = false
                    cell.viewFirstName.borderColor = .red
                }
                else
                {
                    cell.lblFirstNameError.isHidden = true
                    cell.viewFirstName.borderColor = .gray
                }
            }
            else  if textfield == cell.txtLastName
            {
                if cell.txtLastName.text == ""
                {
                    cell.lblErrorLast.isHidden = false
                    cell.viewLastName.borderColor = .red
                }
                else if cell.txtLastName.text!.count < 2
                {
                    cell.lblErrorLast.isHidden = false
                    cell.viewLastName.borderColor = .red
                }
                else if cell.txtLastName.text!.count > 40
                {
                    cell.lblErrorLast.isHidden = false
                    cell.viewLastName.borderColor = .red
                }
                else
                {
                    cell.lblErrorLast.isHidden = true
                    cell.viewLastName.borderColor = .gray
                }
            }
            else  if textfield == cell.txtEmail
            {
                
                if cell.txtEmail.text == ""
                {
                    cell.lblErrorEmail.isHidden = false
                    cell.viewEmail.borderColor = .red
                }
                else if cell.txtEmail.text!.count < 4
                {
                    cell.lblErrorEmail.isHidden = false
                    cell.viewEmail.borderColor = .red
                }
                else if cell.txtEmail.text!.count > 40
                {
                    cell.lblErrorEmail.isHidden = false
                    cell.viewEmail.borderColor = .red
                }
                else
                {
                    cell.lblErrorEmail.isHidden = true
                    cell.viewEmail.borderColor = .gray
                }
            }
            else  if textfield == cell.txtMobile
            {
                
                if cell.txtMobile.text == ""
                {
                    cell.lblErrorMobile.isHidden = false
                    cell.viewMobile.borderColor = .red
                }
                else if cell.txtMobile.text!.count == 10
                {
                    cell.lblErrorMobile.isHidden = true
                    cell.viewMobile.borderColor = .gray
                }
                else
                {
                    cell.lblErrorMobile.isHidden = false
                    cell.viewMobile.borderColor = .red
                }
            }
            else  if textfield == cell.txtWorkingLocation
            {
                
                //                if cell.txtWorkingLocation.text == ""
                //                {
                //                    cell.lblErrorMobile.isHidden = false
                //                    cell.viewMobile.borderColor = .red
                //                }
                //                else
                //                {
                //                    cell.lblErrorMobile.isHidden = false
                //                    cell.viewMobile.borderColor = .red
                //                }
            }
            else  if textfield == cell.txtAadharNo
            {
                
                if cell.txtAadharNo.text == ""
                {
                    cell.lblErrorAadhar.isHidden = false
                    cell.viewAadhaarNu.borderColor = .red
                }
                else if cell.txtAadharNo.text!.count == 12
                {
                    cell.lblErrorAadhar.isHidden = true
                    cell.viewAadhaarNu.borderColor = .gray
                }
                else
                {
                    cell.lblErrorAadhar.isHidden = false
                    cell.viewAadhaarNu.borderColor = .red
                }
            }
            
        }
        else if textfield.tag == 1
        {
            let index = IndexPath(row: 0, section: textfield.tag)
            
            let cell = tblView.cellForRow(at: index) as! PasswordDetailsTableCell
            
            if textfield == cell.txtPassword
            {
                if cell.txtPassword.text == ""
                {
                    cell.lblErrorPassword.isHidden = false
                    cell.viewPassword.borderColor = .red
                }
                else if cell.txtPassword.text!.count < 4
                {
                    cell.lblErrorPassword.isHidden = false
                    cell.viewPassword.borderColor = .red
                }
                else if cell.txtPassword.text!.count > 20
                {
                    cell.lblErrorPassword.isHidden = false
                    cell.viewPassword.borderColor = .red
                }
                else
                {
                    cell.lblErrorPassword.isHidden = true
                    cell.viewPassword.borderColor = .gray
                }
            }
            else  if textfield == cell.txtConfirmPassword
            {
                if cell.txtConfirmPassword.text == ""
                {
                    cell.lblErrorConfirmPassword.isHidden = false
                    cell.viewConfirmPassword.borderColor = .red
                }
                else if cell.txtConfirmPassword.text!.count < 4
                {
                    cell.lblErrorConfirmPassword.isHidden = false
                    cell.viewConfirmPassword.borderColor = .red
                }
                else if cell.txtConfirmPassword.text!.count > 20
                {
                    cell.lblErrorConfirmPassword.isHidden = false
                    cell.viewConfirmPassword.borderColor = .red
                }
                else
                {
                    cell.lblErrorConfirmPassword.isHidden = true
                    cell.viewConfirmPassword.borderColor = .gray
                }
            }
            
        }
        else if textfield.tag == 2
        {
            let index = IndexPath(row: 0, section: textfield.tag)
            
            let cell = tblView.cellForRow(at: index) as! AddressDetailsTableCell
            
            if textfield == cell.txtPinCode
            {
                if cell.txtPinCode.text == ""
                {
                    cell.lblErrorPincode.isHidden = false
                    cell.viewPincode.borderColor = .red
                }
                else if cell.txtPinCode.text!.count == 6
                {
                    cell.lblErrorPincode.isHidden = true
                    cell.viewPincode.borderColor = .gray
                }
                else
                {
                    cell.lblErrorPincode.isHidden = false
                    cell.viewPincode.borderColor = .red
                }
            }
        }
        else if textfield.tag == 3
        {
            var cell : FamilyDetailsTableCell!
            
            var tempView : UIView = textfield
            
            while(true){
                
                if tempView.isKind(of: FamilyDetailsTableCell.self){
                    
                    cell = tempView as? FamilyDetailsTableCell
                    break
                }
                else{
                    tempView = tempView.superview!
                }
                
            }
            
            let indexPath = tblView.indexPath(for: cell)
            
            if textfield == cell.txtRelativeNAme
            {
                if cell.txtRelativeNAme.text == ""
                {
                    cell.lblErrorRelativeName.isHidden = false
                    cell.viewRelativeName.borderColor = .red
                }
                else if cell.txtRelativeNAme.text!.count < 2
                {
                    cell.lblErrorRelativeName.isHidden = false
                    cell.viewRelativeName.borderColor = .red
                }
                else if cell.txtRelativeNAme.text!.count > 40
                {
                    cell.lblErrorRelativeName.isHidden = false
                    cell.viewRelativeName.borderColor = .red
                }
                else
                {
                    cell.lblErrorRelativeName.isHidden = true
                    cell.viewRelativeName.borderColor = .gray
                }
            }
            else if textfield == cell.txtAge
            {
                if cell.txtAge.text == ""
                {
                    cell.lblErrorAge.isHidden = false
                    cell.viewAge.borderColor = .red
                }
                else if cell.txtAge.text == "0" || cell.txtAge.text == "00"
                {
                    cell.lblErrorAge.isHidden = false
                    cell.viewAge.borderColor = .red
                }
                else
                {
                    cell.lblErrorAge.isHidden = true
                    cell.viewAge.borderColor = .gray
                }
                
            }
        }
        
    }
    
    //MARK: - UITableView Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrHeaderView.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3
        {
            return addFamilyDetails
        }
        else if section == 4
        {
            return addQualificationDetails
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "PersonalDetailsCell") as! PersonalDetailsCell
            
            
            cell.btnChooseOption.addTarget(self, action: #selector(clickedChooseProfile(_ :)), for: .touchUpInside)
            
            cell.lblFirstNameError.isHidden = true
            cell.txtFirstName.tag = indexPath.section
            cell.txtFirstName.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorLast.isHidden = true
            cell.txtLastName.tag = indexPath.section
            cell.txtLastName.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorEmail.isHidden = true
            cell.txtEmail.tag = indexPath.section
            cell.txtEmail.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorMobile.isHidden = true
            cell.txtMobile.tag = indexPath.section
            cell.txtMobile.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.txtWorkingLocation.tag = indexPath.section
            cell.txtWorkingLocation.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorAadhar.isHidden = true
            cell.txtAadharNo.tag = indexPath.section
            cell.txtAadharNo.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.mainVIew.layer.cornerRadius = 8
            cell.mainVIew.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            
            let dropDownWorking = DropDown()
            let arrWorkingLocation = NSMutableArray()
            
            for objQu in arrWarehouse
            {
                arrWorkingLocation.add(objQu.descriptionField ?? "")
            }
            
            dropDownWorking.dataSource = arrWorkingLocation as! [String]
            dropDownWorking.anchorView = cell.viewTarget
            dropDownWorking.direction = .any
            
            dropDownWorking.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                cell.txtWorkingLocation.text = item
                
                for objQu in arrWarehouse
                {
                    if objQu.descriptionField == item
                    {
                        self.strWarehouseID = objQu.masterId
                        return
                    }
                    else
                    {
                        self.strWarehouseID = 0
                    }
                }
                
                if item == "Other"
                {
                    cell.viewOtherWorkingHieghtcont.constant = 50
                    cell.viewOtherTopCont.constant = 25
                    cell.viewOtherWorking.isHidden = false
                    
                }
                else
                {
                    cell.viewOtherWorkingHieghtcont.constant = 0
                    cell.viewOtherTopCont.constant = 0
                    cell.viewOtherWorking.isHidden = true
                }
                
                self.tblView.reloadData()
            }
            
            dropDownWorking.bottomOffset = CGPoint(x: 0, y: cell.viewTarget.bounds.height)
            dropDownWorking.topOffset = CGPoint(x: 0, y: -cell.viewTarget.bounds.height)
            dropDownWorking.dismissMode = .onTap
            dropDownWorking.textColor = UIColor.darkGray
            dropDownWorking.backgroundColor = UIColor.white
            dropDownWorking.selectionBackgroundColor = UIColor.clear
            dropDownWorking.reloadAllComponents()
            
            cell.btnWorkingLocation.accessibilityElements = [dropDownWorking]
            cell.btnWorkingLocation.tag = indexPath.row
            cell.btnWorkingLocation.addTarget(self, action: #selector(clickedPostdFamily(_ :)), for: .touchUpInside)
            
            if cell.txtWorkingLocation.text == "Other"
            {
                cell.viewOtherWorkingHieghtcont.constant = 50
                cell.viewOtherTopCont.constant = 25
                cell.viewOtherWorking.isHidden = false
            }
            else
            {
                cell.viewOtherWorkingHieghtcont.constant = 0
                cell.viewOtherTopCont.constant = 0
                cell.viewOtherWorking.isHidden = true
            }
            
            
            cell.txtFirstName.delegate = self
            
            cell.txtLastName.delegate = self
            
            cell.txtEmail.delegate = self
            
            cell.txtMobile.delegate = self
            
            cell.txtWorkingLocation.delegate = self
            
            cell.txtAadharNo.delegate = self
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "PasswordDetailsTableCell") as! PasswordDetailsTableCell
            
            cell.lblErrorPassword.isHidden = true
            cell.txtPassword.tag = indexPath.section
            cell.txtPassword.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorConfirmPassword.isHidden = true
            cell.txtConfirmPassword.tag = indexPath.section
            cell.txtConfirmPassword.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.mainView.layer.cornerRadius = 8
            cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            
            cell.txtPassword.delegate = self
            
            cell.txtConfirmPassword.delegate = self
            
            
            return cell
            
        }
        else if indexPath.section == 2
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "AddressDetailsTableCell") as! AddressDetailsTableCell
            
            cell.lblErrorPincode.isHidden = true
            cell.txtPinCode.tag = indexPath.section
            cell.txtPinCode.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.btnCheck.tag = indexPath.section
            cell.btnCheck.addTarget(self, action: #selector(clickedResidentAddress(_ :)), for: .touchUpInside)
            
            cell.mainView.layer.cornerRadius = 8
            cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            
            cell.txtPinCode.delegate = self
            
            cell.txtHome.delegate = self
            
            cell.txtResidentAddress.delegate = self
            
            cell.txtPer.delegate = self
            
            return cell
        }
        else if indexPath.section == 3
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "FamilyDetailsTableCell") as! FamilyDetailsTableCell
            
            cell.lblErrorRelativeName.isHidden = true
            cell.txtRelativeNAme.tag = indexPath.section
            cell.txtRelativeNAme.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.lblErrorAge.isHidden = true
            cell.txtAge.tag = indexPath.section
            cell.txtAge.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.btnCacnel.tag = indexPath.row
            cell.btnCacnel.addTarget(self, action: #selector(clickedCancelFamily(_ :)), for: .touchUpInside)
            
            cell.btnAddMore.tag = indexPath.row
            cell.btnAddMore.addTarget(self, action: #selector(clickedAddFamily(_ :)), for: .touchUpInside)
            
            cell.txtRelationShip.delegate = self
            cell.txtRelativeNAme.delegate = self
            cell.txtProfesssion.delegate = self
            cell.txtAge.delegate = self
            
            let dropDownRelationship = DropDown()
            let arrRelationship = NSMutableArray()
            
            for objRelation in arrRelation
            {
                arrRelationship.add(objRelation.descriptionField ?? "")
            }
            
            arrRelationship.add("Other")
            
            dropDownRelationship.dataSource = arrRelationship as! [String]
            dropDownRelationship.anchorView = cell.viewRelationTarget
            dropDownRelationship.direction = .any
            
            dropDownRelationship.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                self.strRelationShip = item
                
                if self.dicObject.object(forKey: indexPath.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(item, forKey: "relationship")
                    cell.txtRelationShip.text = item
                    self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(item, forKey: "relationship")
                    cell.txtRelationShip.text = item
                    self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    
                }
                
                if item == "Other"
                {
                    cell.relatonshipHieghtCont.constant = 50
                    cell.relationShipTopCont.constant = 25
                    cell.viewOtherRelationShip.isHidden = false
                    
                }
                else
                {
                    cell.relatonshipHieghtCont.constant = 0
                    cell.relationShipTopCont.constant = 0
                    cell.viewOtherRelationShip.isHidden = true
                }
                
                
                self.tblView.reloadData()
            }
            
            
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "relationship") != nil{
                    cell.txtRelationShip.text = dicData.value(forKey: "relationship") as? String
                }
                else{
                    cell.txtRelationShip.text = ""
                }
            }
            else{
                cell.txtRelationShip.text = ""
            }
            
            dropDownRelationship.bottomOffset = CGPoint(x: 0, y: cell.viewRelationTarget.bounds.height)
            dropDownRelationship.topOffset = CGPoint(x: 0, y: -cell.viewRelationTarget.bounds.height)
            dropDownRelationship.dismissMode = .onTap
            dropDownRelationship.textColor = UIColor.darkGray
            dropDownRelationship.backgroundColor = UIColor.white
            dropDownRelationship.selectionBackgroundColor = UIColor.clear
            dropDownRelationship.reloadAllComponents()
            
            cell.btnRelationshiopDrop.accessibilityElements = [dropDownRelationship]
            cell.btnRelationshiopDrop.tag = indexPath.row
            cell.btnRelationshiopDrop.addTarget(self, action: #selector(clickedPostdFamily(_ :)), for: .touchUpInside)
            
            if cell.txtRelationShip.text == "Other"
            {
                cell.relatonshipHieghtCont.constant = 50
                cell.relationShipTopCont.constant = 25
                cell.viewOtherRelationShip.isHidden = false
            }
            else
            {
                cell.relatonshipHieghtCont.constant = 0
                cell.relationShipTopCont.constant = 0
                cell.viewOtherRelationShip.isHidden = true
            }
            
            let dropDownProfession = DropDown()
            let arrProfession = NSMutableArray()
            
            for objPro in arrProfessions
            {
                arrProfession.add(objPro.descriptionField ?? "")
            }
            
            arrProfession.add("Other")
            
            dropDownProfession.dataSource = arrProfession as! [String]
            dropDownProfession.anchorView = cell.viewProfessionTarget
            dropDownProfession.direction = .any
            
            dropDownProfession.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.strProfession = item
                
                if self.dicObject.object(forKey: indexPath.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(item, forKey: "profession")
                    cell.txtProfesssion.text = item
                    self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(item, forKey: "profession")
                    cell.txtProfesssion.text = item
                    self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    
                }
                
                
                if item == "Other"
                {
                    cell.ProHieghtCont.constant = 50
                    cell.ProTopCont.constant = 25
                    cell.viewOtherpro.isHidden = false
                    
                }
                else
                {
                    cell.ProHieghtCont.constant = 0
                    cell.ProTopCont.constant = 0
                    cell.viewOtherpro.isHidden = true
                }
                
                self.tblView.reloadData()
            }
            
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "profession") != nil{
                    cell.txtProfesssion.text = dicData.value(forKey: "profession") as? String
                }
                else{
                    cell.txtProfesssion.text = ""
                }
            }
            else{
                cell.txtProfesssion.text = ""
            }
            
            dropDownProfession.bottomOffset = CGPoint(x: 0, y: cell.viewProfessionTarget.bounds.height)
            dropDownProfession.topOffset = CGPoint(x: 0, y: -cell.viewProfessionTarget.bounds.height)
            dropDownProfession.dismissMode = .onTap
            dropDownProfession.textColor = UIColor.darkGray
            dropDownProfession.backgroundColor = UIColor.white
            dropDownProfession.selectionBackgroundColor = UIColor.clear
            dropDownProfession.reloadAllComponents()
            
            cell.btnProfession.accessibilityElements = [dropDownProfession]
            cell.btnProfession.tag = indexPath.row
            cell.btnProfession.addTarget(self, action: #selector(clickedPostdPro(_ :)), for: .touchUpInside)
            
            if cell.txtProfesssion.text == "Other"
            {
                cell.ProHieghtCont.constant = 50
                cell.ProTopCont.constant = 25
                cell.viewOtherpro.isHidden = false
            }
            else
            {
                cell.ProHieghtCont.constant = 0
                cell.ProTopCont.constant = 0
                cell.viewOtherpro.isHidden = true
            }
            
            if indexPath.row == addFamilyDetails - 1
            {
                cell.viewcancel.isHidden = true
                cell.viewAddMore.isHidden = false
                cell.mainView.layer.cornerRadius = 8
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            }
            else
            {
                cell.viewcancel.isHidden = false
                cell.viewAddMore.isHidden = true
                cell.mainView.layer.cornerRadius = 0
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            }
            
            cell.txtRelativeNAme.delegate = self
            cell.txtRelativeNAme.accessibilityValue = "relativeName"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "relativeName") != nil{
                    cell.txtRelativeNAme.text = dicData.value(forKey: "relativeName") as? String
                }
                else{
                    cell.txtRelativeNAme.text = ""
                }
            }
            else{
                cell.txtRelativeNAme.text = ""
            }
            
            cell.txtOtherRelationShip.delegate = self
            cell.txtOtherRelationShip.accessibilityValue = "otherRelationship"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "otherRelationship") != nil{
                    cell.txtOtherRelationShip.text = dicData.value(forKey: "otherRelationship") as? String
                }
                else{
                    cell.txtOtherRelationShip.text = ""
                }
            }
            else{
                cell.txtOtherRelationShip.text = ""
            }
            
            cell.txtAge.delegate = self
            cell.txtAge.accessibilityValue = "age"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "age") != nil{
                    cell.txtAge.text = dicData.value(forKey: "age") as? String
                }
                else{
                    cell.txtAge.text = ""
                }
            }
            else{
                cell.txtAge.text = ""
            }
            
            cell.txtOtherPro.delegate = self
            cell.txtOtherPro.accessibilityValue = "otherProfession"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "otherProfession") != nil{
                    cell.txtOtherPro.text = dicData.value(forKey: "otherProfession") as? String
                }
                else{
                    cell.txtOtherPro.text = ""
                }
            }
            else{
                cell.txtOtherPro.text = ""
            }
            
            cell.txtRelationShip.delegate = self
            
            cell.txtOtherRelationShip.delegate = self
            
            cell.txtRelativeNAme.delegate = self
            
            cell.txtAge.delegate = self
            
            cell.txtProfesssion.delegate = self
            
            cell.txtOtherPro.delegate = self
            
            return cell
        }
        else if indexPath.section == 4
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "QualificationTableCell") as! QualificationTableCell
            
            if indexPath.row == 0
            {
                cell.txtHieghestQualification.placeholder = "Highest Qualification *"
                cell.txtOtherHiestQuali.placeholder = "Specify Other Highest Qualification *"
            }
            else
            {
                cell.txtHieghestQualification.placeholder = "Qualification *"
                cell.txtOtherHiestQuali.placeholder = "Specify Other Qualification *"
            }
            
            cell.txtMarksHighest.tag = indexPath.section
            cell.txtMarksHighest.addTarget(self, action: #selector(changeTextfield(_ :)), for: .editingChanged)
            
            cell.btnclose.tag = indexPath.row
            cell.btnclose.addTarget(self, action: #selector(clickedCancelQualification(_ :)), for: .touchUpInside)
            
            cell.btnAddMnoe.tag = indexPath.row
            cell.btnAddMnoe.addTarget(self, action: #selector(clickedAddQualification(_ :)), for: .touchUpInside)
            
            
            let dropDownHighestQualification = DropDown()
            let arrHighestQualification = NSMutableArray()
            
            for objQu in arrQualifications
            {
                arrHighestQualification.add(objQu.descriptionField ?? "")
            }
            
            arrHighestQualification.add("Other")
            
            dropDownHighestQualification.dataSource = arrHighestQualification as! [String]
            dropDownHighestQualification.anchorView = cell.viewTargetHieghestQualifiction
            dropDownHighestQualification.direction = .any
            
            dropDownHighestQualification.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                self.strHeightQua = item
                
                if item == "Other"
                {
                    
                    cell.HiestQualiHieghtCont.constant = 50
                    cell.HiestQualiTopCont.constant = 25
                    cell.viewOtherHiestQuali.isHidden = false
                    
                    if self.dicObject.object(forKey: indexPath.row) == nil{
                        
                        let dicData = NSMutableDictionary()
                        dicData.setValue(item, forKey: "qualification")
                        cell.txtHieghestQualification.text = item
                        self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    }
                    else{
                        let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        dicData.setValue(item, forKey: "qualification")
                        cell.txtHieghestQualification.text = item
                        self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    }
                    
                }
                else
                {
                    cell.HiestQualiHieghtCont.constant = 0
                    cell.HiestQualiTopCont.constant = 0
                    cell.viewOtherHiestQuali.isHidden = true
                    
                    if self.dicObject.object(forKey: indexPath.row) == nil{
                        
                        let dicData = NSMutableDictionary()
                        dicData.setValue(item, forKey: "qualification")
                        cell.txtHieghestQualification.text = item
                        self.arrUploadDocument.add(item)
                        self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    }
                    else{
                        let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                        dicData.setValue(item, forKey: "qualification")
                        cell.txtHieghestQualification.text = item
                        self.arrUploadDocument.add(item)
                        self.dicObject.setObject(dicData, forKey: indexPath.row as NSCopying)
                    }
                    
                }
                
                self.tblView.reloadData()
            }
            
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "qualification") != nil{
                    cell.txtHieghestQualification.text = dicData.value(forKey: "qualification") as? String
                }
                else{
                    cell.txtHieghestQualification.text = ""
                }
            }
            else{
                cell.txtHieghestQualification.text = ""
            }
            
            dropDownHighestQualification.bottomOffset = CGPoint(x: 0, y: cell.viewTargetHieghestQualifiction.bounds.height)
            dropDownHighestQualification.topOffset = CGPoint(x: 0, y: -cell.viewTargetHieghestQualifiction.bounds.height)
            dropDownHighestQualification.dismissMode = .onTap
            dropDownHighestQualification.textColor = UIColor.darkGray
            dropDownHighestQualification.backgroundColor = UIColor.white
            dropDownHighestQualification.selectionBackgroundColor = UIColor.clear
            dropDownHighestQualification.reloadAllComponents()
            
            cell.btnHieghestQualification.accessibilityElements = [dropDownHighestQualification]
            cell.btnHieghestQualification.tag = indexPath.row
            cell.btnHieghestQualification.addTarget(self, action: #selector(clickedPostdQua(_ :)), for: .touchUpInside)
            
            if cell.txtHieghestQualification.text == "Other"
            {
                
                cell.HiestQualiHieghtCont.constant = 50
                cell.HiestQualiTopCont.constant = 25
                cell.viewOtherHiestQuali.isHidden = false
            }
            else
            {
                cell.HiestQualiHieghtCont.constant = 0
                cell.HiestQualiTopCont.constant = 0
                cell.viewOtherHiestQuali.isHidden = true
            }
            
            if indexPath.row == addQualificationDetails - 1
            {
                cell.viewclose.isHidden = true
                cell.viewAddMore.isHidden = false
                cell.mainView.layer.cornerRadius = 8
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            }
            else
            {
                cell.viewclose.isHidden = false
                cell.viewAddMore.isHidden = true
                cell.mainView.layer.cornerRadius = 0
                cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            }
            
            
            cell.txtMarksHighest.delegate = self
            cell.txtMarksHighest.accessibilityValue = "marks"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "marks") != nil{
                    cell.txtMarksHighest.text = dicData.value(forKey: "marks") as? String
                }
                else{
                    cell.txtMarksHighest.text = ""
                }
            }
            else{
                cell.txtMarksHighest.text = ""
            }
            
            
            cell.txtOtherHiestQuali.delegate = self
            cell.txtOtherHiestQuali.accessibilityValue = "otherQualification"
            if self.dicObject.object(forKey: indexPath.row) != nil{
                
                let dicData = (self.dicObject.object(forKey: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                
                if dicData.value(forKey: "otherQualification") != nil{
                    cell.txtOtherHiestQuali.text = dicData.value(forKey: "otherQualification") as? String
                }
                else{
                    cell.txtOtherHiestQuali.text = ""
                }
            }
            else{
                cell.txtOtherHiestQuali.text = ""
            }
            
            return cell
        }
        else
        {
            let cell = tblView.dequeueReusableCell(withIdentifier: "UploadDocTableCell") as! UploadDocTableCell
            
            let dropDownDocument = DropDown()
            
            dropDownDocument.dataSource = arrUploadDocument as! [String]
            dropDownDocument.anchorView = cell.viewTargetDocumentc
            dropDownDocument.direction = .any
            
            dropDownDocument.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                
                cell.txtDocument.text = item
                
                if item == "Other"
                {
                    cell.DocumentHieghtCont.constant = 50
                    cell.DocumentTopCont.constant = 25
                    cell.viewOtherDocument.isHidden = false
                    
                }
                else
                {
                    cell.DocumentHieghtCont.constant = 0
                    cell.DocumentTopCont.constant = 0
                    cell.viewOtherDocument.isHidden = true
                }
                self.tblView.reloadData()
            }
            
            if arrUploadAllDocument.count == 0
            {
                cell.view1.isHidden = true
                cell.view1Cont.constant = 0
                
                cell.view2.isHidden = true
                cell.view2Cont.constant = 0
                
                cell.view3.isHidden = true
                cell.view3Cont.constant = 0
                
                cell.view4.isHidden = true
                cell.view4Cont.constant = 0
                
                cell.view5.isHidden = true
                cell.view5Cont.constant = 0
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
            }
            
            if arrUploadAllDocument.count > 0
            {
                let dicObj = arrUploadAllDocument[0] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName1.text = name
                
                cell.view1.isHidden = false
                cell.view1Cont.constant = 50
                
                cell.view2.isHidden = true
                cell.view2Cont.constant = 0
                
                cell.view3.isHidden = true
                cell.view3Cont.constant = 0
                
                cell.view4.isHidden = true
                cell.view4Cont.constant = 0
                
                cell.view5.isHidden = true
                cell.view5Cont.constant = 0
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash1.tag = 0
                cell.btnTrash1.addTarget(self, action: #selector(clickedTrashDoc1(_ :)), for: .touchUpInside)
            }
            
            
            if arrUploadAllDocument.count > 1
            {
                let dicObj = arrUploadAllDocument[1] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName2.text = name
                
                cell.view2.isHidden = false
                cell.view2Cont.constant = 50
                
                cell.view3.isHidden = true
                cell.view3Cont.constant = 0
                
                cell.view4.isHidden = true
                cell.view4Cont.constant = 0
                
                cell.view5.isHidden = true
                cell.view5Cont.constant = 0
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash2.tag = 1
                cell.btnTrash2.addTarget(self, action: #selector(clickedTrashDoc2(_ :)), for: .touchUpInside)
            }
            
            if arrUploadAllDocument.count > 2
            {
                let dicObj = arrUploadAllDocument[2] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName3.text = name
                cell.view3.isHidden = false
                cell.view3Cont.constant = 50
                
                
                cell.view4.isHidden = true
                cell.view4Cont.constant = 0
                
                cell.view5.isHidden = true
                cell.view5Cont.constant = 0
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash3.tag = 2
                cell.btnTrash3.addTarget(self, action: #selector(clickedTrashDoc3(_ :)), for: .touchUpInside)
            }
            
            if arrUploadAllDocument.count > 3
            {
                let dicObj = arrUploadAllDocument[3] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName4.text = name
                cell.view4.isHidden = false
                cell.view4Cont.constant = 50
                
                cell.view5.isHidden = true
                cell.view5Cont.constant = 0
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash4.tag = 3
                cell.btnTrash4.addTarget(self, action: #selector(clickedTrashDoc4(_ :)), for: .touchUpInside)
            }
            
            if arrUploadAllDocument.count > 4
            {
                let dicObj = arrUploadAllDocument[4] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName5.text = name
                cell.view5.isHidden = false
                cell.view5Cont.constant = 50
                
                cell.view6.isHidden = true
                cell.view5Cont.constant = 0
                
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash5.tag = 4
                cell.btnTrash5.addTarget(self, action: #selector(clickedTrashDoc5(_ :)), for: .touchUpInside)
            }
            
            if arrUploadAllDocument.count > 5
            {
                let dicObj = arrUploadAllDocument[5] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName6.text = name
                cell.view6.isHidden = false
                cell.view6Cont.constant = 50
                
                cell.view7.isHidden = true
                cell.view7Cont.constant = 0
                
                cell.btnTrash6.tag = 5
                cell.btnTrash6.addTarget(self, action: #selector(clickedTrashDoc6(_ :)), for: .touchUpInside)
            }
            
            if arrUploadAllDocument.count > 6
            {
                let dicObj = arrUploadAllDocument[6] as? NSDictionary
                
                let name = dicObj?.value(forKey: "documentName") as! String
                cell.lblName7.text = name
                cell.view7.isHidden = false
                cell.view7Cont.constant = 50
                
                cell.btnTrash7.tag = 6
                cell.btnTrash7.addTarget(self, action: #selector(clickedTrashDoc7(_ :)), for: .touchUpInside)
            }
            
            dropDownDocument.bottomOffset = CGPoint(x: 0, y: cell.viewTargetDocumentc.bounds.height)
            dropDownDocument.topOffset = CGPoint(x: 0, y: -cell.viewTargetDocumentc.bounds.height)
            dropDownDocument.dismissMode = .onTap
            dropDownDocument.textColor = UIColor.darkGray
            dropDownDocument.backgroundColor = UIColor.white
            dropDownDocument.selectionBackgroundColor = UIColor.clear
            dropDownDocument.reloadAllComponents()
            
            cell.btnDocument.accessibilityElements = [dropDownDocument]
            cell.btnDocument.tag = indexPath.row
            cell.btnDocument.addTarget(self, action: #selector(clickedPostdFamily(_ :)), for: .touchUpInside)
            
            if cell.txtDocument.text == "Other"
            {
                cell.DocumentHieghtCont.constant = 50
                cell.DocumentTopCont.constant = 25
                cell.viewOtherDocument.isHidden = false
            }
            else
            {
                cell.DocumentHieghtCont.constant = 0
                cell.DocumentTopCont.constant = 0
                cell.viewOtherDocument.isHidden = true
            }
            
            cell.mainView.layer.cornerRadius = 8
            cell.mainView.layer.maskedCorners = [.layerMinXMaxYCorner, . layerMaxXMaxYCorner]
            
            cell.btnUploadDocument.addTarget(self, action: #selector(clickedUploadDoc(_ :)), for: .touchUpInside)
            
            return cell
            
        }
    }
    
    @objc func clickedTrashDoc1(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 0)
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc2(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 1)
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc3(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 2)
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc4(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at:3 )
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc5(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 4)
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc6(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 5)
        
        tblView.reloadData()
    }
    
    @objc func clickedTrashDoc7(_ sender: UIButton)
    {
        self.arrUploadAllDocument.removeObject(at: 6)
        
        tblView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedHeader == indexPath.section
        {
            return UITableView.automaticDimension
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("SignUpHeaderView", owner: self, options: [:])?.first as! SignUpHeaderView
        
        let dicData = arrHeaderView[section]
        
        headerView.lblTitle.text = dicData
        
        if selectedHeader == section
        {
            headerView.imgSrrow.image = UIImage(named: "btn_Down")
            headerView.mainView.layer.cornerRadius = 8
            headerView.mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        }
        else
        {
            headerView.imgSrrow.image = UIImage(named: "btn_next")
            headerView.mainView.layer.cornerRadius = 8
            headerView.mainView.clipsToBounds = true
        }
        
        if section == 0
        {
            headerView.lblSignuo.isHidden = false
        }
        else
        {
            headerView.lblSignuo.isHidden = true
        }
        
        headerView.btnOpen.tag = section
        headerView.btnOpen.addTarget(self, action: #selector(clickedOpenView(_:)), for: .touchUpInside)
        
        return headerView
    }
    
    @objc func clickedOpenView(_ sender: UIButton)
    {
        if selectedHeader == sender.tag
        {
            selectedHeader = -1
        }
        else
        {
            selectedHeader = sender.tag
        }
        
        tblView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0
        {
            return 135
        }
        else
        {
            return 45
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("SignUpFooterView", owner: self, options: [:])?.first as! SignUpFooterView
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    
    //MARK: - Action Method
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSignup(_ sender: UIButton) {
        
        
        let qualificationcell = tblView.visibleCells.first as? QualificationTableCell
        
        let UploadDocTableCell = tblView.visibleCells.first as? UploadDocTableCell
        
        
        
        if strFirstName == ""
        {
            self.view.makeToast("Please enter your first name")
        }
        else if strLastName == ""
        {
            self.view.makeToast("Please enter your last name")
        }
        else if strEmailName == ""
        {
            self.view.makeToast("Please enter email")
        }
        else if !isValidEmail(testStr: strEmailName)
        {
            self.view.makeToast("Please enter valid email")
        }
        else if strPhoneName == ""
        {
            self.view.makeToast("Please enter mobile number")
        }
        else if strWorkingLocationName == ""
        {
            self.view.makeToast("Please enter working location")
        }
        else if strAdharName == ""
        {
            self.view.makeToast("Please enter aadhar number")
        }
        else if strPasswordName == ""
        {
            self.view.makeToast("Please enter password")
        }
        else if strConfirmPasswordName == ""
        {
            self.view.makeToast("Please enter confirm password")
        }
        else if strConfirmPasswordName != strPasswordName
        {
            self.view.makeToast("password are not matching")
        }
        else if strPinCode == ""
        {
            self.view.makeToast("Please enter your pincode")
        }
        else if strHome == ""
        {
            self.view.makeToast("Please enter home towm/village/city name")
        }
        else if strResident == ""
        {
            self.view.makeToast("Please enter residential address")
        }
        else if strPerment == ""
        {
            self.view.makeToast("Please enter permanent address")
        }
        else if strRelationShip == ""
        {
            self.view.makeToast("Please enter relationShip")
        }
        else if strRelativeName == ""
        {
            self.view.makeToast("Please enter relative name")
        }
        else if strAge == ""
        {
            self.view.makeToast("Please enter age")
        }
        else if strProfession == ""
        {
            self.view.makeToast("Please enter profession")
        }
        else if strHeightQua == ""
        {
            self.view.makeToast("Please enter Qualification")
        }
        else if strMarks == ""
        {
            self.view.makeToast("Please enter Marks")
        }
        else if arrUploadDocument.count != arrUploadAllDocument.count
        {
            self.view.makeToast("Please uplaod all document")
        }
        else
        {
            
            DispatchQueue.main.async {
                self.callSignUpAPI()
            }
            
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    @IBAction func clickedChooseCamera(_ sender: Any) {
        viewPopup.isHidden = true
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func clickedGalaery(_ sender: Any) {
        viewPopup.isHidden = true
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            if self.isUploadDoc == false
            {
                let index = IndexPath(row: 0, section: 0)
                
                let cell = tblView.cellForRow(at: index) as! PersonalDetailsCell
                
                let data = pickedImage.jpegData(compressionQuality: 0.4)
                
                self.strprofileImageByteArray = data! as NSData
                
                cell.btnChooseOption.setImage(pickedImage, for: .normal)
            }
            else
            {
                let index = IndexPath(row: 0, section: 5)
                
                let cell = tblView.cellForRow(at: index) as! UploadDocTableCell
                
                if arrUploadAllDocument.count > 0
                {
                    
                    for dicDatta in arrUploadAllDocument
                    {
                        let dicObj = dicDatta as? NSDictionary
                        
                        let name = dicObj?.value(forKey: "documentName") as! String
                        
                        if name == cell.txtDocument.text ?? ""
                        {
                            self.arrUploadAllDocument.remove(dicDatta)
                        }
                    }
                }
                
                let dicUploadDoc = NSMutableDictionary()
                dicUploadDoc.setValue(cell.txtDocument.text ?? "", forKey: "documentName")
                
                let data = pickedImage.jpegData(compressionQuality: 0.4)
                
                let arrayByetsProfilePic = self.getArrayOfBytesFromImage(imageData: data! as NSData)
                let dataImages = NSKeyedArchiver.archivedData(withRootObject: pickedImage)
                dicUploadDoc.setValue(dataImages, forKey: "documentByteData")
                dicUploadDoc.setValue(arrayByetsProfilePic, forKey: "documentByteArray")
                
                if cell.txtDocument.text == "Aadhaar"
                {
                    dicUploadDoc.setValue("AADHAAR", forKey: "documentCode")
                }
                else
                {
                    dicUploadDoc.setValue("QC\(self.arrUploadAllDocument.count)", forKey: "documentCode")
                }
                
                dicUploadDoc.setValue(false, forKey: "isActive")
                dicUploadDoc.setValue(true, forKey: "isDeletable")
                dicUploadDoc.setValue(false, forKey: "isDownloadable")
                
                self.arrUploadAllDocument.add(dicUploadDoc)
                
                self.tblView.reloadData()
            }
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func getArrayOfBytesFromImage(imageData:NSData) -> NSMutableArray
    {
        
        // the number of elements:
        let count = imageData.length / MemoryLayout<UInt8>.size
        
        // create array of appropriate length:
        var bytes = [UInt8](repeating: 0, count: count)
        // copy bytes into array
        imageData.getBytes(&bytes, length:count)
        
        let byteArray:NSMutableArray = NSMutableArray()
        
        for i in 0..<bytes.count {
            byteArray.add(NSNumber(value: bytes[i]))
        }
        
        return byteArray
    }
    
    
    @IBAction func clickedCancel(_ sender: Any) {
        viewPopup.isHidden = true
    }
    
    
    //MARK: - UITextField delegate
    
    //    func textViewDidEndEditing(_ textView: UITextView) {
    //
    //        var cell : UITableViewCell!
    //
    //        var tempView : UIView = textView
    //
    //        while(true){
    //
    //            if tempView.isKind(of: AddressDetailsTableCell.self){
    //
    //                cell = tempView as? AddressDetailsTableCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! AddressDetailsTableCell
    //
    //                strResident = newCell.txtResidentAddress.text ?? ""
    //                strPerment = newCell.txtPer.text ?? ""
    //
    //                break
    //            }
    //            else{
    //                tempView = tempView.superview!
    //            }
    //
    //        }
    //
    //    }
    //
    func textViewDidChange(_ textView: UITextView) {
        
        var cell : UITableViewCell!
        
        var tempView : UIView = textView
        
        while(true){
            
            if tempView.isKind(of: AddressDetailsTableCell.self){
                
                cell = tempView as? AddressDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! AddressDetailsTableCell
                
                strResident = newCell.txtResidentAddress.text ?? ""
                strPerment = newCell.txtPer.text ?? ""
                
                break
            }
            else{
                tempView = tempView.superview!
            }
            
        }
        
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        var cell : UITableViewCell!
        
        var tempView : UIView = textField
        
        while(true){
            
            
            if tempView.isKind(of: QualificationTableCell.self){
                
                cell = tempView as? QualificationTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! QualificationTableCell
                
                strHeightQua = newCell.txtHieghestQualification.text ?? ""
                strMarks = newCell.txtMarksHighest.text ?? ""
                
                
                break
            }
            else if tempView.isKind(of: PersonalDetailsCell.self){
                
                cell = tempView as? PersonalDetailsCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! PersonalDetailsCell
                
                strFirstName = newCell.txtFirstName.text ?? ""
                strLastName = newCell.txtLastName.text ?? ""
                strEmailName = newCell.txtEmail.text ?? ""
                strPhoneName = newCell.txtMobile.text ?? ""
                strWorkingLocationName = newCell.txtWorkingLocation.text ?? ""
                strAdharName = newCell.txtAadharNo.text ?? ""
                
                break
            }
            else if tempView.isKind(of: PasswordDetailsTableCell.self){
                
                cell = tempView as? PasswordDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! PasswordDetailsTableCell
                
                strPasswordName = newCell.txtPassword.text ?? ""
                strConfirmPasswordName = newCell.txtConfirmPassword.text ?? ""
                
                break
            }
            else if tempView.isKind(of: AddressDetailsTableCell.self){
                
                cell = tempView as? AddressDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! AddressDetailsTableCell
                
                strPinCode = newCell.txtPinCode.text ?? ""
                strHome = newCell.txtHome.text ?? ""
                
                break
            }
            else if tempView.isKind(of: FamilyDetailsTableCell.self){
                
                cell = tempView as? FamilyDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! FamilyDetailsTableCell
                
                strRelationShip = newCell.txtRelationShip.text ?? ""
                strRelativeName = newCell.txtRelativeNAme.text ?? ""
                strAge = newCell.txtAge.text ?? ""
                strProfession = newCell.txtProfesssion.text ?? ""
                
                break
            }
            else{
                tempView = tempView.superview!
            }
            
        }
        
        let indexPath = tblView.indexPath(for: cell)
        
        if indexPath?.section == 3
        {
            
            if textField.accessibilityValue == "relativeName"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(textField.text!, forKey: "relativeName")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(textField.text!, forKey: "relativeName")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
            if textField.accessibilityValue == "otherRelationship"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(textField.text!, forKey: "otherRelationship")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(textField.text!, forKey: "otherRelationship")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
            if textField.accessibilityValue == "age"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(textField.text!, forKey: "age")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(textField.text!, forKey: "age")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
            if textField.accessibilityValue == "otherProfession"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(textField.text!, forKey: "otherProfession")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(textField.text!, forKey: "otherProfession")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
            
        }
        else if indexPath?.section == 4
        {
            if textField.accessibilityValue == "marks"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    
                    let dicData = NSMutableDictionary()
                    dicData.setValue(textField.text!, forKey: "marks")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    dicData.setValue(textField.text!, forKey: "marks")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
            if textField.accessibilityValue == "otherQualification"{
                
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    let dicData = NSMutableDictionary()
                   // self.arrUploadDocument.add(textField.text!)
                    dicData.setValue(textField.text!, forKey: "otherQualification")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
                else{
                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
                 //   self.arrUploadDocument.add(textField.text!)
                    dicData.setValue(textField.text!, forKey: "otherQualification")
                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
                }
            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        var cell : UITableViewCell!
        
        var tempView : UIView = textField
        
        while(true){
            
            if tempView.isKind(of: QualificationTableCell.self){
                
                cell = tempView as? QualificationTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! QualificationTableCell
                
                strHeightQua = newCell.txtHieghestQualification.text ?? ""
                strMarks = newCell.txtMarksHighest.text ?? ""
                
                
                break
            }
            else if tempView.isKind(of: PersonalDetailsCell.self){
                
                cell = tempView as? PersonalDetailsCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! PersonalDetailsCell
                
                strFirstName = newCell.txtFirstName.text ?? ""
                strLastName = newCell.txtLastName.text ?? ""
                strEmailName = newCell.txtEmail.text ?? ""
                strPhoneName = newCell.txtMobile.text ?? ""
                strWorkingLocationName = newCell.txtWorkingLocation.text ?? ""
                strAdharName = newCell.txtAadharNo.text ?? ""
                
                break
            }
            else if tempView.isKind(of: PasswordDetailsTableCell.self){
                
                cell = tempView as? PasswordDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! PasswordDetailsTableCell
                
                strPasswordName = newCell.txtPassword.text ?? ""
                strConfirmPasswordName = newCell.txtConfirmPassword.text ?? ""
                
                break
            }
            else if tempView.isKind(of: AddressDetailsTableCell.self){
                
                cell = tempView as? AddressDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! AddressDetailsTableCell
                
                strPinCode = newCell.txtPinCode.text ?? ""
                strHome = newCell.txtHome.text ?? ""
                
                break
            }
            else if tempView.isKind(of: FamilyDetailsTableCell.self){
                
                cell = tempView as? FamilyDetailsTableCell
                
                let indexPath = tblView.indexPath(for: cell)
                
                let newCell = tblView.cellForRow(at: indexPath!) as! FamilyDetailsTableCell
                
                strRelationShip = newCell.txtRelationShip.text ?? ""
                strRelativeName = newCell.txtRelativeNAme.text ?? ""
                strAge = newCell.txtAge.text ?? ""
                strProfession = newCell.txtProfesssion.text ?? ""
                
                break
            }
            else{
                tempView = tempView.superview!
            }
            
        }
        
        let indexPath = tblView.indexPath(for: cell)
        
        if indexPath?.section == 4
        {
            if textField.accessibilityValue == "otherQualification"
            {
                if self.dicObject.object(forKey: indexPath!.row) == nil{
                    self.arrUploadDocument.add(textField.text!)
                }
                else{
                    self.arrUploadDocument.add(textField.text!)
                }
            }
        }
        
    }
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //
    //        var cell : UITableViewCell!sd
    //
    //        var tempView : UIView = textField
    //
    //        while(true){
    //
    //
    //            if tempView.isKind(of: QualificationTableCell.self){
    //
    //                cell = tempView as? QualificationTableCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! QualificationTableCell
    //
    //                strHeightQua = newCell.txtHieghestQualification.text ?? ""
    //                strMarks = newCell.txtMarksHighest.text ?? ""
    //
    //
    //                break
    //            }
    //            else if tempView.isKind(of: PersonalDetailsCell.self){
    //
    //                cell = tempView as? PersonalDetailsCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! PersonalDetailsCell
    //
    //                strFirstName = newCell.txtFirstName.text ?? ""
    //                strLastName = newCell.txtLastName.text ?? ""
    //                strEmailName = newCell.txtEmail.text ?? ""
    //                strPhoneName = newCell.txtMobile.text ?? ""
    //                strWorkingLocationName = newCell.txtWorkingLocation.text ?? ""
    //                strAdharName = newCell.txtAadharNo.text ?? ""
    //
    //                break
    //            }
    //            else if tempView.isKind(of: PasswordDetailsTableCell.self){
    //
    //                cell = tempView as? PasswordDetailsTableCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! PasswordDetailsTableCell
    //
    //                strPasswordName = newCell.txtPassword.text ?? ""
    //                strConfirmPasswordName = newCell.txtConfirmPassword.text ?? ""
    //
    //                break
    //            }
    //            else if tempView.isKind(of: AddressDetailsTableCell.self){
    //
    //                cell = tempView as? AddressDetailsTableCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! AddressDetailsTableCell
    //
    //                strPinCode = newCell.txtPinCode.text ?? ""
    //                strHome = newCell.txtHome.text ?? ""
    //
    //                break
    //            }
    //            else if tempView.isKind(of: FamilyDetailsTableCell.self){
    //
    //                cell = tempView as? FamilyDetailsTableCell
    //
    //                let indexPath = tblView.indexPath(for: cell)
    //
    //                let newCell = tblView.cellForRow(at: indexPath!) as! FamilyDetailsTableCell
    //
    //                strRelationShip = newCell.txtRelationShip.text ?? ""
    //                strRelativeName = newCell.txtRelativeNAme.text ?? ""
    //                strAge = newCell.txtAge.text ?? ""
    //                strProfession = newCell.txtProfesssion.text ?? ""
    //
    //                break
    //            }
    //            else{
    //                tempView = tempView.superview!
    //            }
    //
    //        }
    //
    //        let indexPath = tblView.indexPath(for: cell)
    //
    //        if indexPath?.section == 3
    //        {
    //
    //            if textField.accessibilityValue == "relativeName"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    dicData.setValue(textField.text!, forKey: "relativeName")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    dicData.setValue(textField.text!, forKey: "relativeName")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //            if textField.accessibilityValue == "otherRelationship"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    dicData.setValue(textField.text!, forKey: "otherRelationship")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    dicData.setValue(textField.text!, forKey: "otherRelationship")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //            if textField.accessibilityValue == "age"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    dicData.setValue(textField.text!, forKey: "age")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    dicData.setValue(textField.text!, forKey: "age")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //            if textField.accessibilityValue == "otherProfession"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    dicData.setValue(textField.text!, forKey: "otherProfession")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    dicData.setValue(textField.text!, forKey: "otherProfession")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //
    //        }
    //        else if indexPath?.section == 4
    //        {
    //            if textField.accessibilityValue == "marks"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    dicData.setValue(textField.text!, forKey: "marks")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    dicData.setValue(textField.text!, forKey: "marks")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //            if textField.accessibilityValue == "otherQualification"{
    //
    //                if self.dicObject.object(forKey: indexPath!.row) == nil{
    //
    //                    let dicData = NSMutableDictionary()
    //                    self.arrUploadDocument.add(textField.text!)
    //                    dicData.setValue(textField.text!, forKey: "otherQualification")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //                else{
    //                    let dicData = (self.dicObject.object(forKey: indexPath!.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
    //                    self.arrUploadDocument.add(textField.text!)
    //                    dicData.setValue(textField.text!, forKey: "otherQualification")
    //                    self.dicObject.setObject(dicData, forKey: indexPath!.row as NSCopying)
    //                }
    //            }
    //
    //        }
    //    }
    
    
    //MARK: - API Calling
    func callRelationsListAPI() {
        
        self.view.hideAllToasts()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_RELATIONS, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.arrRelation.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPSignupList(fromDictionary: obj as! NSDictionary)
                        self.arrRelation.append(dicData)
                    }
                    
                    self.tblView.reloadData()
                    
                }
                else
                {
                    self.arrRelation.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrRelation.removeAll()
                self.tblView.reloadData()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    
    func callProfessionsListAPI() {
        
        self.view.hideAllToasts()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_PROFESSIONS, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.arrProfessions.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPSignupList(fromDictionary: obj as! NSDictionary)
                        self.arrProfessions.append(dicData)
                    }
                    
                    self.tblView.reloadData()
                    
                }
                else
                {
                    self.arrProfessions.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrProfessions.removeAll()
                self.tblView.reloadData()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    
    func callQualificationsListAPI() {
        
        self.view.hideAllToasts()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_QUALIFICATIONS, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            if error == nil
            {
                self.arrQualifications.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPSignupList(fromDictionary: obj as! NSDictionary)
                        self.arrQualifications.append(dicData)
                    }
                    
                    self.tblView.reloadData()
                    
                }
                else
                {
                    self.arrQualifications.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrQualifications.removeAll()
                self.tblView.reloadData()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    
    func callWarehouseListAPI() {
        
        
        
        self.view.hideAllToasts()
        
        let param = ["": ""]
        
        print(param)
        
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(GET_WAREHOUSE, parameters: param) { (response, error, statusCode) in
            
            print("STATUS CODE \(String(describing: statusCode))")
            print("RESPONSE \(String(describing: response))")
            
            let success = response?.value(forKey: "status") as? Int
            let message = response?.value(forKey: "message") as? String
            
            
            if error == nil
            {
                self.arrWarehouse.removeAll()
                
                if response!.count > 0
                {
                    for obj in response!
                    {
                        let dicData = OPWarehouse(fromDictionary: obj as! NSDictionary)
                        self.arrWarehouse.append(dicData)
                    }
                    
                    self.tblView.reloadData()
                    
                }
                else
                {
                    self.arrWarehouse.removeAll()
                    self.tblView.reloadData()
                }
            }
            else
            {
                self.arrWarehouse.removeAll()
                self.tblView.reloadData()
                
                APIClient.sharedInstance.hideIndicator()
                self.view.makeToast(message)
            }
        }
    }
    
    func callSignUpAPI() {
        
        SVProgressHUD.show()
        
        self.tblView.reloadData()
        
        let parameters = NSMutableDictionary()
        
        let arrayByetsProfilePic = self.getArrayOfBytesFromImage(imageData: self.strprofileImageByteArray)
        
        // Persoanl
        parameters.setValue(strFirstName, forKey: "firstName")
        parameters.setValue(strLastName, forKey: "lastName")
        parameters.setValue(strEmailName, forKey: "email")
        parameters.setValue(strPhoneName, forKey: "mobileNumber")
        parameters.setValue(strWarehouseID, forKey: "wareHouseId")
        parameters.setValue(strWorkingLocationName, forKey: "wareHouse")
        parameters.setValue(false, forKey: "isAssignedToWarehouse")
        parameters.setValue(strAdharName, forKey: "aadharNumber")
        parameters.setValue(arrayByetsProfilePic, forKey: "profileImageByteArray")
        
        
        // Password
        parameters.setValue(strPasswordName, forKey: "password")
        parameters.setValue(false, forKey: "isPasswordResetReqApproved")
        parameters.setValue(false, forKey: "isPasswordResetReqSent")
        
        
        // Address
        let dicAddress = NSMutableDictionary()
        dicAddress.setValue(strHome, forKey: "homeTown")
        dicAddress.setValue(strPerment, forKey: "permanentAddress")
        dicAddress.setValue(strPinCode, forKey: "pinCode")
        dicAddress.setValue(strResident, forKey: "residentialAddress")
        if strPerment == strResident
        {
            dicAddress.setValue(true, forKey: "sameAddress")
        }
        else
        {
            dicAddress.setValue(false, forKey: "sameAddress")
        }
        parameters.setValue(dicAddress, forKey: "address")
        
        
        // relationDetail
        let arrRelationDetail = NSMutableArray()
        
        for key in self.dicObject.allKeys{
            
            let dicRelation = NSMutableDictionary()
            
            if self.dicObject.object(forKey: key) != nil{
                
                let object = self.dicObject.object(forKey: key) as! NSDictionary
                
                let profession = object.value(forKey: "profession")
                let relativeName = object.value(forKey: "relativeName")
                let age = object.value(forKey: "age")
                let relationShip = object.value(forKey: "relationship")
                let otherRelationship = object.value(forKey: "otherRelationship")
                let otherProfession = object.value(forKey: "otherProfession")
                
                if relationShip as? String == "Other"
                {
                   // dicRelation.setValue(0, forKey: "relationId")
                    dicRelation.setValue(otherRelationship, forKey: "relationDescription")
                }
                else
                {
                    for objRelation in arrRelation
                    {
                        if objRelation.descriptionField == relationShip as? String
                        {
                            dicRelation.setValue(objRelation.masterId, forKey: "relationId")
                        }
                    }
                    
                    dicRelation.setValue(relationShip, forKey: "relationDescription")
                }
                
                if profession as? String == "Other"
                {
                    //dicRelation.setValue(0, forKey: "professionId")
                    dicRelation.setValue(otherProfession, forKey: "profession")
                }
                else
                {
                    
                    for objProfessions in arrProfessions
                    {
                        if objProfessions.descriptionField == profession as? String
                        {
                            dicRelation.setValue(objProfessions.masterId, forKey: "professionId")
                        }
                    }
                    
                    dicRelation.setValue(profession, forKey: "profession")
                }
                
                dicRelation.setValue(age, forKey: "relationAge")
                dicRelation.setValue(relativeName, forKey: "relativeName")
                
                if dicRelation.count > 0{
                    arrRelationDetail.add(dicRelation)

                }
                
            }
            
        }
        parameters.setValue(arrRelationDetail, forKey: "relationDetail")
        
        // qualification
        let arrQualificationDetail = NSMutableArray()
        
        for key in self.dicObject.allKeys{
            
            let dicQualification = NSMutableDictionary()
            
            if self.dicObject.object(forKey: key) != nil{
                
                let object = self.dicObject.object(forKey: key) as! NSDictionary
                
                let qualification = object.value(forKey: "qualification")
                let marks = object.value(forKey: "marks")
                let otherQualification = object.value(forKey: "otherQualification")
                
                if qualification as? String == "Other"
                {
                   //dicQualification.setValue(0, forKey: "qualificationId")
                    dicQualification.setValue(otherQualification, forKey: "qualificationName")
                }
                else
                {
                    for objQualifications in arrQualifications
                    {
                        if objQualifications.descriptionField == qualification as? String
                        {
                            dicQualification.setValue(objQualifications.masterId, forKey: "qualificationId")
                        }
                    }
                    
                    dicQualification.setValue(qualification, forKey: "qualificationName")
                }
                
                dicQualification.setValue(marks, forKey: "percentage")
                
                if dicQualification.count > 0{
                    arrQualificationDetail.add(dicQualification)
                }
                
            }
            
        }
        parameters.setValue(arrQualificationDetail, forKey: "qualification")
        
        
        self.arrNewUploadAllDocument.removeAllObjects()
        
        for objUplaod in arrUploadAllDocument
        {
            let dicUploadDoc = NSMutableDictionary()
            
            let dicData = objUplaod as? NSDictionary
            let documentName = dicData!.value(forKey: "documentName")
            let documentByteArray = dicData!.value(forKey: "documentByteArray")
            let documentCode = dicData!.value(forKey: "documentCode")
            let isActive = dicData!.value(forKey: "isActive")
            let isDeletable = dicData!.value(forKey: "isDeletable")
            let isDownloadable = dicData!.value(forKey: "isDownloadable")
            
            dicUploadDoc.setValue(documentName, forKey: "documentName")
            dicUploadDoc.setValue(documentByteArray, forKey: "documentByteArray")
            dicUploadDoc.setValue("1", forKey: "documentByteData")
            dicUploadDoc.setValue(documentCode, forKey: "documentCode")
            dicUploadDoc.setValue(false, forKey: "isActive")
            dicUploadDoc.setValue(isDeletable, forKey: "isDeletable")
            dicUploadDoc.setValue(isDownloadable, forKey: "isDownloadable")
            
            
            self.arrNewUploadAllDocument.add(dicUploadDoc)
            
        }
        
        
        parameters.setValue(arrNewUploadAllDocument, forKey: "document")
        
        parameters.setValue(false, forKey: "isLoggedIn")
        parameters.setValue(false, forKey: "isRejected")
        
        print(parameters)
        
        APIClient().MakeAPICallWithAuthHeaderPost(USER_REGISTER, parameters: parameters as! [String : Any]) { (responce, error, statusCose) in
            
            if error == nil {
                print("Responce: \(String(describing: responce))")
                
                let code = responce?.value(forKey: "code") as! Int
                let strMessage = responce?.value(forKey: "message") as! String
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
                
                if statusCose == 200
                {
                    if code == 2000
                    {
                        self.view.makeToast(strMessage)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            
                            let contentView = Login()
                            self.navigationController?.pushViewController(UIHostingController(rootView: contentView), animated: true)
                        }
                    }
                    else
                    {
                        self.view.makeToast(strMessage)
                    }
                }
                else if statusCose == 413
                {
                    self.view.makeToast("Request Entity too large.")
                }
                else
                {
                    self.view.makeToast(strMessage)
                }
            }
            else
            {
                if statusCose == 413
                {
                    self.view.makeToast("Request Entity too large.")
                }
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        IQKeyboardManager.shared().isEnabled = true
    }
    
}

struct SignupView: View {
    var body: some View {
        SignUpViewController1()
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView()
    }
}

struct SignUpViewController1: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> some UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(identifier: "SignUpViewController")
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
}

private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            self.addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    // The method is used to cancel the check when using
    // the Chinese Pinyin input method.
    // Becuase the alphabet also appears in the textfield
    // when inputting, we should cancel the check.
    func isInputMethod() -> Bool {
        if let positionRange = self.markedTextRange {
            if let _ = self.position(from: positionRange.start, offset: 0) {
                return true
            }
        }
        return false
    }
    
    
    @objc func checkMaxLength(textField: UITextField) {
        
        guard !self.isInputMethod(), let prospectiveText = self.text,
              prospectiveText.count > maxLength
        else {
            return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    
}

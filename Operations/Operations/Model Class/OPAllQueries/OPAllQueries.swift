//
//	OPAllQueries.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPAllQueries : NSObject, NSCoding{

	var closeQueries : [OPAllQueriesCloseQuery]!
	var openQueries : [OPAllQueriesCloseQuery]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		closeQueries = [OPAllQueriesCloseQuery]()
		if let closeQueriesArray = dictionary["closeQueries"] as? [NSDictionary]{
			for dic in closeQueriesArray{
				let value = OPAllQueriesCloseQuery(fromDictionary: dic)
				closeQueries.append(value)
			}
		}
		openQueries = [OPAllQueriesCloseQuery]()
		if let openQueriesArray = dictionary["openQueries"] as? [NSDictionary]{
			for dic in openQueriesArray{
				let value = OPAllQueriesCloseQuery(fromDictionary: dic)
				openQueries.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if closeQueries != nil{
			var dictionaryElements = [NSDictionary]()
			for closeQueriesElement in closeQueries {
				dictionaryElements.append(closeQueriesElement.toDictionary())
			}
			dictionary["closeQueries"] = dictionaryElements
		}
		if openQueries != nil{
			var dictionaryElements = [NSDictionary]()
			for openQueriesElement in openQueries {
				dictionaryElements.append(openQueriesElement.toDictionary())
			}
			dictionary["openQueries"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         closeQueries = aDecoder.decodeObject(forKey: "closeQueries") as? [OPAllQueriesCloseQuery]
         openQueries = aDecoder.decodeObject(forKey: "openQueries") as? [OPAllQueriesCloseQuery]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if closeQueries != nil{
			aCoder.encode(closeQueries, forKey: "closeQueries")
		}
		if openQueries != nil{
			aCoder.encode(openQueries, forKey: "openQueries")
		}

	}

}
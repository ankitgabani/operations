//
//	OPAllQueriesCloseQuery.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPAllQueriesCloseQuery : NSObject, NSCoding{

	var queryDate : String!
	var queryDeadlineDate : String!
	var queryId : Int!
	var queryOpenDays : String!
	var querySendBy : String!
	var querySendTo : String!
	var queryStatus : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		queryDate = dictionary["queryDate"] as? String == nil ? "" : dictionary["queryDate"] as? String
		queryDeadlineDate = dictionary["queryDeadlineDate"] as? String == nil ? "" : dictionary["queryDeadlineDate"] as? String
		queryId = dictionary["queryId"] as? Int == nil ? 0 : dictionary["queryId"] as? Int
		queryOpenDays = dictionary["queryOpenDays"] as? String == nil ? "" : dictionary["queryOpenDays"] as? String
		querySendBy = dictionary["querySendBy"] as? String == nil ? "" : dictionary["querySendBy"] as? String
		querySendTo = dictionary["querySendTo"] as? String == nil ? "" : dictionary["querySendTo"] as? String
		queryStatus = dictionary["queryStatus"] as? String == nil ? "" : dictionary["queryStatus"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if queryDate != nil{
			dictionary["queryDate"] = queryDate
		}
		if queryDeadlineDate != nil{
			dictionary["queryDeadlineDate"] = queryDeadlineDate
		}
		if queryId != nil{
			dictionary["queryId"] = queryId
		}
		if queryOpenDays != nil{
			dictionary["queryOpenDays"] = queryOpenDays
		}
		if querySendBy != nil{
			dictionary["querySendBy"] = querySendBy
		}
		if querySendTo != nil{
			dictionary["querySendTo"] = querySendTo
		}
		if queryStatus != nil{
			dictionary["queryStatus"] = queryStatus
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         queryDate = aDecoder.decodeObject(forKey: "queryDate") as? String
         queryDeadlineDate = aDecoder.decodeObject(forKey: "queryDeadlineDate") as? String
         queryId = aDecoder.decodeObject(forKey: "queryId") as? Int
         queryOpenDays = aDecoder.decodeObject(forKey: "queryOpenDays") as? String
         querySendBy = aDecoder.decodeObject(forKey: "querySendBy") as? String
         querySendTo = aDecoder.decodeObject(forKey: "querySendTo") as? String
         queryStatus = aDecoder.decodeObject(forKey: "queryStatus") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if queryDate != nil{
			aCoder.encode(queryDate, forKey: "queryDate")
		}
		if queryDeadlineDate != nil{
			aCoder.encode(queryDeadlineDate, forKey: "queryDeadlineDate")
		}
		if queryId != nil{
			aCoder.encode(queryId, forKey: "queryId")
		}
		if queryOpenDays != nil{
			aCoder.encode(queryOpenDays, forKey: "queryOpenDays")
		}
		if querySendBy != nil{
			aCoder.encode(querySendBy, forKey: "querySendBy")
		}
		if querySendTo != nil{
			aCoder.encode(querySendTo, forKey: "querySendTo")
		}
		if queryStatus != nil{
			aCoder.encode(queryStatus, forKey: "queryStatus")
		}

	}

}
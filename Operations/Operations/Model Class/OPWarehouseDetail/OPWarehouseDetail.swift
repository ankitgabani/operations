//
//	OPWarehouseDetail.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPWarehouseDetail : NSObject, NSCoding{

	var email : String!
	var employeeId : String!
	var firstName : String!
	var fullName : String!
	var isAssignedToWarehouse : Bool!
	var lastName : String!
	var mobileNumber : String!
	var nameInitials : String!
	var userId : Int!
	var wareHouse : String!
	var wareHouseId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		employeeId = dictionary["employeeId"] as? String == nil ? "" : dictionary["employeeId"] as? String
		firstName = dictionary["firstName"] as? String == nil ? "" : dictionary["firstName"] as? String
		fullName = dictionary["fullName"] as? String == nil ? "" : dictionary["fullName"] as? String
		isAssignedToWarehouse = dictionary["isAssignedToWarehouse"] as? Bool == nil ? false : dictionary["isAssignedToWarehouse"] as? Bool
		lastName = dictionary["lastName"] as? String == nil ? "" : dictionary["lastName"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		nameInitials = dictionary["nameInitials"] as? String == nil ? "" : dictionary["nameInitials"] as? String
		userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
		wareHouse = dictionary["wareHouse"] as? String == nil ? "" : dictionary["wareHouse"] as? String
		wareHouseId = dictionary["wareHouseId"] as? Int == nil ? 0 : dictionary["wareHouseId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if email != nil{
			dictionary["email"] = email
		}
		if employeeId != nil{
			dictionary["employeeId"] = employeeId
		}
		if firstName != nil{
			dictionary["firstName"] = firstName
		}
		if fullName != nil{
			dictionary["fullName"] = fullName
		}
		if isAssignedToWarehouse != nil{
			dictionary["isAssignedToWarehouse"] = isAssignedToWarehouse
		}
		if lastName != nil{
			dictionary["lastName"] = lastName
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if nameInitials != nil{
			dictionary["nameInitials"] = nameInitials
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		if wareHouse != nil{
			dictionary["wareHouse"] = wareHouse
		}
		if wareHouseId != nil{
			dictionary["wareHouseId"] = wareHouseId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObject(forKey: "email") as? String
         employeeId = aDecoder.decodeObject(forKey: "employeeId") as? String
         firstName = aDecoder.decodeObject(forKey: "firstName") as? String
         fullName = aDecoder.decodeObject(forKey: "fullName") as? String
         isAssignedToWarehouse = aDecoder.decodeObject(forKey: "isAssignedToWarehouse") as? Bool
         lastName = aDecoder.decodeObject(forKey: "lastName") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         nameInitials = aDecoder.decodeObject(forKey: "nameInitials") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? Int
         wareHouse = aDecoder.decodeObject(forKey: "wareHouse") as? String
         wareHouseId = aDecoder.decodeObject(forKey: "wareHouseId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if employeeId != nil{
			aCoder.encode(employeeId, forKey: "employeeId")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "firstName")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "fullName")
		}
		if isAssignedToWarehouse != nil{
			aCoder.encode(isAssignedToWarehouse, forKey: "isAssignedToWarehouse")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "lastName")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if nameInitials != nil{
			aCoder.encode(nameInitials, forKey: "nameInitials")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}
		if wareHouse != nil{
			aCoder.encode(wareHouse, forKey: "wareHouse")
		}
		if wareHouseId != nil{
			aCoder.encode(wareHouseId, forKey: "wareHouseId")
		}

	}

}
//
//	OPWarehouseDetail.java
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import org.json.*;
import java.util.*;
import com.google.gson.annotations.SerializedName;


public class OPWarehouseDetail{

	@SerializedName("email")
	private String email;
	@SerializedName("employeeId")
	private String employeeId;
	@SerializedName("firstName")
	private String firstName;
	@SerializedName("fullName")
	private String fullName;
	@SerializedName("isAssignedToWarehouse")
	private boolean isAssignedToWarehouse;
	@SerializedName("lastName")
	private String lastName;
	@SerializedName("mobileNumber")
	private String mobileNumber;
	@SerializedName("nameInitials")
	private String nameInitials;
	@SerializedName("userId")
	private int userId;
	@SerializedName("wareHouse")
	private String wareHouse;
	@SerializedName("wareHouseId")
	private int wareHouseId;

	public void setEmail(String email){
		this.email = email;
	}
	public String getEmail(){
		return this.email;
	}
	public void setEmployeeId(String employeeId){
		this.employeeId = employeeId;
	}
	public String getEmployeeId(){
		return this.employeeId;
	}
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public String getFirstName(){
		return this.firstName;
	}
	public void setFullName(String fullName){
		this.fullName = fullName;
	}
	public String getFullName(){
		return this.fullName;
	}
	public void setIsAssignedToWarehouse(boolean isAssignedToWarehouse){
		this.isAssignedToWarehouse = isAssignedToWarehouse;
	}
	public boolean isIsAssignedToWarehouse()
	{
		return this.isAssignedToWarehouse;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	public String getLastName(){
		return this.lastName;
	}
	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}
	public String getMobileNumber(){
		return this.mobileNumber;
	}
	public void setNameInitials(String nameInitials){
		this.nameInitials = nameInitials;
	}
	public String getNameInitials(){
		return this.nameInitials;
	}
	public void setUserId(int userId){
		this.userId = userId;
	}
	public int getUserId(){
		return this.userId;
	}
	public void setWareHouse(String wareHouse){
		this.wareHouse = wareHouse;
	}
	public String getWareHouse(){
		return this.wareHouse;
	}
	public void setWareHouseId(int wareHouseId){
		this.wareHouseId = wareHouseId;
	}
	public int getWareHouseId(){
		return this.wareHouseId;
	}

	/**
	 * Instantiate the instance using the passed jsonObject to set the properties values
	 */
	public OPWarehouseDetail(JSONObject jsonObject){
		if(jsonObject == null){
			return;
		}
		email = jsonObject.opt("email");
		employeeId = jsonObject.opt("employeeId");
		firstName = jsonObject.opt("firstName");
		fullName = jsonObject.opt("fullName");
		isAssignedToWarehouse = jsonObject.optBoolean("isAssignedToWarehouse");
		lastName = jsonObject.opt("lastName");
		mobileNumber = jsonObject.opt("mobileNumber");
		nameInitials = jsonObject.opt("nameInitials");
		userId = jsonObject.optInt("userId");
		wareHouse = jsonObject.opt("wareHouse");
		wareHouseId = jsonObject.optInt("wareHouseId");
	}

	/**
	 * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
	 */
	public JSONObject toJsonObject()
	{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("email", email);
			jsonObject.put("employeeId", employeeId);
			jsonObject.put("firstName", firstName);
			jsonObject.put("fullName", fullName);
			jsonObject.put("isAssignedToWarehouse", isAssignedToWarehouse);
			jsonObject.put("lastName", lastName);
			jsonObject.put("mobileNumber", mobileNumber);
			jsonObject.put("nameInitials", nameInitials);
			jsonObject.put("userId", userId);
			jsonObject.put("wareHouse", wareHouse);
			jsonObject.put("wareHouseId", wareHouseId);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

}
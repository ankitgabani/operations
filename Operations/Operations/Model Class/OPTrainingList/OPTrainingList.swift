//
//	OPTrainingList.swift
//
//	Create by Ankit Gabani on 2/4/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPTrainingList : NSObject, NSCoding{

	var descriptionField : String!
	var documentPath : String!
	var isActive : Bool!
	var masterId : Int!
	var version : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		descriptionField = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
		documentPath = dictionary["documentPath"] as? String == nil ? "" : dictionary["documentPath"] as? String
		isActive = dictionary["isActive"] as? Bool == nil ? false : dictionary["isActive"] as? Bool
		masterId = dictionary["masterId"] as? Int == nil ? 0 : dictionary["masterId"] as? Int
		version = dictionary["version"] as? Int == nil ? 0 : dictionary["version"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if documentPath != nil{
			dictionary["documentPath"] = documentPath
		}
		if isActive != nil{
			dictionary["isActive"] = isActive
		}
		if masterId != nil{
			dictionary["masterId"] = masterId
		}
		if version != nil{
			dictionary["version"] = version
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         documentPath = aDecoder.decodeObject(forKey: "documentPath") as? String
         isActive = aDecoder.decodeObject(forKey: "isActive") as? Bool
         masterId = aDecoder.decodeObject(forKey: "masterId") as? Int
         version = aDecoder.decodeObject(forKey: "version") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if documentPath != nil{
			aCoder.encode(documentPath, forKey: "documentPath")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "isActive")
		}
		if masterId != nil{
			aCoder.encode(masterId, forKey: "masterId")
		}
		if version != nil{
			aCoder.encode(version, forKey: "version")
		}

	}

}
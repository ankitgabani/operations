//
//	OPGroupPolicy.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPGroupPolicy : NSObject, NSCoding{

	var groupId : Int!
	var permissions : [OPGroupPolicyPermission]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		groupId = dictionary["groupId"] as? Int == nil ? 0 : dictionary["groupId"] as? Int
		permissions = [OPGroupPolicyPermission]()
		if let permissionsArray = dictionary["permissions"] as? [NSDictionary]{
			for dic in permissionsArray{
				let value = OPGroupPolicyPermission(fromDictionary: dic)
				permissions.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if groupId != nil{
			dictionary["groupId"] = groupId
		}
		if permissions != nil{
			var dictionaryElements = [NSDictionary]()
			for permissionsElement in permissions {
				dictionaryElements.append(permissionsElement.toDictionary())
			}
			dictionary["permissions"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         groupId = aDecoder.decodeObject(forKey: "groupId") as? Int
         permissions = aDecoder.decodeObject(forKey: "permissions") as? [OPGroupPolicyPermission]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if groupId != nil{
			aCoder.encode(groupId, forKey: "groupId")
		}
		if permissions != nil{
			aCoder.encode(permissions, forKey: "permissions")
		}

	}

}
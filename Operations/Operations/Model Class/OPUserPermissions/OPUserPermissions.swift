//
//	OPUserPermissions.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserPermissions : NSObject, NSCoding{

	var permissions : [OPUserPermissionsPermission]!
	var userId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		permissions = [OPUserPermissionsPermission]()
		if let permissionsArray = dictionary["permissions"] as? [NSDictionary]{
			for dic in permissionsArray{
				let value = OPUserPermissionsPermission(fromDictionary: dic)
				permissions.append(value)
			}
		}
		userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if permissions != nil{
			var dictionaryElements = [NSDictionary]()
			for permissionsElement in permissions {
				dictionaryElements.append(permissionsElement.toDictionary())
			}
			dictionary["permissions"] = dictionaryElements
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         permissions = aDecoder.decodeObject(forKey: "permissions") as? [OPUserPermissionsPermission]
         userId = aDecoder.decodeObject(forKey: "userId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if permissions != nil{
			aCoder.encode(permissions, forKey: "permissions")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}

	}

}
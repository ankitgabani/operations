//
//	OPUserRole.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserRole : NSObject, NSCoding{

	var groups : [OPUserRoleGroup]!
	var userId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		groups = [OPUserRoleGroup]()
		if let groupsArray = dictionary["groups"] as? [NSDictionary]{
			for dic in groupsArray{
				let value = OPUserRoleGroup(fromDictionary: dic)
				groups.append(value)
			}
		}
		userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if groups != nil{
			var dictionaryElements = [NSDictionary]()
			for groupsElement in groups {
				dictionaryElements.append(groupsElement.toDictionary())
			}
			dictionary["groups"] = dictionaryElements
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         groups = aDecoder.decodeObject(forKey: "groups") as? [OPUserRoleGroup]
         userId = aDecoder.decodeObject(forKey: "userId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if groups != nil{
			aCoder.encode(groups, forKey: "groups")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}

	}

}

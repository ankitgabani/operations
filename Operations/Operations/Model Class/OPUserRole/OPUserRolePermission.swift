//
//	OPUserRolePermission.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserRolePermission : NSObject, NSCoding{

	var permissionCode : String!
	var permissionId : Int!
	var permissionName : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		permissionCode = dictionary["permissionCode"] as? String == nil ? "" : dictionary["permissionCode"] as? String
		permissionId = dictionary["permissionId"] as? Int == nil ? 0 : dictionary["permissionId"] as? Int
		permissionName = dictionary["permissionName"] as? String == nil ? "" : dictionary["permissionName"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if permissionCode != nil{
			dictionary["permissionCode"] = permissionCode
		}
		if permissionId != nil{
			dictionary["permissionId"] = permissionId
		}
		if permissionName != nil{
			dictionary["permissionName"] = permissionName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         permissionCode = aDecoder.decodeObject(forKey: "permissionCode") as? String
         permissionId = aDecoder.decodeObject(forKey: "permissionId") as? Int
         permissionName = aDecoder.decodeObject(forKey: "permissionName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if permissionCode != nil{
			aCoder.encode(permissionCode, forKey: "permissionCode")
		}
		if permissionId != nil{
			aCoder.encode(permissionId, forKey: "permissionId")
		}
		if permissionName != nil{
			aCoder.encode(permissionName, forKey: "permissionName")
		}

	}

}
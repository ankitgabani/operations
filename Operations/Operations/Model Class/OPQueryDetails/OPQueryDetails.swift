//
//	OPQueryDetails.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPQueryDetails : NSObject, NSCoding{

	var closedBy : String!
	var closedOn : String!
	var closedRemark : String!
	var queryDate : String!
	var queryDeadlineDateText : String!
	var queryId : Int!
	var querySendBy : String!
	var querySendTo : String!
	var queryStatus : String!
	var queryText : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		closedBy = dictionary["closedBy"] as? String == nil ? "" : dictionary["closedBy"] as? String
		closedOn = dictionary["closedOn"] as? String == nil ? "" : dictionary["closedOn"] as? String
		closedRemark = dictionary["closedRemark"] as? String == nil ? "" : dictionary["closedRemark"] as? String
		queryDate = dictionary["queryDate"] as? String == nil ? "" : dictionary["queryDate"] as? String
		queryDeadlineDateText = dictionary["queryDeadlineDateText"] as? String == nil ? "" : dictionary["queryDeadlineDateText"] as? String
		queryId = dictionary["queryId"] as? Int == nil ? 0 : dictionary["queryId"] as? Int
		querySendBy = dictionary["querySendBy"] as? String == nil ? "" : dictionary["querySendBy"] as? String
		querySendTo = dictionary["querySendTo"] as? String == nil ? "" : dictionary["querySendTo"] as? String
		queryStatus = dictionary["queryStatus"] as? String == nil ? "" : dictionary["queryStatus"] as? String
		queryText = dictionary["queryText"] as? String == nil ? "" : dictionary["queryText"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if closedBy != nil{
			dictionary["closedBy"] = closedBy
		}
		if closedOn != nil{
			dictionary["closedOn"] = closedOn
		}
		if closedRemark != nil{
			dictionary["closedRemark"] = closedRemark
		}
		if queryDate != nil{
			dictionary["queryDate"] = queryDate
		}
		if queryDeadlineDateText != nil{
			dictionary["queryDeadlineDateText"] = queryDeadlineDateText
		}
		if queryId != nil{
			dictionary["queryId"] = queryId
		}
		if querySendBy != nil{
			dictionary["querySendBy"] = querySendBy
		}
		if querySendTo != nil{
			dictionary["querySendTo"] = querySendTo
		}
		if queryStatus != nil{
			dictionary["queryStatus"] = queryStatus
		}
		if queryText != nil{
			dictionary["queryText"] = queryText
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         closedBy = aDecoder.decodeObject(forKey: "closedBy") as? String
         closedOn = aDecoder.decodeObject(forKey: "closedOn") as? String
         closedRemark = aDecoder.decodeObject(forKey: "closedRemark") as? String
         queryDate = aDecoder.decodeObject(forKey: "queryDate") as? String
         queryDeadlineDateText = aDecoder.decodeObject(forKey: "queryDeadlineDateText") as? String
         queryId = aDecoder.decodeObject(forKey: "queryId") as? Int
         querySendBy = aDecoder.decodeObject(forKey: "querySendBy") as? String
         querySendTo = aDecoder.decodeObject(forKey: "querySendTo") as? String
         queryStatus = aDecoder.decodeObject(forKey: "queryStatus") as? String
         queryText = aDecoder.decodeObject(forKey: "queryText") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if closedBy != nil{
			aCoder.encode(closedBy, forKey: "closedBy")
		}
		if closedOn != nil{
			aCoder.encode(closedOn, forKey: "closedOn")
		}
		if closedRemark != nil{
			aCoder.encode(closedRemark, forKey: "closedRemark")
		}
		if queryDate != nil{
			aCoder.encode(queryDate, forKey: "queryDate")
		}
		if queryDeadlineDateText != nil{
			aCoder.encode(queryDeadlineDateText, forKey: "queryDeadlineDateText")
		}
		if queryId != nil{
			aCoder.encode(queryId, forKey: "queryId")
		}
		if querySendBy != nil{
			aCoder.encode(querySendBy, forKey: "querySendBy")
		}
		if querySendTo != nil{
			aCoder.encode(querySendTo, forKey: "querySendTo")
		}
		if queryStatus != nil{
			aCoder.encode(queryStatus, forKey: "queryStatus")
		}
		if queryText != nil{
			aCoder.encode(queryText, forKey: "queryText")
		}

	}

}
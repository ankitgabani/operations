//
//	OPRoles.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPRoles : NSObject, NSCoding{

	var groupId : Int!
	var groupName : String!
	var isDefault : Bool!
	var permissions : [String]!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		groupId = dictionary["groupId"] as? Int == nil ? 0 : dictionary["groupId"] as? Int
		groupName = dictionary["groupName"] as? String == nil ? "" : dictionary["groupName"] as? String
		isDefault = dictionary["isDefault"] as? Bool == nil ? false : dictionary["isDefault"] as? Bool
		permissions = dictionary["permissions"] as? [String] == nil ? [] : dictionary["permissions"] as? [String]
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if groupId != nil{
			dictionary["groupId"] = groupId
		}
		if groupName != nil{
			dictionary["groupName"] = groupName
		}
		if isDefault != nil{
			dictionary["isDefault"] = isDefault
		}
		if permissions != nil{
			dictionary["permissions"] = permissions
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         groupId = aDecoder.decodeObject(forKey: "groupId") as? Int
         groupName = aDecoder.decodeObject(forKey: "groupName") as? String
         isDefault = aDecoder.decodeObject(forKey: "isDefault") as? Bool
         permissions = aDecoder.decodeObject(forKey: "permissions") as? [String]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if groupId != nil{
			aCoder.encode(groupId, forKey: "groupId")
		}
		if groupName != nil{
			aCoder.encode(groupName, forKey: "groupName")
		}
		if isDefault != nil{
			aCoder.encode(isDefault, forKey: "isDefault")
		}
		if permissions != nil{
			aCoder.encode(permissions, forKey: "permissions")
		}

	}

}

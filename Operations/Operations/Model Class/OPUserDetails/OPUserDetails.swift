//
//	OPUserDetails.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserDetails : NSObject, NSCoding{

	var aadharNumber : String!
	var address : OPUserDetailsAddres!
	var deviceId : String!
	var deviceName : String!
	var document : [OPUserDetailsDocument]!
	var email : String!
	var employeeId : String!
	var firstName : String!
	var fullName : String!
	var isActive : Bool!
	var isLoggedIn : Bool!
	var isPasswordResetReqApproved : Bool!
	var isRejected : Bool!
	var isSuperAdmin : Bool!
	var lastName : String!
	var mobileNumber : String!
	var qualification : [OPUserDetailsQualification]!
	var relationDetail : [OPUserDetailsRelationDetail]!
	var userId : Int!
	var wareHouse : String!
	var wareHouseId : Int!
    var profileImageByteArray : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		aadharNumber = dictionary["aadharNumber"] as? String == nil ? "" : dictionary["aadharNumber"] as? String
        profileImageByteArray = dictionary["profileImageByteArray"] as? String == nil ? "" : dictionary["profileImageByteArray"] as? String

		if let addressData = dictionary["address"] as? NSDictionary{
			address = OPUserDetailsAddres(fromDictionary: addressData)
		}
		else
		{
			address = OPUserDetailsAddres(fromDictionary: NSDictionary.init())
		}
		deviceId = dictionary["deviceId"] as? String == nil ? "" : dictionary["deviceId"] as? String
		deviceName = dictionary["deviceName"] as? String == nil ? "" : dictionary["deviceName"] as? String
		document = [OPUserDetailsDocument]()
		if let documentArray = dictionary["document"] as? [NSDictionary]{
			for dic in documentArray{
				let value = OPUserDetailsDocument(fromDictionary: dic)
				document.append(value)
			}
		}
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		employeeId = dictionary["employeeId"] as? String == nil ? "" : dictionary["employeeId"] as? String
		firstName = dictionary["firstName"] as? String == nil ? "" : dictionary["firstName"] as? String
		fullName = dictionary["fullName"] as? String == nil ? "" : dictionary["fullName"] as? String
		isActive = dictionary["isActive"] as? Bool == nil ? false : dictionary["isActive"] as? Bool
		isLoggedIn = dictionary["isLoggedIn"] as? Bool == nil ? false : dictionary["isLoggedIn"] as? Bool
		isPasswordResetReqApproved = dictionary["isPasswordResetReqApproved"] as? Bool == nil ? false : dictionary["isPasswordResetReqApproved"] as? Bool
		isRejected = dictionary["isRejected"] as? Bool == nil ? false : dictionary["isRejected"] as? Bool
		isSuperAdmin = dictionary["isSuperAdmin"] as? Bool == nil ? false : dictionary["isSuperAdmin"] as? Bool
		lastName = dictionary["lastName"] as? String == nil ? "" : dictionary["lastName"] as? String
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		qualification = [OPUserDetailsQualification]()
		if let qualificationArray = dictionary["qualification"] as? [NSDictionary]{
			for dic in qualificationArray{
				let value = OPUserDetailsQualification(fromDictionary: dic)
				qualification.append(value)
			}
		}
		relationDetail = [OPUserDetailsRelationDetail]()
		if let relationDetailArray = dictionary["relationDetail"] as? [NSDictionary]{
			for dic in relationDetailArray{
				let value = OPUserDetailsRelationDetail(fromDictionary: dic)
				relationDetail.append(value)
			}
		}
		userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
		wareHouse = dictionary["wareHouse"] as? String == nil ? "" : dictionary["wareHouse"] as? String
		wareHouseId = dictionary["wareHouseId"] as? Int == nil ? 0 : dictionary["wareHouseId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aadharNumber != nil{
			dictionary["aadharNumber"] = aadharNumber
		}
        if profileImageByteArray != nil{
            dictionary["profileImageByteArray"] = profileImageByteArray
        }
		if address != nil{
			dictionary["address"] = address.toDictionary()
		}
		if deviceId != nil{
			dictionary["deviceId"] = deviceId
		}
		if deviceName != nil{
			dictionary["deviceName"] = deviceName
		}
		if document != nil{
			var dictionaryElements = [NSDictionary]()
			for documentElement in document {
				dictionaryElements.append(documentElement.toDictionary())
			}
			dictionary["document"] = dictionaryElements
		}
		if email != nil{
			dictionary["email"] = email
		}
		if employeeId != nil{
			dictionary["employeeId"] = employeeId
		}
		if firstName != nil{
			dictionary["firstName"] = firstName
		}
		if fullName != nil{
			dictionary["fullName"] = fullName
		}
		if isActive != nil{
			dictionary["isActive"] = isActive
		}
		if isLoggedIn != nil{
			dictionary["isLoggedIn"] = isLoggedIn
		}
		if isPasswordResetReqApproved != nil{
			dictionary["isPasswordResetReqApproved"] = isPasswordResetReqApproved
		}
		if isRejected != nil{
			dictionary["isRejected"] = isRejected
		}
		if isSuperAdmin != nil{
			dictionary["isSuperAdmin"] = isSuperAdmin
		}
		if lastName != nil{
			dictionary["lastName"] = lastName
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if qualification != nil{
			var dictionaryElements = [NSDictionary]()
			for qualificationElement in qualification {
				dictionaryElements.append(qualificationElement.toDictionary())
			}
			dictionary["qualification"] = dictionaryElements
		}
		if relationDetail != nil{
			var dictionaryElements = [NSDictionary]()
			for relationDetailElement in relationDetail {
				dictionaryElements.append(relationDetailElement.toDictionary())
			}
			dictionary["relationDetail"] = dictionaryElements
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		if wareHouse != nil{
			dictionary["wareHouse"] = wareHouse
		}
		if wareHouseId != nil{
			dictionary["wareHouseId"] = wareHouseId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        profileImageByteArray = aDecoder.decodeObject(forKey: "profileImageByteArray") as? String

         aadharNumber = aDecoder.decodeObject(forKey: "aadharNumber") as? String
         address = aDecoder.decodeObject(forKey: "address") as? OPUserDetailsAddres
         deviceId = aDecoder.decodeObject(forKey: "deviceId") as? String
         deviceName = aDecoder.decodeObject(forKey: "deviceName") as? String
         document = aDecoder.decodeObject(forKey: "document") as? [OPUserDetailsDocument]
         email = aDecoder.decodeObject(forKey: "email") as? String
         employeeId = aDecoder.decodeObject(forKey: "employeeId") as? String
         firstName = aDecoder.decodeObject(forKey: "firstName") as? String
         fullName = aDecoder.decodeObject(forKey: "fullName") as? String
         isActive = aDecoder.decodeObject(forKey: "isActive") as? Bool
         isLoggedIn = aDecoder.decodeObject(forKey: "isLoggedIn") as? Bool
         isPasswordResetReqApproved = aDecoder.decodeObject(forKey: "isPasswordResetReqApproved") as? Bool
         isRejected = aDecoder.decodeObject(forKey: "isRejected") as? Bool
         isSuperAdmin = aDecoder.decodeObject(forKey: "isSuperAdmin") as? Bool
         lastName = aDecoder.decodeObject(forKey: "lastName") as? String
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         qualification = aDecoder.decodeObject(forKey: "qualification") as? [OPUserDetailsQualification]
         relationDetail = aDecoder.decodeObject(forKey: "relationDetail") as? [OPUserDetailsRelationDetail]
         userId = aDecoder.decodeObject(forKey: "userId") as? Int
         wareHouse = aDecoder.decodeObject(forKey: "wareHouse") as? String
         wareHouseId = aDecoder.decodeObject(forKey: "wareHouseId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if aadharNumber != nil{
			aCoder.encode(aadharNumber, forKey: "aadharNumber")
		}
        if profileImageByteArray != nil{
            aCoder.encode(profileImageByteArray, forKey: "profileImageByteArray")
        }
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if deviceId != nil{
			aCoder.encode(deviceId, forKey: "deviceId")
		}
		if deviceName != nil{
			aCoder.encode(deviceName, forKey: "deviceName")
		}
		if document != nil{
			aCoder.encode(document, forKey: "document")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if employeeId != nil{
			aCoder.encode(employeeId, forKey: "employeeId")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "firstName")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "fullName")
		}
		if isActive != nil{
			aCoder.encode(isActive, forKey: "isActive")
		}
		if isLoggedIn != nil{
			aCoder.encode(isLoggedIn, forKey: "isLoggedIn")
		}
		if isPasswordResetReqApproved != nil{
			aCoder.encode(isPasswordResetReqApproved, forKey: "isPasswordResetReqApproved")
		}
		if isRejected != nil{
			aCoder.encode(isRejected, forKey: "isRejected")
		}
		if isSuperAdmin != nil{
			aCoder.encode(isSuperAdmin, forKey: "isSuperAdmin")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "lastName")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if qualification != nil{
			aCoder.encode(qualification, forKey: "qualification")
		}
		if relationDetail != nil{
			aCoder.encode(relationDetail, forKey: "relationDetail")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}
		if wareHouse != nil{
			aCoder.encode(wareHouse, forKey: "wareHouse")
		}
		if wareHouseId != nil{
			aCoder.encode(wareHouseId, forKey: "wareHouseId")
		}

	}

}

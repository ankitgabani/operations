//
//	OPUserDetailsAddres.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserDetailsAddres : NSObject, NSCoding{

	var addressId : Int!
	var homeTown : String!
	var permanentAddress : String!
	var pinCode : String!
	var residentialAddress : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		addressId = dictionary["addressId"] as? Int == nil ? 0 : dictionary["addressId"] as? Int
		homeTown = dictionary["homeTown"] as? String == nil ? "" : dictionary["homeTown"] as? String
		permanentAddress = dictionary["permanentAddress"] as? String == nil ? "" : dictionary["permanentAddress"] as? String
		pinCode = dictionary["pinCode"] as? String == nil ? "" : dictionary["pinCode"] as? String
		residentialAddress = dictionary["residentialAddress"] as? String == nil ? "" : dictionary["residentialAddress"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if addressId != nil{
			dictionary["addressId"] = addressId
		}
		if homeTown != nil{
			dictionary["homeTown"] = homeTown
		}
		if permanentAddress != nil{
			dictionary["permanentAddress"] = permanentAddress
		}
		if pinCode != nil{
			dictionary["pinCode"] = pinCode
		}
		if residentialAddress != nil{
			dictionary["residentialAddress"] = residentialAddress
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressId = aDecoder.decodeObject(forKey: "addressId") as? Int
         homeTown = aDecoder.decodeObject(forKey: "homeTown") as? String
         permanentAddress = aDecoder.decodeObject(forKey: "permanentAddress") as? String
         pinCode = aDecoder.decodeObject(forKey: "pinCode") as? String
         residentialAddress = aDecoder.decodeObject(forKey: "residentialAddress") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if addressId != nil{
			aCoder.encode(addressId, forKey: "addressId")
		}
		if homeTown != nil{
			aCoder.encode(homeTown, forKey: "homeTown")
		}
		if permanentAddress != nil{
			aCoder.encode(permanentAddress, forKey: "permanentAddress")
		}
		if pinCode != nil{
			aCoder.encode(pinCode, forKey: "pinCode")
		}
		if residentialAddress != nil{
			aCoder.encode(residentialAddress, forKey: "residentialAddress")
		}

	}

}
//
//	OPUserDetailsQualification.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserDetailsQualification : NSObject, NSCoding{

	var percentage : Float!
	var qualificationId : Int!
	var qualificationName : String!
	var userQualificationId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		percentage = dictionary["percentage"] as? Float == nil ? 0 : dictionary["percentage"] as? Float
		qualificationId = dictionary["qualificationId"] as? Int == nil ? 0 : dictionary["qualificationId"] as? Int
		qualificationName = dictionary["qualificationName"] as? String == nil ? "" : dictionary["qualificationName"] as? String
		userQualificationId = dictionary["userQualificationId"] as? Int == nil ? 0 : dictionary["userQualificationId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if percentage != nil{
			dictionary["percentage"] = percentage
		}
		if qualificationId != nil{
			dictionary["qualificationId"] = qualificationId
		}
		if qualificationName != nil{
			dictionary["qualificationName"] = qualificationName
		}
		if userQualificationId != nil{
			dictionary["userQualificationId"] = userQualificationId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         percentage = aDecoder.decodeObject(forKey: "percentage") as? Float
         qualificationId = aDecoder.decodeObject(forKey: "qualificationId") as? Int
         qualificationName = aDecoder.decodeObject(forKey: "qualificationName") as? String
         userQualificationId = aDecoder.decodeObject(forKey: "userQualificationId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if percentage != nil{
			aCoder.encode(percentage, forKey: "percentage")
		}
		if qualificationId != nil{
			aCoder.encode(qualificationId, forKey: "qualificationId")
		}
		if qualificationName != nil{
			aCoder.encode(qualificationName, forKey: "qualificationName")
		}
		if userQualificationId != nil{
			aCoder.encode(userQualificationId, forKey: "userQualificationId")
		}

	}

}
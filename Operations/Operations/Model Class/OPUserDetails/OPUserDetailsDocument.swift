//
//	OPUserDetailsDocument.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserDetailsDocument : NSObject, NSCoding{

	var documentCode : String!
	var documentName : String!
	var documentPath : String!
	var userDocumentId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		documentCode = dictionary["documentCode"] as? String == nil ? "" : dictionary["documentCode"] as? String
		documentName = dictionary["documentName"] as? String == nil ? "" : dictionary["documentName"] as? String
		documentPath = dictionary["documentPath"] as? String == nil ? "" : dictionary["documentPath"] as? String
		userDocumentId = dictionary["userDocumentId"] as? Int == nil ? 0 : dictionary["userDocumentId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if documentCode != nil{
			dictionary["documentCode"] = documentCode
		}
		if documentName != nil{
			dictionary["documentName"] = documentName
		}
		if documentPath != nil{
			dictionary["documentPath"] = documentPath
		}
		if userDocumentId != nil{
			dictionary["userDocumentId"] = userDocumentId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         documentCode = aDecoder.decodeObject(forKey: "documentCode") as? String
         documentName = aDecoder.decodeObject(forKey: "documentName") as? String
         documentPath = aDecoder.decodeObject(forKey: "documentPath") as? String
         userDocumentId = aDecoder.decodeObject(forKey: "userDocumentId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if documentCode != nil{
			aCoder.encode(documentCode, forKey: "documentCode")
		}
		if documentName != nil{
			aCoder.encode(documentName, forKey: "documentName")
		}
		if documentPath != nil{
			aCoder.encode(documentPath, forKey: "documentPath")
		}
		if userDocumentId != nil{
			aCoder.encode(userDocumentId, forKey: "userDocumentId")
		}

	}

}
//
//	OPUserDetailsRelationDetail.swift
//
//	Create by iMac on 25/3/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPUserDetailsRelationDetail : NSObject, NSCoding{

	var profession : String!
	var professionId : Int!
	var relationAge : Int!
	var relationDescription : String!
	var relationId : Int!
	var relativeName : String!
	var userRelationId : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		profession = dictionary["profession"] as? String == nil ? "" : dictionary["profession"] as? String
		professionId = dictionary["professionId"] as? Int == nil ? 0 : dictionary["professionId"] as? Int
		relationAge = dictionary["relationAge"] as? Int == nil ? 0 : dictionary["relationAge"] as? Int
		relationDescription = dictionary["relationDescription"] as? String == nil ? "" : dictionary["relationDescription"] as? String
		relationId = dictionary["relationId"] as? Int == nil ? 0 : dictionary["relationId"] as? Int
		relativeName = dictionary["relativeName"] as? String == nil ? "" : dictionary["relativeName"] as? String
		userRelationId = dictionary["userRelationId"] as? Int == nil ? 0 : dictionary["userRelationId"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if profession != nil{
			dictionary["profession"] = profession
		}
		if professionId != nil{
			dictionary["professionId"] = professionId
		}
		if relationAge != nil{
			dictionary["relationAge"] = relationAge
		}
		if relationDescription != nil{
			dictionary["relationDescription"] = relationDescription
		}
		if relationId != nil{
			dictionary["relationId"] = relationId
		}
		if relativeName != nil{
			dictionary["relativeName"] = relativeName
		}
		if userRelationId != nil{
			dictionary["userRelationId"] = userRelationId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         profession = aDecoder.decodeObject(forKey: "profession") as? String
         professionId = aDecoder.decodeObject(forKey: "professionId") as? Int
         relationAge = aDecoder.decodeObject(forKey: "relationAge") as? Int
         relationDescription = aDecoder.decodeObject(forKey: "relationDescription") as? String
         relationId = aDecoder.decodeObject(forKey: "relationId") as? Int
         relativeName = aDecoder.decodeObject(forKey: "relativeName") as? String
         userRelationId = aDecoder.decodeObject(forKey: "userRelationId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if profession != nil{
			aCoder.encode(profession, forKey: "profession")
		}
		if professionId != nil{
			aCoder.encode(professionId, forKey: "professionId")
		}
		if relationAge != nil{
			aCoder.encode(relationAge, forKey: "relationAge")
		}
		if relationDescription != nil{
			aCoder.encode(relationDescription, forKey: "relationDescription")
		}
		if relationId != nil{
			aCoder.encode(relationId, forKey: "relationId")
		}
		if relativeName != nil{
			aCoder.encode(relativeName, forKey: "relativeName")
		}
		if userRelationId != nil{
			aCoder.encode(userRelationId, forKey: "userRelationId")
		}

	}

}
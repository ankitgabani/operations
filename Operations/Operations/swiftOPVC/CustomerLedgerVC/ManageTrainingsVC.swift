//
//  ManageTrainingsVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit

class ManageTrainingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblView: UITableView!
  
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewUpdate: UIView!
    @IBOutlet weak var viewDelete: UIView!
    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        viewBackground.isHidden = true
        viewUpdate.isHidden = true
        viewDelete.isHidden = true
        
        viewUpdate.clipsToBounds = true
        viewUpdate.layer.cornerRadius = 15
        viewUpdate.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
     
    }
    
    // MARK: - btnAction
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedConfirm(_ sender: Any) {
        
        viewBackground.isHidden = true
        viewUpdate.isHidden = true
        viewDelete.isHidden = true
        
    }
    @IBAction func clickedCacel(_ sender: Any) {
        
        viewBackground.isHidden = true
        viewUpdate.isHidden = true
        viewDelete.isHidden = true
    }
    
    @IBAction func clickedYes(_ sender: Any) {
        
        viewBackground.isHidden = true
        viewUpdate.isHidden = true
        viewDelete.isHidden = true
    }
    @IBAction func clickedNo(_ sender: Any) {
        
        viewBackground.isHidden = true
        viewUpdate.isHidden = true
        viewDelete.isHidden = true
    }
    
    // MARK: - tblview Delegate Method
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "ManageTrainingsTblCell") as! ManageTrainingsTblCell
        
        cell.btnDelete.tag
        cell.btnDelete.addTarget(self, action: #selector(clickedDelete(_:)), for: .touchUpInside)
        
        cell.btnEdit.tag
        cell.btnEdit.addTarget(self, action: #selector(clickedEdit(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func clickedDelete(_ sender: UIButton)
    {
        viewBackground.isHidden = false
        viewDelete.isHidden = false
        viewUpdate.isHidden = true
        
    }
    
    @objc func clickedEdit(_ sender: UIButton)
    {
        viewBackground.isHidden = false
        viewUpdate.isHidden = false
        viewDelete.isHidden = true
        
    }
   
}

class ManageTrainingsTblCell: UITableViewCell
{
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var btnEdit: UIButton!
}

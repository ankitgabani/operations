//
//  InventoryHistoryVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit

class InventoryHistoryVC: UIViewController {

    @IBOutlet weak var btnReload: UIButton!
    @IBOutlet weak var tblVIew: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblVIew.delegate = self
        tblVIew.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickedReload(_ sender: Any) {
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension InventoryHistoryVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblVIew.dequeueReusableCell(withIdentifier: "InvetoryHistoryTblCell") as! InvetoryHistoryTblCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}

class InvetoryHistoryTblCell: UITableViewCell{
    
    
}

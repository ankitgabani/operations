//
//  AllOrdersVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit

class AllOrdersVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblViewClose: UITableView!
    
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var viewUnderlineOpen: UIView!
    @IBOutlet weak var viewUnderlineClose: UIView!
    
    @IBOutlet weak var viewClose: UIView!
    @IBOutlet weak var viewOpen: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
       
        tblViewClose.delegate = self
        tblViewClose.dataSource = self
        
        viewOpen.isHidden = false
        viewClose.isHidden = true
        
        viewUnderlineOpen.isHidden = false
        viewUnderlineClose.isHidden = true
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedOpen(_ sender: Any) {
        
        viewOpen.isHidden = false
        viewClose.isHidden = true
        
        viewUnderlineOpen.isHidden = false
        viewUnderlineClose.isHidden = true
        
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        
        viewOpen.isHidden = true
        viewClose.isHidden = false
        
        viewUnderlineOpen.isHidden = true
        viewUnderlineClose.isHidden = false
        
    }
    
}

extension AllOrdersVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblView
        {
            return 10
        }
        else if tableView == tblViewClose
        {
            return 0
        }
        else
        {
            return 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if tableView == tblView
        {
            let cell = self.tblView.dequeueReusableCell(withIdentifier: "AllOrdersOpenTblCell") as! AllOrdersOpenTblCell
            
            return cell
            
        }
        else if tableView == tblViewClose
        {
            let cell = self.tblViewClose.dequeueReusableCell(withIdentifier: "AllOrderCloseTblCell") as! AllOrderCloseTblCell
            
            return cell
            
        }
        else
        {
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
     
}
    


class AllOrdersOpenTblCell: UITableViewCell
{
    
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblTrackOders: UILabel!
    @IBOutlet weak var lblBora: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
}

class AllOrderCloseTblCell: UITableViewCell
{
    
}

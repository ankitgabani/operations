//
//  InternalQueryVC.swift
//  KC Ops
//
//  Created by macOS on 07/09/22.
//

import UIKit

class InternalQueryVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnRecieved: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    
    @IBOutlet weak var viewUnderLineRecived: UIView!
    @IBOutlet weak var viewUnderLineSend: UIView!
    
    @IBOutlet weak var viewRecieved: UIView!
    @IBOutlet weak var viewSend: UIView!
    
    @IBOutlet weak var tblViewRecieved: UITableView!
    @IBOutlet weak var tblViewSend: UITableView!
    
    @IBOutlet weak var btnNewQuery: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblViewRecieved.delegate = self
        tblViewRecieved.dataSource = self
        
        tblViewSend.delegate = self
        tblViewSend.dataSource  = self
        
        viewRecieved.isHidden = false
        viewSend.isHidden = true
        
        viewUnderLineRecived.isHidden = false
        viewUnderLineSend.isHidden = true
        
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func clickedRecieved(_ sender: Any) {
       
        viewRecieved.isHidden = false
        viewSend.isHidden = true
        
        viewUnderLineRecived.isHidden = false
        viewUnderLineSend.isHidden = true
    }
    
    @IBAction func clickedSend(_ sender: Any) {
       
        viewRecieved.isHidden = true
        viewSend.isHidden = false
        
        viewUnderLineRecived.isHidden = true
        viewUnderLineSend.isHidden = false
        
    }
    
    @IBAction func clickedNewQuery(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewQueryVC") as! NewQueryVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
extension InternalQueryVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblViewRecieved
        {
            return 3
        }
        else if tableView == tblViewSend
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblViewRecieved
        {
            let cell = self.tblViewRecieved.dequeueReusableCell(withIdentifier: "QueryrRecievedTblCell") as! QueryrRecievedTblCell
            
            return cell
        }
        else  if tableView == tblViewSend
        {
            let cell = self.tblViewSend.dequeueReusableCell(withIdentifier: "QuerySendTblCell") as! QuerySendTblCell
            
            return cell
        }
        else
        {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblViewRecieved
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "QueryDetailsViewController") as! QueryDetailsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "QueryDetailsViewController") as! QueryDetailsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    
}

class QueryrRecievedTblCell: UITableViewCell
{
    @IBOutlet weak var lblQuery: UILabel!
    @IBOutlet weak var lblSentBy: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

class QuerySendTblCell: UITableViewCell
{
    @IBOutlet weak var lblQuery: UILabel!
    @IBOutlet weak var lblSentBy: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

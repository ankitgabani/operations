//
//  OrderRecivedVC.swift
//  KC Ops
//
//  Created by iMac on 01/10/22.
//

import UIKit

class OrderRecivedVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
    }
    
    @IBAction func clickedDispatch(_ sender: Any) {
    }
    
    @IBAction func clickedReject(_ sender: Any) {
    }
    
}

extension OrderRecivedVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "OrderListTblCell") as! OrderListTblCell
        
        return cell
    }
    
    
}

class OrderListTblCell: UITableViewCell
{
   
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblBarCode: UILabel!
    @IBOutlet weak var lblRefrnce: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    
}

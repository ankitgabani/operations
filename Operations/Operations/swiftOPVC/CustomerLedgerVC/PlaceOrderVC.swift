//
//  PlaceOrderVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit
import DropDown

class PlaceOrderVC: UIViewController {
    
    var arrDrop = ["KO1_LK","KO1_LK","KO1_LK","KO1_LK","KO1_LK"]
    let dropDownReport = DropDown()

    
    @IBOutlet weak var viewDrop: UIView!
    @IBOutlet weak var btnBAck: UIButton!
    @IBOutlet weak var btnExcel: UIButton!
    @IBOutlet weak var viewWarehouse: UIView!
    @IBOutlet weak var txtSearchBarcode: UITextField!
    @IBOutlet weak var txtWarehouse: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDropDownReport()
        
        self.tabBarController?.tabBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickedExcel(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UploadExcelVC") as! UploadExcelVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func clickedDropDown(_ sender: Any) {
        dropDownReport.show()
    }
    
    @IBAction func clickedSearch(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func setDropDownReport() {
        
        dropDownReport.dataSource = arrDrop
        dropDownReport.anchorView = viewDrop
        dropDownReport.direction = .any
        
        dropDownReport.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtWarehouse.text = item
            
        }
        
        dropDownReport.bottomOffset = CGPoint(x: 0, y: viewDrop.bounds.height)
        dropDownReport.topOffset = CGPoint(x: 0, y: -viewDrop.bounds.height)
        dropDownReport.dismissMode = .onTap
        dropDownReport.textColor = UIColor.darkGray
        dropDownReport.backgroundColor = UIColor.white
        dropDownReport.selectionBackgroundColor = UIColor.clear
        
        dropDownReport.reloadAllComponents()
    }
    

}
 

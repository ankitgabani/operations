//
//  OrderHistoryVC.swift
//  KC Ops
//
//  Created by macOS on 06/09/22.
//

import UIKit

class OrderHistoryVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var bntreceive: UIButton!
    @IBOutlet weak var btnPlace: UIButton!
   
    @IBOutlet weak var viewPalced: UIView!
    @IBOutlet weak var viewReceived: UIView!
    
    @IBOutlet weak var tblViewPlced: UITableView!
    @IBOutlet weak var tblVIewReceived: UITableView!
   
    @IBOutlet weak var viewUndrlineReceive: UIView!
    @IBOutlet weak var viewUnderlinePlaced: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewReceived.isHidden = false
        viewUndrlineReceive.isHidden = false
        viewPalced.isHidden = true
        viewUnderlinePlaced.isHidden = true
        
        tblViewPlced.delegate = self
        tblViewPlced.dataSource = self
        
        tblVIewReceived.delegate = self
        tblVIewReceived.dataSource = self
        
        self.tabBarController?.tabBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func btnReceive(_ sender: Any) {
        
        viewReceived.isHidden = false
        viewUndrlineReceive.isHidden = false
        viewPalced.isHidden = true
        viewUnderlinePlaced.isHidden = true
        
    }
    @IBAction func clickedPlace(_ sender: Any) {
        
        viewReceived.isHidden = true
        viewUndrlineReceive.isHidden = true
        viewPalced.isHidden = false
        viewUnderlinePlaced.isHidden = false
        
    }
    

}

extension OrderHistoryVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblVIewReceived
        {
            return 3
        }
        else if tableView == tblViewPlced
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if tableView == tblVIewReceived
        {
            let cell = self.tblVIewReceived.dequeueReusableCell(withIdentifier: "OrderReceiveTblCell") as! OrderReceiveTblCell
            
            return cell
        }
        else if tableView == tblViewPlced
        {
            let cell = self.tblViewPlced.dequeueReusableCell(withIdentifier: "OrderPlaceTblCell") as! OrderPlaceTblCell
            
            return cell
        }
        else
        {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewPlced
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsVC") as! OrderDetailsVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}

class OrderReceiveTblCell: UITableViewCell
{
    @IBOutlet weak var lblnName: UILabel!
    @IBOutlet weak var lblNumberCount: UILabel!
    
}

class OrderPlaceTblCell: UITableViewCell
{
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblWhareHouseNo: UILabel!
    @IBOutlet weak var lblBoraQty: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
}

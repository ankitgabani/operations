//
//  TrainingsVC.swift
//  KC Ops
//
//  Created by macOS on 07/09/22.
//

import UIKit

class TrainingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblViewTraning: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewTraning.delegate = self
        tblViewTraning.dataSource = self
        
        self.tabBarController?.tabBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = self.tblViewTraning.dequeueReusableCell(withIdentifier: "TraningTblViewCell") as! TraningTblViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TraningModuleVC") as! TraningModuleVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

class TraningTblViewCell: UITableViewCell
{
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}

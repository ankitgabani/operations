//
//  OrderDetailsVC.swift
//  KC Ops
//
//  Created by macOS on 30/09/22.
//

import UIKit

class OrderDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderBy: UILabel!
    @IBOutlet weak var lblOrderFrom: UILabel!
    @IBOutlet weak var lblOrderTo: UILabel!
    @IBOutlet weak var lblTotalQty: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self

        
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedDownload(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "OrderDetailsCell") as! OrderDetailsCell
        
        return cell
    }
    

    

}
class OrderDetailsCell: UITableViewCell{
    @IBOutlet weak var lblBarcorde: UILabel!
    @IBOutlet weak var lblReferenceNo: UILabel!
    @IBOutlet weak var lblOpen: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblRupees: UILabel!
    
}

//
//  AccountLedgerVC.swift
//  KC Ops
//
//  Created by iMac on 27/09/22.
//

import UIKit

class AccountLedgerVC: UIViewController {

    @IBOutlet weak var lblAgentName: UILabel!
    @IBOutlet weak var lblCustomerCode: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCustomerMobileNo: UILabel!
    @IBOutlet weak var lblCustomerGSTNo: UILabel!
    @IBOutlet weak var lblARInvoiceDate: UILabel!
    @IBOutlet weak var lblARInvoiceAmt: UILabel!
    @IBOutlet weak var lblIncommingPaymentDate: UILabel!
    @IBOutlet weak var lblIncomingPaymentAmt: UILabel!
    @IBOutlet weak var lblClosingBalance: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func clcikedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedOpenDou(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ErrrorViewController") as! ErrrorViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedDownloadDou(_ sender: Any) {
    }
    

}

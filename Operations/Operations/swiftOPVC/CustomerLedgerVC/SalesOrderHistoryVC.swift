//
//  SalesOrderHistoryVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit

class SalesOrderHistoryVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgCancel: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        txtSearch.delegate = self

        
        
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
            
        }
        else
        {
            imgCancel.isHidden = false
        }
        
        self.txtSearch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        
        txtSearch.text = ""
        imgCancel.isHidden = true
        
        
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField)
    {
        if txtSearch.text == ""
        {
            imgCancel.isHidden = true
            
        }
        else
        {
            imgCancel.isHidden = false
        }
    }
   
    
    
}

extension SalesOrderHistoryVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "SalesOrderHistorTblCell") as! SalesOrderHistorTblCell
        
        return cell
    }
    
    
}

class SalesOrderHistorTblCell: UITableViewCell
{
    @IBOutlet weak var lblSalesOederId: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSalesMen: UILabel!
    @IBOutlet weak var lblTransport: UILabel!
    @IBOutlet weak var lblCustomer: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
}

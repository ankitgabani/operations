//
//  CustomerLedgerVC.swift
//  KC Ops
//
//  Created by macOS on 05/09/22.
//

import UIKit

class CustomerLedgerVC: UIViewController {

    @IBOutlet weak var txtSrarch: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblDateTIme: UILabel!
    @IBOutlet weak var imgCancel: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.isHidden = false
        
        if txtSrarch.text == ""
        {
            imgCancel.isHidden = true
            
            
        }
        else
        {
            imgCancel.isHidden = false
            

        }
        
        self.txtSrarch.addTarget(self, action: #selector(searchWorkersAsPerText(_ :)), for: .editingChanged)
    }
    
    @IBAction func clickedCencel(_ sender: Any) {
        txtSrarch.text = ""
        imgCancel.isHidden = true
        
    }
    @IBAction func clickedAdd(_ sender: Any) {
    }
    
    @objc func searchWorkersAsPerText(_ textfield:UITextField)
    {
        if txtSrarch.text == ""
        {
            imgCancel.isHidden = true
            
        }
        else
        {
            imgCancel.isHidden = false
        }
    }
    
    
}

extension CustomerLedgerVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "LedgerTblCell") as! LedgerTblCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountLedgerVC") as! AccountLedgerVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

class LedgerTblCell: UITableViewCell
{
    @IBOutlet weak var lblCustomerCode: UILabel!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblPaymentDate: UILabel!
    @IBOutlet weak var lblBalence: UILabel!
    
}

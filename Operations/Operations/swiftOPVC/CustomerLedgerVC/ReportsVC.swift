//
//  ReportsVC.swift
//  KC Ops
//
//  Created by iMac on 27/09/22.
//

import UIKit
import DropDown

class ReportsVC: UIViewController {

    @IBOutlet weak var txtEnterReport: UITextField!
    @IBOutlet weak var viewDrop: UIView!
    
    let dropDownReport = DropDown()
    
    var arrReport = ["All Queries Report"," Attendance Report","Best article Report"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDropDownReport()

        // Do any additional setup after loading the view.
    }
    
    func setDropDownReport() {
        
        dropDownReport.dataSource = arrReport
        dropDownReport.anchorView = viewDrop
        dropDownReport.direction = .any
        
        dropDownReport.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.txtEnterReport.text = item
            
        }
        
        dropDownReport.bottomOffset = CGPoint(x: 0, y: viewDrop.bounds.height)
        dropDownReport.topOffset = CGPoint(x: 0, y: -viewDrop.bounds.height)
        dropDownReport.dismissMode = .onTap
        dropDownReport.textColor = UIColor.darkGray
        dropDownReport.backgroundColor = UIColor.white
        dropDownReport.selectionBackgroundColor = UIColor.clear
        
        dropDownReport.reloadAllComponents()
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func clickedDropDown(_ sender: Any) {
        
        dropDownReport.show()
        
    }
    @IBAction func clickedDownloadReport(_ sender: Any) {
    }
    
    
}

//
//  NewQueryVC.swift
//  KC Ops
//
//  Created by macOS on 07/09/22.
//

import UIKit

class NewQueryVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnDatePicker: UIButton!
    @IBOutlet weak var btnSendQuery: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var bgDatePickerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        bgDatePickerView.isHidden = true
        datePicker.locale = .current
        datePicker.date = Date()
        datePicker.preferredDatePickerStyle = .inline
        datePicker.addTarget(self, action: #selector(dateSelected), for: .valueChanged )
    }
    
    @objc func dateSelected(){
        
        let formmeter  = DateFormatter()
        formmeter.dateFormat = "d MMM yyyy"
        let date = formmeter.string(from: datePicker.date)
        lblDate.text = date
     self.view.endEditing(true)
        
    }
    
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedDatePicker(_ sender: Any) {
        bgDatePickerView.isHidden = false
        datePicker.preferredDatePickerStyle = .inline
    
        
        
    }
    @IBAction func clcikedSendQuery(_ sender: Any) {
        if isVlidationLogin(){
            
        }
    }
    func isVlidationLogin() -> Bool{
        if txtTo.text == ""
        {
            self.view.makeToast("please enter query recipient")
            return false
        }
        return true
    }
        
}

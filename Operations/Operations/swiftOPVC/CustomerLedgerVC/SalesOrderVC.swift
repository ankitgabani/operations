//
//  SalesOrderVC.swift
//  KC Ops
//
//  Created by iMac on 08/10/22.
//

import UIKit

class SalesOrderVC: UIViewController {

   
    @IBOutlet weak var lblSalemen: UILabel!
    @IBOutlet weak var lblTransport: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStation: UILabel!
    @IBOutlet weak var lblAgent: UILabel!
    @IBOutlet weak var lblPackageType: UILabel!
 
    @IBOutlet weak var txtPackageOty: UITextField!
    @IBOutlet weak var txtCost: UITextField!
    @IBOutlet weak var txtPackage: UITextField!
    
    @IBOutlet weak var viewBackGround: UIView!
    @IBOutlet weak var CenterView: UIView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var txtTypes: UITextField!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var viewSelectSalesMan: UIView!
    @IBOutlet weak var lblSelectSalesMan: UILabel!
    @IBOutlet weak var tblSalesMan: UITableView!
    
    var arrSales = ["ayush Kumar","Abhiranjan Mishra","Abhishek Kumar","ayush Kumar","Abhiranjan Mishra","Abhishek Kumar","ayush Kumar","Abhiranjan Mishra","Abhishek Kumar","ayush Kumar","Abhiranjan Mishra","Abhishek Kumar","ayush Kumar","Abhiranjan Mishra","Abhishek Kumar"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSalesMan.delegate = self
        tblSalesMan.dataSource = self
        
        viewBackGround.isHidden = true
        CenterView.isHidden = true
        viewSelectSalesMan.isHidden = true

    }
    
    @IBAction func clickedBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func clickedSalesmen(_ sender: Any) {
        viewBackGround.isHidden = false
        viewSelectSalesMan.isHidden = false
        
    }
    @IBAction func clickedTransport(_ sender: Any) {
        viewBackGround.isHidden = false
        CenterView.isHidden = false
        
       
    }
    @IBAction func clickedCode(_ sender: Any) {
        viewBackGround.isHidden = false
        CenterView.isHidden = false
       
            }
    @IBAction func clickedMobileNo(_ sender: Any) {
        viewBackGround.isHidden = false
        CenterView.isHidden = false
        
                
    }
    @IBAction func clickedPackageType(_ sender: Any) {
        viewBackGround.isHidden = false
        viewSelectSalesMan.isHidden = false
    }
    @IBAction func clickedRemark(_ sender: Any) {
        
       
    }
    
    @IBAction func clickedCreate(_ sender: Any) {
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        viewBackGround.isHidden = true
        CenterView.isHidden = true
    }
    @IBAction func clickedOk(_ sender: Any) {
        viewBackGround.isHidden = true
        CenterView.isHidden = true
    }
    
    @IBAction func clickedBarcode(_ sender: Any) {
    }
    @IBAction func btnHideBackGround(_ sender: Any) {
        viewBackGround.isHidden = true
        viewSelectSalesMan.isHidden = true
        CenterView.isHidden = true
    }
}

class SelectSalsesManCell: UITableViewCell{
    
    @IBOutlet weak var lblName: UILabel!
}
extension SalesOrderVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSalesMan.dequeueReusableCell(withIdentifier: "SelectSalsesManCell") as! SelectSalsesManCell
        cell.lblName.text = arrSales[indexPath.row]
        return cell
    }
    
    
}

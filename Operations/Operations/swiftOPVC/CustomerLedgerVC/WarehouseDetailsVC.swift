//
//  WarehouseDetailsVC.swift
//  KC Ops
//
//  Created by macOS on 30/09/22.
//

import UIKit

class WarehouseDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
    }
    
    @IBAction func clickedBack(_ sender: Any) {
    }
    
    @IBAction func clickedDone(_ sender: Any) {
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "WarehouseEmployeesCell") as! WarehouseEmployeesCell
        
        return cell
    }
    
    
}
class WarehouseEmployeesCell: UITableViewCell{
    
    @IBOutlet weak var lblEmployeeName: UILabel!
    @IBOutlet weak var lblEmployeeEmail: UILabel!
    @IBOutlet weak var switchPermistion: UISwitch!
}

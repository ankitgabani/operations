//
//  LoginOPViewController.swift
//  KC Ops
//
//  Created by macOS on 17/08/22.
//

import UIKit

class LoginOPViewController: UIViewController {

    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPaasword: UITextField!
    @IBOutlet weak var btnForgetPass: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnShowPass: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedForgetPassword(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordOPViewController") as! ForgetPasswordOPViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
    }
    @IBAction func clickedSignup(_ sender: Any) {
    }
    @IBAction func clickedShowPass(_ sender: Any) {
    }
    

}

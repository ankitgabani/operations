//
//  WelcomOPViewController.swift
//  KC Ops
//
//  Created by macOS on 17/08/22.
//

import UIKit

class WelcomOPViewController: UIViewController {

    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedSignup(_ sender: Any) {
    }
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginOPViewController") as! LoginOPViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

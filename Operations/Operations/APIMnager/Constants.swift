//
//  Constants.swift
//  Operations
//
//  Created by Ankit Gabani on 12/02/22.
//

import Foundation
import UIKit

let BASE_URL = "http://operations-test.ap-south-1.elasticbeanstalk.com/v1/"

// ******************************
//MARK: -  SPLAS
let USER_LOGIN = "users/login"

let SPLASH_SCREEN = "splashScreen"

let RECENT_INVENRORY_UPDATE = "recent-inventory-update"


// ******************************
//MARK: - LOGIN
let APP_VERSION = "version"

//let SPLASH_SCREEN = "splashScreen"

//let RECENT_INVENRORY_UPDATE = "recent-inventory-update"


// ******************************
//MARK: - SIGN_UP
let GET_RELATIONS = "relations"

let GET_PROFESSIONS = "professions"

let GET_QUALIFICATIONS = "qualifications"

let USER_REGISTER = "users"


// ******************************
//MARK: - FORGET_PASSWORD
let FORGET_PASSWORD = "users/forgetpassword"


// ******************************
//MARK: -  INVENTORY
let SEARCH_BARCODE = "search"

let SEARCH_DETAILS = "search/"

// ******************************
//MARK: - FORGET_PASSWORD

// ******************************
//MARK: -  WAREHOUSE
let GET_WAREHOUSE = "warehouses"


let REQ_COUNT = "counts"
let SEARCH_EMPLOYEE = "warehouses?filter=Y"
let GET_USERS = "users?location"

let GET_USERS_DETAILS = "users/"
let GET_USERS_ROLE = "/user-roles"
let GET_USERS_PERMISSIONS = "/permissions"

let GET_GROUP_ROLES = "roles"
let GET_ALL_PERMISSIONS = "permissions"
let GET_GROUP_POLICY = "/policies"

let GET_CURRENT_WAREHOUSES = "warehouses"
let GET_WAREHOUS_DETAILS = "/users"

let GET_ALL_QUERIES = "query/all"
let GET_QUERY_DETAIL = "query/"


//MARK:- UPLOAD DOCUMENTS
let GET_DOCUMENT_TYPE = "documenttypes"
let GET_TRAINING_TYPE = "training"
let DOCUMENT_UPLOAD = "upload"


let ASSIGN_ROLES = "assign-roles"

let DEACTIVATE = "users/deactivate"

let CREATE_GROUP = "roles"

let CREATE_WAREHOUESE = "warehouses"

let UPDATE_ASSIGNED_USERS = "warehouses/"

let QUERY_ACTION = "query/action"

let DOWNLOAND_TEMP = "download/temp"


let appDelegate = UIApplication.shared.delegate as! AppDelegate

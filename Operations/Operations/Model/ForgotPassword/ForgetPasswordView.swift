//
//  ForgetPasswordView.swift
//  Operations
//
//  Created by Ankit Gabani on 26/01/22.
//

import SwiftUI
 
struct ForgetPassword: View {
    @State var email = ""
    @State var errorMessage = ""
    @State var parameters = [String: Any]()
    @State private var showingAlert = false
    @State private var isLoginSuccess = false
 
    var body: some View {
        ZStack{
            Color.white
                .ignoresSafeArea(edges: .all)
            VStack{
                Image("kc_logo")
                    .resizable()
                    .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                    .frame(width:65,height: 65, alignment: .center)
                
                Text("Reset Your Password")
                    .fontWeight(.semibold)
                    .font(.title2)
                    .padding()
                Text("Enter your email address and a password reset\n request will be sent to Admin")
                    .font(.caption2)
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color(red: 200/255, green: 200/255, blue: 200/255, opacity: 1))
                
                VStack{
                    HStack
                    {
                        Image(systemName: "envelope.fill")
                            .opacity(0.6)
                            .accentColor(.gray)
                        TextField("Email", text: self.$email)
                            .foregroundColor(.gray)
                            .padding(.leading,6)
                            .keyboardType(.emailAddress)
                    }
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 6).stroke(lineWidth: 2).foregroundColor(Color(red: 200/255, green: 200/255, blue: 200/255, opacity: 1)))
                    .frame(width: 320, height: 100, alignment: .center)
                }
                .padding(.bottom, 8.0)
                
                VStack{
                    Button(action: {
                        if let errorMessage = self.validView() {
                            print(errorMessage)
                            self.errorMessage = errorMessage
                            showingAlert = true
                            
                            return
                        }
                        
                        APIClient().MakeAPICallWithAuthHeaderPut(FORGET_PASSWORD, parameters: email.lowercased()) { (responce, error, statusCose) in
                            
                            if error == nil {
                                print("Responce: \(String(describing: responce))")
                                
                                let strMessage = responce?.value(forKey: "message") as! String
                                self.errorMessage = strMessage
                                showingAlert = true
                               
                            } else {
                                self.errorMessage = (String(describing: error))
                                showingAlert = true
                                
                            }
                            
                        }
                        
                    }, label: {
                        Text("RESET")
                            .frame(maxWidth:.infinity)
                            .font(.callout)
                    })
                }.padding()
                    .frame(width: 170.0, height: 35.0)
                    .foregroundColor(.white)
                    .background(Color.black)
                    .cornerRadius(10)
            }
            .padding(.bottom, 200.0)
            Spacer()
            Spacer()
        }
        .alert(isPresented: $showingAlert) {
            Alert(title: Text(""), message: Text(errorMessage), dismissButton: .default(Text("OK")))
            
        }
    }
    
    private func validView() -> String? {
        if email.isEmpty {
            return "Email is empty"
        }
        
        if !self.isValidEmail(email) {
            return "Email is invalid"
        }
        
        // Do same like other validation as per needed
        
        return nil
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}

struct ForgetPassword_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ForgetPassword()
        }
    }
}

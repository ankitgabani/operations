//
//  WelcomeView.swift
//  Operations
//
//  Created by Ankit Gabani on 26/01/22.
//

import SwiftUI
import CoreGraphics
import Foundation

struct WelcomeView: View {
    @State var selectionIndex = 0

    init() {
        UIScrollView.appearance().bounces = false
    }
    
    var body: some View {
        
        NavigationView{
            ZStack{
                Color.black
                    .ignoresSafeArea(edges:.bottom)
                
                VStack{
                    ScrollView{
                        VStack{
                            Image("kc_logo")
                                .resizable()
                                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                                .frame(width:70,height: 70, alignment: .center)
                            Image("Launch1")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .padding(12)
                            Image("Launch2")
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 200, height: 200, alignment: .center)
                            VStack{
                                Text("CONTACT US")
                                //.fontWeight(.bold)
                                    .font(.system(size: 18))
                                    .padding(6)
                                    .padding(.top)
                                Divider().background(Color.white)
                                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                                
                                Text("My Contact Details")
                                    .padding(10)
                                    .font(.system(size: 25))
                                Button(action: {
                                    EmailHelper.shared.sendEmail(subject: "Support", body: "", to: "support@gmail.com")
                                }) {
                                    Text("support@gmail.com")
                                        .padding(4)
                                        .font(.system(size: 14))
                                        .accentColor(.white)
                                }
                                Link("+91 999999999", destination: URL(string: "tel://+91999999999")!)
                                    .padding(10)
                                    .font(.system(size: 14))
                                VStack
                                {
                                    Divider().background(Color.white)
                                        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                                    //.frame(width:240,height: 2)
                                    //.background(Color.white)
                                    Link("ABOUT COMPANY",destination: URL(string: "https://www.clickdimensions.com/links/TestPDFfile.pdf")!)
                                        .padding(4)
                                        .font(.system(size: 12))
                                    Link("OUR BRANDS",destination: URL(string: "https://en.wikipedia.org/wiki/Company")!)
                                        .padding(4)
                                        .font(.system(size: 12))
                                    Link("ABOUT DIRECTORS",destination: URL(string: "https://en.wikipedia.org/wiki/Company")!)
                                        .padding(4)
                                        .font(.system(size: 12))
                                    Link("WORKING STRUCTURE",destination: URL(string: "https://en.wikipedia.org/wiki/Company")!)
                                        .padding(4)
                                        .font(.system(size: 12))
                                    Link("HOW TO USE",destination: URL(string: "https://en.wikipedia.org/wiki/Company")!)
                                        .padding(4)
                                        .font(.system(size: 12))
                                }
                            }.border(Color.black, width: 2)
                                .foregroundColor(.white)
                                .background(Color.black)
                                .frame(maxWidth: .infinity, maxHeight: .infinity)
                        }
                        .background(Color.white)
                    }
                    HStack(alignment: .bottom, spacing: 0) {
                        Button(action: {}) {
                            // navigationBarHidden(true)
                            NavigationLink("SIGN UP", destination: SignupView())
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 14))
                                .padding(10)
                        }
                        .cornerRadius(25)
                        .clipped()
                        .foregroundColor(.white)
                        .overlay(RoundedRectangle(cornerRadius: 27)
                                    .stroke(Color.white, lineWidth: 3))
                        Spacer()
                        Spacer()
                        Button(action: { }) {
                            NavigationLink("LOGIN", destination: Login())
                            
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 14))
                                .padding(10)
                        }
                        .overlay(RoundedRectangle(cornerRadius: 27)
                                    .stroke(Color.white, lineWidth: 3))
                    }
                    .cornerRadius(5)
                    .clipped()
                    .padding(EdgeInsets(top: 0, leading: 14, bottom: 15, trailing: 14))
                    .foregroundColor(.white)
                    .background(Color.black)
                    //.padding(.bottom)
                }
                .background(Color.black)
            }
            .onAppear {
              
                APIClient().MakeAPICallWithAuthHeaderGetHeaderEmail(SPLASH_SCREEN, headers: ["Email": "vb@gmail.com"]) { (responce, error, statusCose) in
                    if error == nil {
                        print("Responce: \(String(describing: responce))")
                    }
                    
                }
            }
            .navigationBarHidden(true)
        }
        .navigationBarHidden(true)
    }
}


struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
           
    }
}




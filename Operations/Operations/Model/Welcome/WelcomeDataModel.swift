//
//  WelcomeDataModel.swift
//  Operations
//
//  Created by Ankit Gabani on 14/02/22.
//

import Foundation

struct WelcomeDataModel: Codable {
    var userId: Int
    var email: String
    var fullName: String
    var employeeId: String
    var deviceId: String
    var isActive: Bool
    var isPasswordResetReqSent: Bool
    var isPasswordResetReqApproved: Bool
    var isLoggedIn: Bool
    var permissions: [permissions]
}

struct permissions: Codable {
    var permissionCode: String
    var permissionId: Int
    var permissionName: String
}

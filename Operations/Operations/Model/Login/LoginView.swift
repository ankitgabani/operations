//
//  ContentView.swift
//  Operatious
//
//  Created by Gabani King on 22/01/22.
//

import SwiftUI
import SVProgressHUD

struct Login: View {
    @State var email = ""
    @State var password = ""
    @State var errorMessage = ""
    @State var parameters = [String: Any]()
    @State private var hidde: Bool = true
    @State private var showingAlert = false
    @State private var isLoginSuccess = false
    
    
    
    var body: some View {
        ZStack{
            VStack{
                Image("kc_logo")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width:65,height: 65, alignment: .center)
                VStack{
                    Text("Login")
                        .frame(width: 325, height: 50, alignment: .center)
                        .border(Color.black)
                        .foregroundColor(.white)
                        .background(Color.black)
                    HStack{
                        Image(systemName: "envelope.fill")
                            .opacity(0.6)
                            .accentColor(.gray)
                        TextField("Email", text: self.$email)
                            .foregroundColor(.gray)
                            .padding(.leading,6)
                            .keyboardType(.emailAddress)
                    }.padding()
                        .overlay(RoundedRectangle(cornerRadius: 6).stroke(lineWidth: 2).foregroundColor(Color(red: 200/255, green: 200/255, blue: 200/255, opacity: 1)))
                        .frame(width: 300, height: 100, alignment: .center)
                    HStack{
                        Image(systemName: "lock.fill")
                            .opacity(0.6)
                        
                        if hidde {
                            SecureField("Password", text: self.$password)
                                .foregroundColor(.gray)
                                .padding(.leading,6)
                        } else {
                            TextField("Password", text: self.$password)
                                .foregroundColor(.gray)
                                .padding(.leading,6)
                        }
                        Button(action: {
                            hidde.toggle()
                            
                        }) {
                            Image(systemName: self.hidde ? "eye.fill" : "eye.slash.fill")
                                .accentColor(.gray)
                        }
                    }.padding()
                        .overlay(RoundedRectangle(cornerRadius: 6).stroke(lineWidth: 2).foregroundColor(Color(red: 200/255, green: 200/255, blue: 200/255, opacity: 1)))
                        .frame(width: 300, alignment: .center)
                    NavigationLink("Forget Password?", destination: ForgetPassword())
                    
                        .accentColor(.black)
                        .font(.callout)
                        .padding(EdgeInsets(top: 40, leading: 170, bottom: 0, trailing: 0))
                    
                    VStack{
                        Button(action: {
                            if let errorMessage = self.validView() {
                                print(errorMessage)
                                self.errorMessage = errorMessage
                                showingAlert = true
                                
                                return
                            }
                            self.parameters["email"] = email
                            self.parameters["password"] = password
                            self.parameters["deviceId"] = "fsfdsf"
                            self.parameters["deviceName"] = "iPhone8"
                            
                            APIClient().MakeAPICallWithAuthHeaderPost(USER_LOGIN, parameters: parameters) { (responce, error, statusCose) in
                                
                                if error == nil {
                                    print("Responce: \(String(describing: responce))")
                                    
                                    let code = responce?.value(forKey: "code") as? Int
                                    let message = responce?.value(forKey: "message") as? String
                                    
                                    if statusCose == 200
                                    {
                                        let dicData = OPLoginData(fromDictionary: responce!)
                                        
                                        appDelegate.saveCurrentUserData(dic: dicData)
                                        appDelegate.dicLoginDetails = dicData
                                        
                                        isLoginSuccess = true
                                        UserDefaults.standard.set(dicData.token ?? "", forKey: "UserToken")
                                        UserDefaults.standard.set(true, forKey: "UserLogin")
                                        UserDefaults.standard.set(email, forKey: "Email")
                                    }
                                    else
                                    {
                                        self.errorMessage = message ?? ""
                                        showingAlert = true
                                    }
                                    
                                } else {
                                    self.errorMessage = (String(describing: error))
                                    showingAlert = true
                                    
                                }
                                
                            }
                            
                        }, label: {
                            NavigationLink(destination: HomeView() ,isActive: $isLoginSuccess) { //<- This will take it to rest
                                EmptyView()
                            }
                            Text("LOGIN")
                                .frame(maxWidth:.infinity)
                                .font(.callout)
                            
                        })
                        
                        .padding()
                        .frame(width: 170.0, height: 35.0)
                        .foregroundColor(.white)
                        .background(Color.black)
                        .cornerRadius(10)
                    }.padding(.top, 22)
                }
                .padding(.bottom,30)
                .background(Color.white)
                .cornerRadius(10)
                .shadow(color: Color.black.opacity(0.4), radius: 10, x: 0.0, y: 0.0)
                HStack{
                    Text("Dont have an account?")
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    NavigationLink("Sign Up",destination: SignupView())
                        .foregroundColor(.black)
                }
                .padding(.top)
                Spacer()
            }.padding(.top,60)
            
        }
        .edgesIgnoringSafeArea(.top)
        
        .alert(isPresented: $showingAlert) {
            Alert(title: Text(""), message: Text(errorMessage), dismissButton: .default(Text("OK")))
        }
        
    }
    
    
    private func validView() -> String? {
        if email.isEmpty {
            return "Email is empty"
        }
        
        if !self.isValidEmail(email) {
            return "Email is invalid"
        }
        
        if password.isEmpty {
            return "Password is empty"
        }
        
        if self.password.count < 5 {
            return "Password should be 8 character long"
        }
        
        // Do same like other validation as per needed
        
        return nil
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}



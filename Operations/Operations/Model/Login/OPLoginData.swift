//
//	OPLoginData.swift
//
//	Create by Ankit Gabani on 14/2/2022
//	Copyright © 2022. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class OPLoginData : NSObject, NSCoding{

	var email : String!
	var employeeId : String!
	var fullName : String!
	var isSuperAdmin : Bool!
	var mobileNumber : String!
	var refreshToken : String!
	var token : String!
	var userId : Int!
	var username : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
		employeeId = dictionary["employeeId"] as? String == nil ? "" : dictionary["employeeId"] as? String
		fullName = dictionary["fullName"] as? String == nil ? "" : dictionary["fullName"] as? String
		isSuperAdmin = dictionary["isSuperAdmin"] as? Bool == nil ? false : dictionary["isSuperAdmin"] as? Bool
		mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
		refreshToken = dictionary["refreshToken"] as? String == nil ? "" : dictionary["refreshToken"] as? String
		token = dictionary["token"] as? String == nil ? "" : dictionary["token"] as? String
		userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
		username = dictionary["username"] as? String == nil ? "" : dictionary["username"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if email != nil{
			dictionary["email"] = email
		}
		if employeeId != nil{
			dictionary["employeeId"] = employeeId
		}
		if fullName != nil{
			dictionary["fullName"] = fullName
		}
		if isSuperAdmin != nil{
			dictionary["isSuperAdmin"] = isSuperAdmin
		}
		if mobileNumber != nil{
			dictionary["mobileNumber"] = mobileNumber
		}
		if refreshToken != nil{
			dictionary["refreshToken"] = refreshToken
		}
		if token != nil{
			dictionary["token"] = token
		}
		if userId != nil{
			dictionary["userId"] = userId
		}
		if username != nil{
			dictionary["username"] = username
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObject(forKey: "email") as? String
         employeeId = aDecoder.decodeObject(forKey: "employeeId") as? String
         fullName = aDecoder.decodeObject(forKey: "fullName") as? String
         isSuperAdmin = aDecoder.decodeObject(forKey: "isSuperAdmin") as? Bool
         mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
         refreshToken = aDecoder.decodeObject(forKey: "refreshToken") as? String
         token = aDecoder.decodeObject(forKey: "token") as? String
         userId = aDecoder.decodeObject(forKey: "userId") as? Int
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if employeeId != nil{
			aCoder.encode(employeeId, forKey: "employeeId")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "fullName")
		}
		if isSuperAdmin != nil{
			aCoder.encode(isSuperAdmin, forKey: "isSuperAdmin")
		}
		if mobileNumber != nil{
			aCoder.encode(mobileNumber, forKey: "mobileNumber")
		}
		if refreshToken != nil{
			aCoder.encode(refreshToken, forKey: "refreshToken")
		}
		if token != nil{
			aCoder.encode(token, forKey: "token")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "userId")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}

//
//  UsersModel.swift
//  Operations
//
//  Created by iMac on 23/03/22.
//

import Foundation

class UsersDataModel: NSObject, NSCoding {
    
    var userId: Int!
    var email: String!
    var firstName: String!
    var lastName: String!
    var fullName: String!
    var nameInitials: String!
    var mobileNumber: String!
    var employeeId: String!
    var wareHouseId: Int!
    var wareHouse: String!
    
    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }

    /**
     * Overiding init method
     */
    override init(){
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        userId = dictionary["userId"] as? Int == nil ? 0 : dictionary["userId"] as? Int
        wareHouseId = dictionary["wareHouseId"] as? Int == nil ? 0 : dictionary["wareHouseId"] as? Int
        email = dictionary["email"] as? String == nil ? "" : dictionary["email"] as? String
        firstName = dictionary["firstName"] as? String == nil ? "" : dictionary["firstName"] as? String
        lastName = dictionary["lastName"] as? String == nil ? "" : dictionary["lastName"] as? String
        fullName = dictionary["fullName"] as? String == nil ? "" : dictionary["fullName"] as? String
        nameInitials = dictionary["nameInitials"] as? String == nil ? "" : dictionary["nameInitials"] as? String
        mobileNumber = dictionary["mobileNumber"] as? String == nil ? "" : dictionary["mobileNumber"] as? String
        employeeId = dictionary["employeeId"] as? String == nil ? "" : dictionary["employeeId"] as? String
        wareHouse = dictionary["wareHouse"] as? String == nil ? "" : dictionary["wareHouse"] as? String

    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if userId != nil{
            dictionary["userId"] = userId
        }
        if wareHouseId != nil{
            dictionary["wareHouseId"] = wareHouseId
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstName != nil{
            dictionary["firstName"] = firstName
        }
        if lastName != nil{
            dictionary["lastName"] = lastName
        }
        if fullName != nil{
            dictionary["fullName"] = fullName
        }
        if nameInitials != nil{
            dictionary["nameInitials"] = nameInitials
        }
        if mobileNumber != nil{
            dictionary["mobileNumber"] = mobileNumber
        }
        if employeeId != nil{
            dictionary["employeeId"] = employeeId
        }
        if wareHouse != nil{
            dictionary["wareHouse"] = wareHouse
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        userId = aDecoder.decodeObject(forKey: "userId") as? Int
        wareHouseId = aDecoder.decodeObject(forKey: "wareHouseId") as? Int
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        fullName = aDecoder.decodeObject(forKey: "fullName") as? String
        nameInitials = aDecoder.decodeObject(forKey: "nameInitials") as? String
        mobileNumber = aDecoder.decodeObject(forKey: "mobileNumber") as? String
        employeeId = aDecoder.decodeObject(forKey: "employeeId") as? String
        wareHouse = aDecoder.decodeObject(forKey: "wareHouse") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder)
    {
        if userId != nil{
            aCoder.encode(userId, forKey: "userId")
        }
        if wareHouseId != nil{
            aCoder.encode(wareHouseId, forKey: "wareHouseId")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if firstName != nil{
            aCoder.encode(firstName, forKey: "firstName")
        }
        if lastName != nil{
            aCoder.encode(lastName, forKey: "lastName")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "fullName")
        }
        if nameInitials != nil{
            aCoder.encode(nameInitials, forKey: "nameInitials")
        }
        if mobileNumber != nil{
            aCoder.encode(mobileNumber, forKey: "mobileNumber")
        }
        if employeeId != nil{
            aCoder.encode(employeeId, forKey: "employeeId")
        }
        if wareHouse != nil{
            aCoder.encode(wareHouse, forKey: "wareHouse")
        }
    }

}

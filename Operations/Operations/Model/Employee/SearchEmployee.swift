//
//  SearchEmployee.swift
//  Operations
//
//  Created by iMac on 23/03/22.
//

import Foundation


class SearchEmployee: NSObject, NSCoding {
    
    var masterId: Int!
    var descriptionName: String!
    var code: String!
    var isActive: Bool!
    var canRegister: Bool!

    
    /**
     * Overiding init method
     */
    init(fromDictionary dictionary: NSDictionary)
    {
        super.init()
        parseJSONData(fromDictionary: dictionary)
    }

    /**
     * Overiding init method
     */
    override init(){
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    @objc func parseJSONData(fromDictionary dictionary: NSDictionary)
    {
        masterId = dictionary["masterId"] as? Int == nil ? 0 : dictionary["masterId"] as? Int
        descriptionName = dictionary["description"] as? String == nil ? "" : dictionary["description"] as? String
        code = dictionary["code"] as? String == nil ? nil : dictionary["code"] as? String
        isActive = dictionary["isActive"] as? Bool == nil ? false : dictionary["isActive"] as? Bool
        canRegister = dictionary["canRegister"] as? Bool == nil ? false : dictionary["canRegister"] as? Bool
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if descriptionName != nil{
            dictionary["description"] = descriptionName
        }
        if canRegister != nil{
            dictionary["canRegister"] = canRegister
        }
        if code != nil{
            dictionary["code"] = code
        }
        if isActive != nil{
            dictionary["isActive"] = isActive
        }
        if masterId != nil{
            dictionary["masterId"] = masterId
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        descriptionName = aDecoder.decodeObject(forKey: "description") as? String
        code = aDecoder.decodeObject(forKey: "code") as? String
         isActive = aDecoder.decodeObject(forKey: "isActive") as? Bool
         masterId = aDecoder.decodeObject(forKey: "masterId") as? Int
        canRegister = aDecoder.decodeObject(forKey: "canRegister") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder)
    {
        if descriptionName != nil{
            aCoder.encode(descriptionName, forKey: "description")
        }
        if isActive != nil{
            aCoder.encode(isActive, forKey: "isActive")
        }
        if masterId != nil{
            aCoder.encode(masterId, forKey: "masterId")
        }
        if canRegister != nil{
            aCoder.encode(masterId, forKey: "canRegister")
        }
        if code != nil{
            aCoder.encode(masterId, forKey: "code")
        }
    }

}
